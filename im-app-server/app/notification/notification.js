const nodemailer = require('nodemailer');
const propertiesReader = require('properties-reader');
const path = require('path');
const properties = new propertiesReader(path.join(__dirname,'../../config/email.properties'));



const service = properties.get('nodemailer.service');
/*const port = properties.get('nodemailer.port');
const host = properties.get('nodemailer.host');*/

const username = properties.get('nodemailer.auth.username');
const password = properties.get('nodemailer.auth.password');

var Hogan = require('hogan.js');
var fs = require('fs');


var template = fs.readFileSync('./templates/emailotp/tempOtp.hjs','utf-8');
var compiledTemplate = Hogan.compile(template); 

var transporter = nodemailer.createTransport({
	service: service,
    auth: {
        user: username,
        pass: password
    }
})

var mailOptions = {
     from: 'IM Linkeddots <no-reply@linkeddots.com>',
     to: 'samkumaran@linkeddots.com',
     subject: 'IM Notification ',
     //html:compiledTemplate.render({name:"Sam"});
}


transporter.sendMail(mailOptions, function (err, res) {
    if(err){
         console.log(err);
     } else {
         console.log('Email Sent');
     }
});


exports.sendNotification = function (msg){
	mailOptions.html=compiledTemplate.render(msg);
	transporter.sendMail(mailOptions, function (err, res) {
	    if(err){
	         console.log(err);
	     } else {
	         console.log('Notification Sent ...');
	     }
	});
}



