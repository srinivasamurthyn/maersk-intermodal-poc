const Sequelize = require('sequelize');
const propertiesReader = require('properties-reader');
const properties = new propertiesReader('../config/db.properties');


//DB Config
var dbhost = properties.get('db.connection.host');
var dbport = properties.get('db.connection.port');
var dbdialect = properties.get('db.connection.dialect');
var dbname = properties.get('db.connection.dbname');
var dbuser = properties.get('db.connection.user');
var dbpassword = properties.get('db.connection.password');


const sequelize = new Sequelize(dbname, dbuser, dbpassword, {
  host: dbhost,
  dialect: dbdialect,
  operatorsAliases: false,
  port: dbport,
  dialectOptions: {
	    requestTimeout: 600000,
	    encrypt: true
  },
});

sequelize.authenticate().then(() => {
  console.log('DB Connection has been established successfully.');
}).catch(err => {
  console.error('Unable to connect to the database:', err);
});


exports.getSequelize=function(){
	return sequelize;
};
