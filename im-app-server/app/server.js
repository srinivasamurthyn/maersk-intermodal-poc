var app = require('./app');
var cluster = require('cluster');
var numCPUs = require('os').cpus().length;
var propertiesReader = require('properties-reader');
var properties = new propertiesReader('../config/server.properties');

//config & start server
process.env.PORT=properties.get('server.port');

if (cluster.isMaster) {
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
  });
} else {
  	app.set('port', process.env.PORT || 8080);
  	var server = app.listen(app.get('port'), function() {
  	  console.log('Application running on port' + server.address().port);
  	});
}