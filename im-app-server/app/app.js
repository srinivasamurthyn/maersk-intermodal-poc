const express    = require('express');
const http = require('http');
const path = require('path');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const compression = require('compression');
const customerapi = require('./routes/customerapi');
const vendorapi = require('./routes/vendorapi');
const trailerapi = require('./routes/trailerapi');
const carrierapi = require('./routes/carrierapi');
const workorderapi = require('./routes/workorderapi');
const gpsdataapi = require('./routes/gpsdataapi');
const containerapi = require('./routes/containerapi');
const containerprefixapi = require('./routes/containerprefixapi');
const configapi = require('./routes/configapi');
const ContainerMasterViewapi = require('./routes/ContainerMasterViewapi');
const authcontroller = require('./auth/authcontroller');
const app = module.exports = express();
const router = express.Router();
const crouter = express.Router();

//ui setup
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb',extended: true}));
app.use(methodOverride());
app.use(compression());
app.use(express.static('../ui'));



router.get('/', function (req, res) {
	  res.send('Welcome to Linkeddots App.');
});

//Check Service oAuth
router.use(function(req,res,next){
	authcontroller.verifyHeaderToken(req,res,next);
});

//basicAuth
crouter.post('/useraccount/user', authcontroller.create);
crouter.post('/useraccount/login', authcontroller.login);

//Customer
router.get('/customer/list/all', customerapi.findAll);
router.get('/customer/:id', customerapi.findById);
router.get('/customer/customerno/:no', customerapi.findByCustomerNo);

//Vendor
router.get('/vendor/list/all', vendorapi.findAll);
router.get('/vendor/:id', vendorapi.findById);

//Trailer
crouter.post('/trailer', trailerapi.upsertTrailer);
crouter.get('/trailer/:id', trailerapi.findById);
crouter.get('/trailer/stoptrip/:trailerNo',trailerapi.stopTripByTrailerNo);
router.get('/trailer/trailerno/:trailerId', trailerapi.findByTrailerId);
router.get('/trailer/list/active', trailerapi.listActiveTrailer);


//Container
crouter.post('/container', containerapi.upsertContainer);
crouter.get('/container/unloaded/:containerNo', containerapi.unloadedContainer);
crouter.get('/container/list/all', containerapi.findAll);
crouter.get('/container/listbystatus/:status/:page/:limit', containerapi.findByStatus);
router.get('/container/listbystatus/country/:country/:status', containerapi.findContainerStatusByCountry);
crouter.get('/container/:id', containerapi.findById);
crouter.get('/container/trailer/:trailerId', containerapi.findByTrailerId);
router.get('/container/country/list/:country', containerapi.findContainerByCountry);
router.get('/container/summary/count', containerapi.countBySummary);
router.get('/container/livestatus/:status', containerapi.listByLiveStatus);
router.get('/container/summary/count/country/:country', containerapi.countSummaryByCountry);
router.get('/container/livestatus/country/:country/:status', containerapi.listLiveStatusByCountry);
router.get('/container/containerno/:containerNo', containerapi.findByContainerNo);

//Container Search
crouter.get('/container/search/:key/:value/:status/:page/:limit', containerapi.searchContainer);

//Container Prefix
crouter.get('/containerprefix/list/all', containerprefixapi.findAll);

//ContainerMasterView
router.get('/ContainerMasterViewapi/livestatus/:status', ContainerMasterViewapi.listByLiveStatus);
router.get('/ContainerMasterViewapi/livestatus/country/:country/:status', ContainerMasterViewapi.listLiveStatusByCountry);
crouter.get('/ContainerMasterViewapi/listbystatus/:status', ContainerMasterViewapi.findByStatus);
router.get('/ContainerMasterViewapi/summary/count', ContainerMasterViewapi.countBySummary);

//Carrier
router.get('/carrier/list/all', carrierapi.findAll);
router.get('/carrier/:id', carrierapi.findById);
router.get('/carrier/carriercode/:code', carrierapi.findByCarrierCode);

//WorkOrder
router.post('/workorder', workorderapi.upsert);
router.post('/workorder/bulk', workorderapi.upsertBulk);
router.get('/workorder/list/all', workorderapi.findAll);
router.get('/workorder/:id', workorderapi.findById);
router.get('/workorder/workorder/:no', workorderapi.findByWorkOrderNo);
router.get('/workorder/country/:country', workorderapi.findByCountry);
router.get('/workorder/listbycontainer/:containerNo', workorderapi.findByContainer);
router.get('/workorder/country/code/:code/:region', workorderapi.findByCountryCode);

router.get('/workorder/remove/:workOrderNo', workorderapi.deactivateWokOrder);



//gpsData
crouter.post('/gpsdata/bulk', gpsdataapi.upsertGpsdata);
router.get('/gpsdata/list/all', gpsdataapi.findAll);
router.get('/gpsdata/workorder/:id', gpsdataapi.findByWorkOrder);
router.get('/gpsdata/trailer/range/startend/:trailerId/:startTime/:endTime', gpsdataapi.trailerByStartEnd);


//config
router.get('/hearmapi/api', configapi.getHearMapInfo);



app.use('/controller', router);
app.use('/basic', crouter);

morgan('combined', {
	  skip: function (req, res) { return res.statusCode < 400; }
});

module.exports = app;

