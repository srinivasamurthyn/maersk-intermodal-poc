var vendordbservices = require('../dbservices/vendor');

exports.findAll = function(req,res) {
	vendordbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}


exports.findById = function(req,res) {
	vendordbservices.findById(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.id);
}

