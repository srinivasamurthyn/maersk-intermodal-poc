var containerdbservices = require('../dbservices/ContainerMasterView');


exports.findAll = function(req,res) {
	containerdbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}

exports.findByStatus = function(req,res) {
	containerdbservices.findByStatus(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.status);
}

exports.findById = function(req,res) {
	containerdbservices.findById(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.id);
}



exports.listByLiveStatus = function(req,res) {
	containerdbservices.listByLiveStatus(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.status);
}


exports.listLiveStatusByCountry = function(req,res) {
	containerdbservices.listLiveStatusByCountry(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.country,req.params.status);
}



exports.countBySummary = function(req,res) {
	containerdbservices.countBySummary(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}

