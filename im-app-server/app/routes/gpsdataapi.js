var gpsdatadbservices = require('../dbservices/gpsdata');


exports.upsertGpsdata = function(req,res) {
	
	var body = req.body;
	
	if(Array.isArray(body)){
		for(i=0;i<body.length;i++){
			var gpsdata = {
					trailerId : body[i].trailerId,
					gpslat : body[i].gpslat,
					gpslon : body[i].gpalon,
					gpsdate : body[i].gpsdate,
					gpsdateepoch : body[i].gpsdateepoch,
					event : body[i].event,
			}
			
			gpsdatadbservices.upsert(gpsdata,function(err, data) {
			    console.log('GPS Added ...')
			});
		}
		
		return res.json({msg:"Bulk GPS Added ..."});
		
	}else{
		var gpsdata = {
				trailerId : body.trailerId,
				gpslat : body.gpslat,
				gpalon : body.gpalon,
				gpsdate : body.gpsdate,
				gpsdateepoch : body.gpsdateepoch,
				event : body.event
		}
			
		gpsdatadbservices.upsert(gpsdata,function(err, data) {
		    if (!err) {
		       	return res.json(data);
		    }else{
		    	return res.json({error:err});
		    }
		});
	}
}


exports.findAll = function(req,res) {
	gpsdatadbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}


exports.findByWorkOrder = function(req,res) {
	gpsdatadbservices.findByWorkOrder(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.id);
}



exports.trailerByStartEnd = function(req,res) {
	gpsdatadbservices.trailerByStartEnd(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.trailerId,req.params.startTime,req.params.endTime);
}










