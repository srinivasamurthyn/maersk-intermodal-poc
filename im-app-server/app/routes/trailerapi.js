var trailerdbservices = require('../dbservices/trailer');



exports.upsertTrailer = function(req,res) {
	
	var resBody = req.body;
	if(resBody && resBody.trailerNo){
		trailerdbservices.findActiveTrailerById(function(err, data) {
			if (data) {
	        	return res.json({error:"Trailer is in active state"});
	        }else{
	        	var trailerObj = {
	        			trailerNo:resBody.trailerNo,
	    				status:resBody.status,
	    				startTime:resBody.startTime,
	    				endTime:resBody.endTime,
	    		};
	    		
	    		trailerdbservices.upsert(trailerObj,function(err, data) {
	    	        if (!err) {
	    	        	return res.json(data);
	    	        } else {
	    	        	return res.json({error:err});
	    	        }
	    	    });
	        }
	    },resBody.trailerNo);
	}else{
		return res.json({error:"Enter Valid Trailer No"});
	}
	
}

exports.stopTripByTrailerNo = function(req,res) {
	trailerdbservices.stopTripByTrailerNo(function(err, data) {
	    if (!err) {
	    	return res.json(data);
	    } else {
	        return res.json({error:err});
	    }
	},req.params.trailerNo);
}



exports.findAll = function(req,res) {
	trailerdbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}


exports.findById = function(req,res) {
	trailerdbservices.findById(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.id);
}



exports.findByTrailerId = function(req,res) {
	trailerdbservices.findByTrailerId(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.trailerId);
}

exports.listActiveTrailer = function (req,res) {
	trailerdbservices.listActiveTrailer(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}




