var propertiesReader = require('properties-reader');
var properties = new propertiesReader('../config/map.properties');
var hearConfig = {
	appId : properties.get('hear.map.appId'),
	appCode : properties.get('hear.map.appCode'),
	drivingMode : properties.get('hear.map.drivingMode')
}

exports.getHearMapInfo = function (req,res){
	return res.json(hearConfig);
}