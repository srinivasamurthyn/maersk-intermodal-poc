var workorderdbservices = require('../dbservices/workorder');



exports.upsert = function(req,res) {
	
	var resBody = req.body;
	if(resBody && resBody.workOrderNo){
		var workOrderObj = {
				workOrderId : resBody.workOrderNo+"_"+resBody.containerNo,
				workOrderNo : resBody.workOrderNo,
				woCreatedYearWeek : resBody.woCreatedYearWeek,
				woMode : resBody.woMode,
				woCompleteYearWeek : resBody.woCompleteYearWeek,
				woCompleteDate : resBody.woCompleteDate,
				woDetailType : resBody.woDetailType,
				equipmentNumbers : resBody.equipmentNumbers,
				beginAddressline1 : resBody.beginAddressline1,
				beginAddressline2 : resBody.beginAddressline2,
				beginAddressline3 : resBody.beginAddressline3,
				beginZip : resBody.beginZip,
				endAddressline1 : resBody.endAddressline1,
				endAddressline2 : resBody.endAddressline2,
				endAddressline3 : resBody.endAddressline3,
				endZip : resBody.endZip,
				countryCode : resBody.countryCode,
				stateCode : resBody.stateCode,
				clearencePort : resBody.clearencePort,
				blNumber : resBody.blNumber,
				blNumberOriginal : resBody.blNumberOriginal,
				comment : resBody.comment,
				commodityDescription : resBody.commodityDescription,

				departureDate : resBody.departureDate,
				arrivalDate : resBody.arrivalDate,
				
				carrierCarrierCode : resBody.carrier,
				customerCustomerNumber : resBody.customer,
				vendorVendorId : resBody.vendor,
				
				containerNo : resBody.containerNo,
				geoCodeStatus : "NO",
				linkWoToConStatus : "NO",
				
				status : "ACTIVE"
				
		};
		
		workorderdbservices.upsert(workOrderObj,function(err, data) {
	        if (!err) {
	        	return res.json(data);
	        } else {
	        	return res.json({error:err});
	        }
	    });
	}else{
		return res.json({error:"Enter Valid WorkOrder Id"});
	}
	
}


exports.upsertBulk = function(req,res) {
	var woArray = req.body.woArray;
	
	workorderdbservices.upsertBulk(woArray,function(err, data) {
        if (!err) {
        	return res.json(data);
        } else {
        	return res.json({error:err});
        }
    });
}


exports.findAll = function(req,res) {
	workorderdbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}


exports.findById = function(req,res) {
	workorderdbservices.findById(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json([{error:err}]);
        }
    },req.params.id);
}


exports.findByWorkOrderNo = function(req,res) {
	workorderdbservices.findByWorkOrderNo(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.no);
}


exports.findByCountry = function(req,res) {
	workorderdbservices.findByCountry(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.country);
}


exports.findByContainer = function(req,res) {
	workorderdbservices.findByContainer(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.containerNo);
}


exports.findByCountryCode = function(req,res) {
	workorderdbservices.findByCountryCode(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.code,req.params.region);
}

exports.deactivateWokOrder = function(req,res) {
	workorderdbservices.deactivateWokOrder(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.workOrderNo);

}

