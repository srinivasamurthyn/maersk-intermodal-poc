var containerprefixdbservices = require('../dbservices/containerprefix');

exports.findAll = function(req,res) {
	containerprefixdbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}
