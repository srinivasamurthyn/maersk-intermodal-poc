var containerdbservices = require('../dbservices/container');




exports.upsertContainer = function(req,res) {
	
	var resBody = req.body;
	if(resBody && resBody.containerNo){
		containerdbservices.findActiveContainerById(function(err, data) {
			if (data && data[0]) {
	        	return res.json({error:"Container is already loaded"});
	        }else{
	        	var containerObj = {
	        			containerNo:resBody.containerNo,
	    				status:resBody.status,
	    				loadedTime:new Date().getTime(),
	    				loadedDate:parseInt(new Date(new Date().setHours(0,0,0,0)).getTime()),
	    				unloadedTime:null,
	    				trailerTrailerId : resBody.trailerId,
	    				geoCodeStatus : "NO",
	    				linkConToWoStatus : "NO"
	    		};
	    		
	        	containerdbservices.upsert(containerObj,function(err, data) {
	    	        if (!err) {
	    	        	return res.json(data);
	    	        } else {
	    	        	return res.json({error:err});
	    	        }
	    	    });
	        }
	    },resBody.containerNo);
	}else{
		return res.json({error:"Enter Valid Container No"});
	}
	
}


exports.unloadedContainer = function(req,res) {
	containerdbservices.unloadedContainer(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.containerNo);
}

exports.findAll = function(req,res) {
	containerdbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}


exports.findById = function(req,res) {
	containerdbservices.findById(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.id);
}

exports.findByStatus = function(req,res) {
	var page = 1;
	var limit = 500;
	if(req.params.page){
		page = parseInt(req.params.page);
	}
	
	if(req.params.limit && req.params.limit < 500){
		limit = parseInt(req.params.limit);
	}
	
	containerdbservices.findByStatus(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.status,page,limit);
	
}


exports.findByTrailerId = function(req,res) {
	containerdbservices.findByTrailerId(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.trailerId);
}


exports.findByContainerNo = function(req,res) {
	containerdbservices.findByContainerNo(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.containerNo);
}


exports.countBySummary = function(req,res) {
	containerdbservices.countBySummary(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}

exports.countSummaryByCountry = function(req,res) {
	containerdbservices.countSummaryByCountry(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.country);
}


exports.listByLiveStatus = function(req,res) {
	containerdbservices.listByLiveStatus(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.status);
}


exports.listLiveStatusByCountry = function(req,res) {
	containerdbservices.listLiveStatusByCountry(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.country,req.params.status);
}


exports.findContainerByCountry = function (req,res) {
	containerdbservices.findContainerByCountry(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.country);
}

exports.findContainerStatusByCountry = function (req,res) {
	containerdbservices.findContainerStatusByCountry(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.country,req.params.status);
}



exports.searchContainer = function (req,res) {
	
	var page = 1;
	var limit = 500;
	if(req.params.page){
		page = parseInt(req.params.page);
	}
	
	if(req.params.limit && req.params.limit < 500){
		limit = parseInt(req.params.limit);
	}
	
	switch (req.params.key) {
	case "containerNo":
		containerdbservices.searchContainerByContainerNo(function(err, data) {
	        if (!err) {
	        	return res.json(data);
	        }else{
	        	return res.json({error:err});
	        }
	    },req.params.key,req.params.value,req.params.status,page,limit);
		break;
	case "vendor":
		containerdbservices.searchContainerByVendor(function(err, data) {
	        if (!err) {
	        	return res.json(data);
	        }else{
	        	return res.json({error:err});
	        }
	    },req.params.key,req.params.value,req.params.status,page,limit);
		break;
	case "worder":
		containerdbservices.searchContainerByWorkorder(function(err, data) {
	        if (!err) {
	        	return res.json(data);
	        }else{
	        	return res.json({error:err});
	        }
	    },req.params.key,req.params.value,req.params.status,page,limit);
		break;
	default:
		return res.json({error:"key is not corrrect ..."});
		break;
	}
	
	
	

	
	
}




