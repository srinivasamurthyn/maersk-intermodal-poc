var carrierdbservices = require('../dbservices/carrier');

exports.findAll = function(req,res) {
	carrierdbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}


exports.findById = function(req,res) {
	carrierdbservices.findById(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.id);
}



exports.findByCarrierCode = function(req,res) {
	carrierdbservices.findByCarrierCode(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.code);
}
