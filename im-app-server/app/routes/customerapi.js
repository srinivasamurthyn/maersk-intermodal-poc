var customerdbservices = require('../dbservices/customer');

exports.findAll = function(req,res) {
	customerdbservices.findAll(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    });
}


exports.findById = function(req,res) {
	customerdbservices.findById(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.id);
}


exports.findByCustomerNo = function(req,res) {
	customerdbservices.findByCustomerNo(function(err, data) {
        if (!err) {
        	return res.json(data);
        }else{
        	return res.json({error:err});
        }
    },req.params.no);
}


