const Auth = require('./auth');
const Jwt = require('jsonwebtoken');
const propertiesReader = require('properties-reader');
const authProperties = new propertiesReader('../config/auth.properties');
const privateKey = authProperties.get("auth.privateKey")
const randomstring = require("randomstring")
const useraccountdbservices = require('../dbservices/useraccount');

exports.create = function (req, res){
	req.body.password = Auth.encrypt(req.body.password);
	useraccountdbservices.findByUserName(function(err, user) {
		if(user){
			return res.json({error:"Username already exists ..."});
		}
		
		if(user.password !== user.cpassword){
			return res.json({error:"Password not match .... "});
		}
		
		if(user.cpassword.length < 8) {
			return res.json({error:"Your password must be at least 8 characters"}); 
	    }
	    
		if(user.cpassword.search(/[a-z]/i) < 0) {
	    	return res.json({error:"Your password must contain at least one letter."});
	    }
	    
		if(user.cpassword.search(/[0-9]/) < 0) {
	    	return res.json({error:"Your password must contain at least one digit."}); 
	    }
		
		var user = {
			userName : req.body.userName,
			userType : req.body.userType,
			password : req.body.password,
			userEmail : req.body.userEmail,
			userEnable : false,
		};
		
		useraccountdbservices.upsert(function(err, user) {
			console.log('User............');
			if (!err) {
				//Auth.sentMailVerificationLink(user,Jwt.sign(tokenData, privateKey));
				return res.json({success:"Please wait admin confirm your account ..."});
			} else {
				return res.json({error:"Creating account failed ..."});
			}
		},user);
   },req.body.userName);
}

exports.login = function (req, res){
	
	var reqAuth =req.headers.authorization.split(" ");
	var buf = new Buffer(reqAuth[1], 'base64');
	
	var auth = buf.toString('utf8');
	var authorization = auth.split(":");
	var userName = authorization[0];
	var password = authorization[1];
	
	useraccountdbservices.findByUserName(function(err, user) {
		if (!err) {
			if(!user){
	         	return res.json({error:"Invalid username or password"});
	        }
						
			if (password === Auth.decrypt(user.password)) {
                if(!user.userEnable){
                	return res.json({error:"Please wait admin verifiy your account"});
                }else{	
                	var tokenData = {
    	                username: user.userName,
    	                id: user.id,
    	                userType : user.userType
    	            };
    	            
                	var result = {
    	                userName : user.userName,
    	                token : Jwt.sign(tokenData, privateKey),
    	                userType : user.userType
    	            };
    	               
    	            res.set({
    	            	'token': Jwt.sign(tokenData, privateKey),
    	               	'userType': user.userType,
    	               	'landingRole':user.landingRole,
    	               	'vendorAccess':user.vendorAccess
    	            });
    	            return res.json({});
                }
            }else{
            	return res.json({error:"invalid username or password"});
            }
		}
	},userName);
}




exports.verifyHeaderToken = function (req,res,next){
	if(req.headers.authorization){
		var token=req.headers.authorization;
		Jwt.verify(token, privateKey ,function (err,decode){
			if(!decode || !decode.username){
				res.status(401).json({error:'Invalid Token'});
			}else{
				next();
			}
		});
	}else{
		res.status(401).json({error:'Invalid Token'});
	}
}