const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const useraccount = sequelizeCon.define('useraccount', {
	id : { type : Sequelize.INTEGER,primaryKey: true,autoIncrement: true}, 
	userType : Sequelize.STRING, //CUSTOMER,CARE,MANAGER,CARRIER
	userName : Sequelize.STRING,
	password : Sequelize.STRING,
	userEnable : Sequelize.BOOLEAN,
	userEmail : Sequelize.STRING,
	landingRole : Sequelize.STRING,
	vendorAccess : Sequelize.STRING // ALL OR ID FOR VENDOR
},{timestamps: false});

useraccount.sync();

exports.useraccount = useraccount;


