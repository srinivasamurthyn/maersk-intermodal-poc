const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const carrier = require('./carrier.js').carrier;
const customer = require('./customer.js').customer;
const vendor = require('./vendor.js').vendor;

const workorder = sequelizeCon.define('workorder', {
	workOrderId : Sequelize.STRING,
	workOrderNo : { type : Sequelize.STRING, primaryKey : true },
	woCreatedYearWeek : Sequelize.STRING,
	woMode : Sequelize.STRING,
	woCompleteYearWeek : Sequelize.STRING,
	woCompleteDate : Sequelize.STRING,
	woDetailType : Sequelize.STRING,
	equipmentNumbers : Sequelize.STRING,
	beginAddressline1 : Sequelize.STRING,
	beginAddressline2 : Sequelize.STRING,
	beginAddressline3 : Sequelize.STRING,
	beginZip : Sequelize.STRING,
	

	
	endAddressline1 : Sequelize.STRING,
	endAddressline2 : Sequelize.STRING,
	endAddressline3 : Sequelize.STRING,
	endZip : Sequelize.STRING,
	countryCode : Sequelize.STRING,
	stateCode : Sequelize.STRING,
	clearencePort : Sequelize.STRING,
	blNumber : Sequelize.STRING,
	blNumberOriginal : Sequelize.STRING,
	comment : Sequelize.STRING,
	commodityDescription : Sequelize.STRING,
	
	departureDate : Sequelize.BIGINT,
	arrivalDate : Sequelize.BIGINT,
	
	
	
	
	//geo loc
	beginLat : Sequelize.DOUBLE,
	beginLon :Sequelize.DOUBLE,
	
	endLat : Sequelize.DOUBLE,
	endLon :Sequelize.DOUBLE,
	
	//geocode status
	geoCodeStatus : Sequelize.STRING,
	
	
	//container
	containerNo : { type : Sequelize.STRING, primaryKey : true },
	containerId : Sequelize.BIGINT,
	linkWoToConStatus :  Sequelize.STRING, // YES,NO
	
	status : Sequelize.STRING // ACTIVE,INACTIVE
	
},{timestamps: false});



//Foreign keys
workorder.belongsTo(carrier,{onDelete: "CASCADE",as: 'carrier'});
workorder.belongsTo(customer,{onDelete: "CASCADE",as: 'customer'});
workorder.belongsTo(vendor,{onDelete: "CASCADE",as: 'vendor'});

workorder.sync();

exports.workorder = workorder;