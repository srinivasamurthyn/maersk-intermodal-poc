const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const customer = sequelizeCon.define('customer', {
	customerNumber : { type : Sequelize.STRING, primaryKey : true},
	customerName : Sequelize.STRING,
	addressLine1 : Sequelize.STRING,
	addressLine2 : Sequelize.STRING,
	addressLine3 : Sequelize.STRING,
	state : Sequelize.STRING,
	country : Sequelize.STRING,
	zipCode : Sequelize.STRING,
	emailAddress : Sequelize.STRING,
	contactNo : Sequelize.BIGINT,
	vipFlag : Sequelize.BOOLEAN,
},{timestamps: false});

customer.sync();

exports.customer = customer;













