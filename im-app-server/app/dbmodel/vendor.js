const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const vendor = sequelizeCon.define('vendor', {
	id : { type : Sequelize.BIGINT, primaryKey: true,autoIncrement: true}, 
	name : Sequelize.STRING,
	enable : Sequelize.STRING,
	status : Sequelize.STRING,
	lastRunTime : Sequelize.STRING,
	interval : Sequelize.STRING,
	config : Sequelize.STRING,
},{timestamps: false});

vendor.sync();
exports.vendor = vendor;