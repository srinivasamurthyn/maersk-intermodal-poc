const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const trailer = require('./trailer.js').trailer;

const container = sequelizeCon.define('container', {
	containerId : { type : Sequelize.BIGINT, primaryKey : true , autoIncrement: true},
	containerNo :  Sequelize.STRING,
	status : Sequelize.STRING,
	loadedDate : Sequelize.BIGINT,
	loadedTime : Sequelize.BIGINT,
	unloadedDate : Sequelize.BIGINT,
	unloadedTime : Sequelize.BIGINT,
	
	
	sloclat : Sequelize.DOUBLE,
	sloclon : Sequelize.DOUBLE,
	
	eloclat : Sequelize.DOUBLE,
	eloclon : Sequelize.DOUBLE,
	
	ctryCode : Sequelize.STRING,
	geoCodeStatus : Sequelize.STRING,
	
	sgfStatus : Sequelize.STRING, // UNKNOWN ,IN ,EXITED start geofence status
	sgfInTime :  Sequelize.BIGINT,
	
	egfStatus : Sequelize.STRING, // UNKNOWN ,IN ,EXITED end geofence status
	egfInTime :  Sequelize.BIGINT,
	egfExitTime :  Sequelize.BIGINT,
	
	//eta
	etaDistance : Sequelize.STRING, 
	etaDuration : Sequelize.STRING, 
	
	//linkConToWoStatus
	linkConToWoStatus : Sequelize.STRING, // YES,NO
	workOrderId : Sequelize.STRING,
	worder : Sequelize.STRING,
	vendor : Sequelize.STRING,
	lastSeenTime :  Sequelize.BIGINT
	
},{timestamps: false});

container.belongsTo(trailer,{onDelete: "CASCADE",as: 'trailer'});

container.sync();

exports.container = container;