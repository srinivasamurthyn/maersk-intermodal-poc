const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();

const containerprefix = sequelizeCon.define('containerprefix', {
	
	id : { type : Sequelize.STRING, primaryKey : true },
	name  :  Sequelize.STRING,

},{timestamps: false});

containerprefix.sync();

exports.containerprefix = containerprefix;