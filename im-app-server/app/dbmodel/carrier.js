const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const carrier = sequelizeCon.define('carrier', {
	carrierCode : { type : Sequelize.STRING, primaryKey : true },
	carrierName : Sequelize.STRING,
	addressLine1 : Sequelize.STRING,
	addressLine2 : Sequelize.STRING,
	addressLine3 : Sequelize.STRING,
	state : Sequelize.STRING,
	country : Sequelize.STRING,
	zipCode : Sequelize.STRING,
	emailAddress : Sequelize.STRING,
	contactNo : Sequelize.BIGINT
},{timestamps: false});

carrier.sync();

exports.carrier = carrier;
