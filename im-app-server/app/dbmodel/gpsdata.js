const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const gpsdata = sequelizeCon.define('gpsdata', {
	id : { type : Sequelize.BIGINT, primaryKey : true , autoIncrement: true},
	vendor : Sequelize.STRING,
	worder : Sequelize.STRING,
	containerno : Sequelize.STRING,
	trailerId : Sequelize.STRING,
	bookingno : Sequelize.STRING,
	gpslat : Sequelize.DOUBLE,
	gpslon : Sequelize.DOUBLE,
	gpsdate : Sequelize.DATE,
	event : Sequelize.STRING,
	eventstatus : Sequelize.STRING,
	apikeyid : Sequelize.STRING,
	sourceip : Sequelize.STRING,
	reqtime : Sequelize.STRING,
	gpsdateepoch : Sequelize.BIGINT
},{timestamps: false});

gpsdata.sync();

exports.gpsdata = gpsdata;


