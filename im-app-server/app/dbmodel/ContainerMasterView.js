const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const ContainerMasterView = sequelizeCon.define('ContainerMasterView', {
	containerId : Sequelize.BIGINT,
	containerNo : Sequelize.STRING,
	status : Sequelize.STRING,
	/*loadedDate : Sequelize.BIGINT,*/
	loadedTime : Sequelize.BIGINT,
	/*unloadedDate : Sequelize.BIGINT,*/
	unloadedTime : Sequelize.BIGINT,
	
	/*sloclat : Sequelize.DOUBLE,
	sloclon : Sequelize.DOUBLE,
	
	eloclat : Sequelize.DOUBLE,
	eloclon : Sequelize.DOUBLE,
	
	ctryCode : Sequelize.STRING,
	geoCodeStatus : Sequelize.STRING,*/
	
	sgfStatus : Sequelize.STRING, // UNKNOWN ,IN ,EXITED start geofence status
	/*sgfInTime :  Sequelize.BIGINT,*/
	
	egfStatus : Sequelize.STRING, // UNKNOWN ,IN ,EXITED end geofence status
	/*egfInTime :  Sequelize.BIGINT,
	egfExitTime :  Sequelize.BIGINT,
	*/
	//eta
	/*etaDistance : Sequelize.STRING, 
	etaDuration : Sequelize.STRING, */
	
	//linkConToWoStatus
	/*linkConToWoStatus : Sequelize.STRING, // YES,NO
	workOrderId : Sequelize.STRING,*/
	vendor : Sequelize.STRING,
	/*worder : Sequelize.STRING,*/
	/*
	trailerTrailerId : Sequelize.BIGINT,
	trailerNo : Sequelize.STRING,*/
	firstSeen : Sequelize.BIGINT,
	lastSeen : Sequelize.BIGINT,
	workOrderNo : Sequelize.STRING
	

},{treatAsView: true,timestamps: false});

//ContainerMasterView.sync();

exports.ContainerMasterView = ContainerMasterView;

