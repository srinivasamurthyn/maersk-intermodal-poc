const Sequelize = require('sequelize');
const sequelizeCon = require('../dbconnection/db.js').getSequelize();

var trailer = sequelizeCon.define('trailer', {
	trailerId : { type : Sequelize.BIGINT, primaryKey : true ,autoIncrement: true},
    trailerNo : Sequelize.STRING, 
    status : Sequelize.STRING,
    startTime : Sequelize.BIGINT,
    endTime : Sequelize.BIGINT,
    
    sloclat : Sequelize.DOUBLE,
	sloclon : Sequelize.DOUBLE,
	slocgcStatus : Sequelize.STRING, //NO, YES ,FAILED geocoding status
	sCtryCode : Sequelize.STRING,
	
	eloclat : Sequelize.DOUBLE,
	eloclon : Sequelize.DOUBLE,
	elocgcStatus : Sequelize.STRING, //NO, YES ,FAILED geocoding status
	eCtryCode : Sequelize.STRING,
    
},{timestamps: false});

trailer.sync();

exports.trailer = trailer;