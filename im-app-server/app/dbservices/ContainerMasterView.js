var containerdb = require('../dbmodel/ContainerMasterView.js').ContainerMasterView;
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const Op = sequelizeCon.Op

exports.findAll = function(callback) {
	containerdb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findById = function(callback,id) {
	containerdb.findOne({ where : {
		containerId : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.findByStatus = function(callback,status) {
	var today = parseInt(new Date(new Date().setHours(0,0,0,0)).getTime());
	containerdb.findAll({
		  where : {
			  status : status
			 /* ,
			  lastSeen : {
					[Op.gte]: parseInt(today),
			  }*/
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
	console.log(err);
    	return callback(err);
    });
};


exports.listByLiveStatus = function(callback,status) {
	if(status === "STARTED"){
		containerdb.findAll({where: {sgfStatus: 'IN'} , egfStatus : { [Op.eq]:null} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "INTRANSIT"){
		containerdb.findAll({where: {sgfStatus: 'EXITED' , egfStatus : { [Op.eq]:null}} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "REACHED"){
		containerdb.findAll({where: {egfStatus: 'IN' } }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}
};



exports.listLiveStatusByCountry = function(callback,ctryCode,status) {
	if(status === "STARTED"){
		containerdb.findAll({where: {ctryCode : ctryCode,sgfStatus: 'IN'} , egfStatus : { [Op.eq]:null} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "INTRANSIT"){
		containerdb.findAll({where: {ctryCode : ctryCode,sgfStatus: 'EXITED' , egfStatus : { [Op.eq]:null}} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "REACHED"){
		containerdb.findAll({where: {ctryCode : ctryCode,egfStatus: 'IN' } }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}
};


exports.countBySummary = function(callback) {
	containerdb.count({ where: {'status': 'LOADED' } }).then(loadStatus => {
		containerdb.count({ where: {'status': 'UNLOADED' } }).then(unloadStatus => {
			containerdb.count({ where: {'sgfStatus': 'IN'} , egfStatus : { [Op.eq]:null} }).then(started => {
				containerdb.count({ where: {'sgfStatus': 'EXITED' , egfStatus : { [Op.eq]:null}} }).then(exited => {
					containerdb.count({ where: {'egfStatus': 'IN' } }).then(reached => {
						return callback(null, {loadStatus:loadStatus,unloadStatus:unloadStatus,started:started,inTransit:exited,reached:reached});
					});
				});
			});
		});
	})
};



