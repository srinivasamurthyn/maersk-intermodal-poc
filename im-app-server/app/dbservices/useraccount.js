var useraccountdb = require('../dbmodel/useraccount.js').useraccount;
const sequelizeCon = require('../dbconnection/db.js').getSequelize();

exports.upsert = function(callback,user){
	sequelizeCon.transaction().then(function(t) {
		useraccountdb.create(user,{
	        transaction: t
	    }).then(function() {
	        t.commit();
	        return callback(null,{msg:"Success"});
	    }).catch(function(error) {
	        console.log(error);
	        return callback(error);
	    });
	});
};

exports.findAll = function(callback) {
	useraccountdb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByUserName = function(callback,userName) {
	useraccountdb.findOne({ where : {
		userName : userName
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};
