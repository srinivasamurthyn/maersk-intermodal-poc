const gpsdatadb = require('../dbmodel/gpsdata.js').gpsdata;
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const Op = sequelizeCon.Op

exports.upsert = function(gpsdata,callback) {
	sequelizeCon.transaction().then(function(t) {
		gpsdatadb.create(gpsdata,{
	        transaction: t
	    }).then(function() {
	        t.commit();
	        return callback(null,{msg:"Success"});
	    }).catch(function(error) {
	        console.log(error);
	        return callback(error);
	    });
	});
};


exports.findAll = function(callback) {
	gpsdatadb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByWorkOrder = function(callback,id) {
	gpsdatadb.findOne({ where : {
		worder : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};



exports.trailerByStartEnd = function(callback,trailerId,startTime,endTime) {
	gpsdatadb.findAll({ where: {
		trailerId : trailerId,
		gpsdateepoch: {
			[Op.lte]: parseInt(endTime), 
			[Op.gte]: parseInt(startTime), 
		}
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};
