var workorderdb = require('../dbmodel/workorder.js').workorder;
const carrier = require('./carrier.js').carrier;
const customer = require('./customer.js').customer;
const vendor = require('./vendor.js').vendor;
const sequelizeCon = require('../dbconnection/db.js').getSequelize();


exports.upsert = function(workorder,callback) {
	

	workorderdb.findOne({ where : {
		workOrderNo : workorder.workOrderNo, containerNo:workorder.containerNo
	}}).then(function (data) {
		if(data){
    		return callback(null,{msg:"Already Exist"});
    	}else{
    		sequelizeCon.transaction().then(function(t) {
    			workorderdb.create(workorder,{
    		        transaction: t
    		    }).then(function() {
    		    	
//    		    	console.log(wo);
//    		    	var con = {containerNo:workorder.containerNo};
//    		    	wo.addContainer(t, { containerContainerId: workorder.containerNo });
    		    		
    		    		t.commit();
    		        return callback(null,{msg:"Success"});
    		    }).catch(function(error) {
    		        console.log('err1',error);
    		        return callback(error);
    		    });
    		});
    	}		
    }).catch(function(err) {
    	console.log('err2',err);
    	return callback(err);
    });

	
	

};

exports.upsertBulk = function(workorders,callback) {
	workorderdb.bulkCreate(workorders)
	.then(function (data) {
		console.log(data.length)
		return callback(null, data);
    }).catch(function(err) {
		console.log('err')
		console.log(err)
    	return callback(err);
    });
	
};





exports.findAll = function(callback) {
	workorderdb.findAll({ where : {
		status : "ACTIVE"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findById = function(callback,id) {
	workorderdb.findOne({ where : {
		workOrderId : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.findByWorkOrderNo = function(callback,no) {
	workorderdb.findOne({ where : {
		workOrderNo : no
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};




exports.findByCountry = function(callback,country) {
	workorderdb.findAll({ where : {
		countryCode : country,
		status : "ACTIVE"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByContainer = function(callback,containerNo) {
	workorderdb.findOne({ where : {
		containerNo : containerNo,
		status : "ACTIVE"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByCountryCode = function(callback,code,region) {
	workorderdb.findAll({ where : {
		countryCode : code , stateCode:region, status : "ACTIVE"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.deactivateWokOrder = function(callback,workOrderNo) {
	
	sequelizeCon.transaction().then(function(t) {
		workorderdb.update({
			status : "INACTIVE"
		},{ where: { workOrderNo : workOrderNo}
		}).then(function() {
	        t.commit();
	        return callback(null,{msg:"Deactivated Successfully"});
	    }).catch(function(error) {
	        console.log(error);
	        return callback(error);
	    });
	});

};

