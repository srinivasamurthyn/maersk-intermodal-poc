var vendordb = require('../dbmodel/vendor.js').vendor;

exports.findAll = function(callback) {
	vendordb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findById = function(callback,id) {
	vendordb.findOne({ where : {
		id : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};
