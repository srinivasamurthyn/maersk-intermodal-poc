var statisticsdb = require('../dbmodel/statistics.js').statistics;

exports.findAll = function(callback) {
	statisticsdb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.findByCountryCode = function(callback,code) {
	statisticsdb.findAll({ where : {
		country_code : code
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.findByWorkOrder = function(callback,id) {
	statisticsdb.findOne({ where : {
		workOrder : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.countAll = function(callback) {
	
	statisticsdb.count({ where: {'isInTransit': true } }).then(transitCount => {
		statisticsdb.count({ where: {'isStalled': true } }).then(stalledCount => {
			statisticsdb.count({ where: {'isReached': true } }).then(reachedCount => {
				statisticsdb.count({ where: {'isNotReached': true } }).then(notReachedCount => {
					return callback(null, {transitCount:transitCount,stalledCount:stalledCount,reachedCount:reachedCount,notReachedCount:notReachedCount});
				})
			})
		});
	})
	
};

exports.countByCountry = function(callback,code) {
	
	statisticsdb.count({ where: {'isInTransit': true , country_code : code } }).then(transitCount => {
		statisticsdb.count({ where: {'isStalled': true , country_code : code} }).then(stalledCount => {
			statisticsdb.count({ where: {'isReached': true , country_code : code } }).then(reachedCount => {
				statisticsdb.count({ where: {'isNotReached': true , country_code : code } }).then(notReachedCount => {
					return callback(null, {transitCount:transitCount,stalledCount:stalledCount,reachedCount:reachedCount,notReachedCount:notReachedCount});
				})
			})
		});
	})
	
};


