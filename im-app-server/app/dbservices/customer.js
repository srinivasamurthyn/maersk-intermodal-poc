var customerdb = require('../dbmodel/customer.js').customer;

exports.findAll = function(callback) {
	customerdb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findById = function(callback,id) {
	customerdb.findOne({ where : {
		customerId : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByCustomerNo = function(callback,no) {
	customerdb.findOne({ where : {
		customerNo : no
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};
