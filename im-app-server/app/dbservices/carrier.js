var carrierdb = require('../dbmodel/carrier.js').carrier;

exports.findAll = function(callback) {
	carrierdb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findById = function(callback,id) {
	carrierdb.findOne({ where : {
		carrierId : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByCarrierCode = function(callback,code) {
	carrierdb.findOne({ where : {
		carrierCode : code
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};