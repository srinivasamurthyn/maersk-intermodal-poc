var trailerdb = require('../dbmodel/trailer.js').trailer;
const sequelizeCon = require('../dbconnection/db.js').getSequelize();


exports.upsert = function(trailer,callback) {
	sequelizeCon.transaction().then(function(t) {
		trailerdb.create(trailer,{
	        transaction: t
	    }).then(function() {
	        t.commit();
	        return callback(null,{msg:"Success"});
	    }).catch(function(error) {
	        console.log(error);
	        return callback(error);
	    });
	});
};

exports.stopTripByTrailerNo = function(callback,trailerNo) {
	sequelizeCon.transaction().then(function(t) {
		trailerdb.update({
			status : "ENDED",endTime:new Date().getTime()
		},{ where: { trailerNo : trailerNo , status : "STARTED" }
		}).then(function() {
	        t.commit();
	        return callback(null,{msg:"Unloaded Successfully"});
	    }).catch(function(error) {
	        console.log(error);
	        return callback(error);
	    });
	});

};

exports.findAll = function(callback) {
	trailerdb.findAll().then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.findById = function(callback,id) {
	trailerdb.findOne({ where : {
		trailerNo : id , status : "STARTED"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};



exports.findActiveTrailerById = function(callback,no) {
	trailerdb.findOne({ where : {
		trailerNo : no , status : "STARTED"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByTrailerId = function(callback,trailerId) {
	trailerdb.findOne({ where : {
		trailerId : trailerId
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.listActiveTrailer = function (callback){
	trailerdb.findAll({ where : {
		status : "STARTED"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
}
