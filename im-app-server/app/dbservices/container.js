const containerdb = require('../dbmodel/container.js').container;
const trailerdb = require('../dbmodel/trailer.js').trailer;
const sequelizeCon = require('../dbconnection/db.js').getSequelize();
const Op = sequelizeCon.Op

exports.upsert = function(container,callback) {
	sequelizeCon.transaction().then(function(t) {
		containerdb.create(container,{
	        transaction: t
	    }).then(function() {
	        t.commit();
	        return callback(null,{msg:"Success"});
	    }).catch(function(error) {
	        console.log(error);
	        return callback(error);
	    });
	});
};

exports.unloadedContainer = function(callback,containerNo) {
	
	sequelizeCon.transaction().then(function(t) {
		containerdb.update({
			status : "UNLOADED",unloadedTime:new Date().getTime(),unloadedDate:parseInt(new Date(new Date().setHours(0,0,0,0)).getTime())
		},{ where: { containerNo : containerNo , status : "LOADED" }
		}).then(function() {
	        t.commit();
	        return callback(null,{msg:"Unloaded Successfully"});
	    }).catch(function(error) {
	        console.log(error);
	        return callback(error);
	    });
	});

};






exports.findAll = function(callback) {
	containerdb.findAll({
		include: [{
		    model: trailerdb, as:'trailer'
		  }]
	}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findById = function(callback,id) {
	containerdb.findOne({ where : {
		containerId : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};

exports.findByStatus = function(callback,status,cupage,culimit) {
	let limit = culimit;
	let offset = 0;
	containerdb.findAndCountAll({where : { status : status}}).then((cdata) => {
	    let page = cupage;      // page number
	    let pages = Math.ceil(cdata.count / limit);
		offset = limit * (page - 1);
		containerdb.findAll({
			where : { status : status},
			//include: [{ model: trailerdb, as:'trailer' }],
			attributes: ['worder', 'vendor', 'containerNo', 'lastSeenTime','loadedDate','loadedTime','status','unloadedDate','unloadedTime','containerId'],
			limit: limit,
			offset: offset,
			order: [
	            ['lastSeenTime', 'DESC'],
	            ['containerNo', 'ASC'],
	        ],
		}).then((data) => {
	    	
	    	var pagination = [];
	    	
	    	//pre 
	    	if(page === 1){
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev disabled" , display : true };
	    	}else{
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev" , display : true};
	    	}
	    	pagination.push(sobj);
	    	
	    	
	    	var sdis = page + 3;
	    	var edis = page - 3;
	    	
	    	
	    	//page no
	    	for(i=1;i<pages;i++){
	    		var obj = { lable : i , page : i , vclass : "inactive" , display : true};
	    		if(page === i){
	    			obj.vclass = "active";
	    		}
	    		
	    		if(page <= i && i <= (page + 3)){
	    			obj.display = true;
	    		}else if(page >= i && i >= (page - 3)){
	    			obj.display = true;
	    		}else{
	    			obj.display = false;
	    		}
	    		pagination.push(obj);
	    		
	    	}
	    	
	    	//next
	    	if(page === pages){
	    		var eobj = { lable : "next" , page : "next" , vclass : "next disabled" , display : true};
	    	}else{
	    		var eobj = { lable : "Next → " , page : "next" , vclass : "next" , display : true};
	    	}
	    	pagination.push(eobj);
	    	
	    	return callback(null, {'result': data, 'count': cdata.count, 'pages': pages ,page:page,pagination:pagination});
	    });
		
	}).catch(function (error) {
		 console.log(error);
		 return callback(error);
	});
	
	
	/*containerdb.findAll({
		include: [{
		    model: trailerdb, as:'trailer'
		  }],
		  where : {
			  status : status
	}}).then(function (data) {
		
		console.log(data);
		
		return callback(null, data);
    }).catch(function(err) {
    	
    	cosole.log(err);
    	
    	return callback(err);
    });*/
};

exports.findByTrailerId = function(callback,id) {
	containerdb.findAll({ where : {
		trailerTrailerId : id
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.findActiveContainerById = function(callback,containerNo) {
	containerdb.findAll({ where : {
		containerNo : containerNo , status : "LOADED"
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.findByContainerNo = function(callback,containerNo) {
	containerdb.findAll({ where : {
		containerNo : containerNo
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};


exports.countBySummary = function(callback) {
	containerdb.count({ where: {'status': 'LOADED' } }).then(loadStatus => {
		containerdb.count({ where: {'status': 'UNLOADED' } }).then(unloadStatus => {
			containerdb.count({ where: {'sgfStatus': 'IN'} , egfStatus : { [Op.eq]:null} }).then(started => {
				containerdb.count({ where: {'sgfStatus': 'EXITED' , egfStatus : { [Op.eq]:null}} }).then(exited => {
					containerdb.count({ where: {'egfStatus': 'IN' } }).then(reached => {
						return callback(null, {loadStatus:loadStatus,unloadStatus:unloadStatus,started:started,inTransit:exited,reached:reached});
					});
				});
			});
		});
	})
};



exports.countSummaryByCountry = function(callback,ctryCode) {
	containerdb.count({ where: {ctryCode : ctryCode, status: 'LOADED' } }).then(loadStatus => {
		containerdb.count({ where: {ctryCode : ctryCode,status: 'UNLOADED' } }).then(unloadStatus => {
			containerdb.count({ where: {ctryCode : ctryCode,sgfStatus: 'IN'} , egfStatus : { [Op.eq]:null} }).then(started => {
				containerdb.count({ where: {ctryCode : ctryCode,sgfStatus: 'EXITED' , egfStatus : { [Op.eq]:null}} }).then(exited => {
					containerdb.count({ where: {ctryCode : ctryCode,egfStatus: 'IN' } }).then(reached => {
						return callback(null, {loadStatus:loadStatus,unloadStatus:unloadStatus,started:started,inTransit:exited,reached:reached});
					});
				});
			});
		});
	})
};




exports.listByLiveStatus = function(callback,status) {
	if(status === "STARTED"){
		containerdb.findAll({include: [{
		    model: trailerdb, as:'trailer'
		  }], where: {sgfStatus: 'IN'} , egfStatus : { [Op.eq]:null} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "INTRANSIT"){
		containerdb.findAll({include: [{
		    model: trailerdb, as:'trailer'
		  }], where: {sgfStatus: 'EXITED' , egfStatus : { [Op.eq]:null}} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "REACHED"){
		containerdb.findAll({include: [{
		    model: trailerdb, as:'trailer'
		  }], where: {egfStatus: 'IN' } }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}
};



exports.listLiveStatusByCountry = function(callback,ctryCode,status) {
	if(status === "STARTED"){
		containerdb.findAll({ include: [{
		    model: trailerdb, as:'trailer'
		  }],where: {ctryCode : ctryCode,sgfStatus: 'IN'} , egfStatus : { [Op.eq]:null} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "INTRANSIT"){
		containerdb.findAll({include: [{
		    model: trailerdb, as:'trailer'
		  }], where: {ctryCode : ctryCode,sgfStatus: 'EXITED' , egfStatus : { [Op.eq]:null}} }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}else if(status === "REACHED"){
		containerdb.findAll({include: [{
		    model: trailerdb, as:'trailer'
		  }], where: {ctryCode : ctryCode,egfStatus: 'IN' } }).then(function (data) {
			return callback(null, data);
	    }).catch(function(err) {
	    	return callback(err);
	    });
	}
};


exports.findContainerByCountry = function (callback,ctryCode){
	containerdb.findAll({
		include: [{
		    model: trailerdb, as:'trailer'
		  }],
		  where : {
			  ctryCode : ctryCode
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
}

exports.findContainerStatusByCountry = function (callback,ctryCode,status){
	containerdb.findAll({
		include: [{
		    model: trailerdb, as:'trailer'
		  }],
		  where : {
			  ctryCode : ctryCode,status : status
	}}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
}


exports.searchContainerByWorkorder = function (callback,key,value,status,cupage,culimit){
	let limit = culimit;
	let offset = 0;
	containerdb.findAndCountAll({where : { worder : { [Op.like ] : '%'+value.toLowerCase()+'%' } , status : status}}).then((cdata) => {
	    let page = cupage;      // page number
	    let pages = Math.ceil(cdata.count / limit);
		offset = limit * (page - 1);
		containerdb.findAll({
			where : { worder : { [Op.like ] : '%'+value.toLowerCase()+'%' } , status : status},
			//include: [{ model: trailerdb, as:'trailer' }],
			attributes: ['worder', 'vendor', 'containerNo', 'lastSeenTime','loadedDate','loadedTime','status','unloadedDate','unloadedTime','containerId'],
			limit: limit,
			offset: offset,
			order: [
	            ['lastSeenTime', 'DESC'],
	            ['containerNo', 'ASC'],
	        ],
		}).then((data) => {
	    	
	    	var pagination = [];
	    	
	    	//pre 
	    	if(page === 1){
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev disabled" , display : true };
	    	}else{
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev" , display : true};
	    	}
	    	pagination.push(sobj);
	    	
	    	
	    	var sdis = page + 3;
	    	var edis = page - 3;
	    	
	    	
	    	//page no
	    	for(i=1;i<pages;i++){
	    		var obj = { lable : i , page : i , vclass : "inactive" , display : true};
	    		if(page === i){
	    			obj.vclass = "active";
	    		}
	    		
	    		if(page <= i && i <= (page + 3)){
	    			obj.display = true;
	    		}else if(page >= i && i >= (page - 3)){
	    			obj.display = true;
	    		}else{
	    			obj.display = false;
	    		}
	    		pagination.push(obj);
	    		
	    	}
	    	
	    	//next
	    	if(page === pages){
	    		var eobj = { lable : "next" , page : "next" , vclass : "next disabled" , display : true};
	    	}else{
	    		var eobj = { lable : "Next → " , page : "next" , vclass : "next" , display : true};
	    	}
	    	pagination.push(eobj);
	    	
	    	return callback(null, {'result': data, 'count': cdata.count, 'pages': pages ,page:page,pagination:pagination});
	    });
		
	}).catch(function (error) {
		 console.log(error);
		 return callback(error);
	});

	
	
}



exports.searchContainerByVendor = function (callback,key,value,status,cupage,culimit){
	let limit = culimit;
	let offset = 0;
	containerdb.findAndCountAll({where : { vendor : { [Op.like ] : '%'+value.toLowerCase()+'%' } , status : status}}).then((cdata) => {
	    let page = cupage;      // page number
	    let pages = Math.ceil(cdata.count / limit);
		offset = limit * (page - 1);
		containerdb.findAll({
			where : { vendor : { [Op.like ] : '%'+value.toLowerCase()+'%' }  , status : status},
			//include: [{ model: trailerdb, as:'trailer' }],
			attributes: ['worder', 'vendor', 'containerNo', 'lastSeenTime','loadedDate','loadedTime','status','unloadedDate','unloadedTime','containerId'],
			limit: limit,
			offset: offset,
			order: [
	            ['lastSeenTime', 'DESC'],
	            ['containerNo', 'ASC'],
	        ],
		}).then((data) => {
	    	
	    	var pagination = [];
	    	
	    	//pre 
	    	if(page === 1){
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev disabled" , display : true };
	    	}else{
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev" , display : true};
	    	}
	    	pagination.push(sobj);
	    	
	    	
	    	var sdis = page + 3;
	    	var edis = page - 3;
	    	
	    	
	    	//page no
	    	for(i=1;i<pages;i++){
	    		var obj = { lable : i , page : i , vclass : "inactive" , display : true};
	    		if(page === i){
	    			obj.vclass = "active";
	    		}
	    		
	    		if(page <= i && i <= (page + 3)){
	    			obj.display = true;
	    		}else if(page >= i && i >= (page - 3)){
	    			obj.display = true;
	    		}else{
	    			obj.display = false;
	    		}
	    		pagination.push(obj);
	    		
	    	}
	    	
	    	//next
	    	if(page === pages){
	    		var eobj = { lable : "next" , page : "next" , vclass : "next disabled" , display : true};
	    	}else{
	    		var eobj = { lable : "Next → " , page : "next" , vclass : "next" , display : true};
	    	}
	    	pagination.push(eobj);
	    	
	    	return callback(null, {'result': data, 'count': cdata.count, 'pages': pages ,page:page,pagination:pagination});
	    });
		
	}).catch(function (error) {
		 console.log(error);
		 return callback(error);
	});
	
	
}



exports.searchContainerByContainerNo = function (callback,key,value,status,cupage,culimit){
	let limit = culimit;
	let offset = 0;
	containerdb.findAndCountAll({where : { containerNo : { [Op.like ] : '%'+value.toLowerCase()+'%' } , status : status}}).then((cdata) => {
	    let page = cupage;      // page number
	    let pages = Math.ceil(cdata.count / limit);
		offset = limit * (page - 1);
		containerdb.findAll({
			where : { containerNo : { [Op.like ] : '%'+value.toLowerCase()+'%' } , status : status},
			//include: [{ model: trailerdb, as:'trailer' }],
			attributes: ['worder', 'vendor', 'containerNo', 'lastSeenTime','loadedDate','loadedTime','status','unloadedDate','unloadedTime','containerId'],
			limit: limit,
			offset: offset,
			order: [
	            ['lastSeenTime', 'DESC'],
	            ['containerNo', 'ASC'],
	        ],
		}).then((data) => {
	    	
	    	var pagination = [];
	    	
	    	//pre 
	    	if(page === 1){
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev disabled" , display : true };
	    	}else{
	    		var sobj = { lable : "← Previous" , page : "pre" , vclass : "prev" , display : true};
	    	}
	    	pagination.push(sobj);
	    	
	    	
	    	var sdis = page + 3;
	    	var edis = page - 3;
	    	
	    	
	    	//page no
	    	for(i=1;i<pages;i++){
	    		var obj = { lable : i , page : i , vclass : "inactive" , display : true};
	    		if(page === i){
	    			obj.vclass = "active";
	    		}
	    		
	    		if(page <= i && i <= (page + 3)){
	    			obj.display = true;
	    		}else if(page >= i && i >= (page - 3)){
	    			obj.display = true;
	    		}else{
	    			obj.display = false;
	    		}
	    		pagination.push(obj);
	    		
	    	}
	    	
	    	//next
	    	if(page === pages){
	    		var eobj = { lable : "next" , page : "next" , vclass : "next disabled" , display : true};
	    	}else{
	    		var eobj = { lable : "Next → " , page : "next" , vclass : "next" , display : true};
	    	}
	    	pagination.push(eobj);
	    	
	    	return callback(null, {'result': data, 'count': cdata.count, 'pages': pages ,page:page,pagination:pagination});
	    });
		
	}).catch(function (error) {
		 console.log(error);
		 return callback(error);
	});
		
}






