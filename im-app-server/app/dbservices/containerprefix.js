var containerprefixdb = require('../dbmodel/containerprefix.js').containerprefix;

exports.findAll = function(callback) {
	containerprefixdb.findAll({order: [
        ['name', 'ASC']
    ]}).then(function (data) {
		return callback(null, data);
    }).catch(function(err) {
    	return callback(err);
    });
};