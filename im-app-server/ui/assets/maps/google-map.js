//search box
var searchBox;
var searchBoxMob;
var dotsIcon = "assets/images/markericon/dot.png";

function DrawRoute(containerId,id,list,zoom,center){
		
		var map = new google.maps.Map(
			 document.getElementById(id), {
			 center: new google.maps.LatLng(center.lat, center.lon),
			 zoom: zoom,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		
		/*var input = document.getElementById('pac-input');
		searchBox = new google.maps.places.SearchBox(input);
		
		var inputmob = document.getElementById('pac-inputMob');
		searchBoxMob = new google.maps.places.SearchBox(inputmob);*/
		
		var lat_lng = [];
		for(i=0;i<list.length;i++){
			var myCenter = new google.maps.LatLng(list[i].lat,list[i].lon);
			lat_lng.push(myCenter);
			
			
			
			//marker
			if(list[i].idInfo === "Trailer Id"){
				
				if(dotsIcon === list[i].icon){
					var icon = {
						    url: list[i].icon, // url
						    scaledSize: new google.maps.Size(10, 10), // scaled size
					};
				}else{
					var icon = {
						    url: list[i].icon, // url
						    scaledSize: new google.maps.Size(40, 40), // scaled size
					};
				}
				
				
			}else{
				var icon = {
					    url: list[i].icon, // url
					    scaledSize: new google.maps.Size(30, 40),
				};
			}
			
			
			var marker = new google.maps.Marker({
				position:myCenter,
				icon:icon
			});
			
						
			//infowindow
			var content = '<b class="size-h4"> '+list[i].idInfo+' :</b> <lable class="size-h4">'+list[i].id+'</lable><br>'+
				'<b class="size-h4"> '+list[i].info+' :  </b> <lable class="size-h4">'+getStringDate(list[i].timeStamp)+'</lable></b>';
			var infowindow = new google.maps.InfoWindow({
				  content:content,
			});
			
			
			google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
			    return function() {
			        infowindow.open(map,marker);
			    };
			})(marker,infowindow));
			
			
			marker.setMap(map);
			
		}
		
		for (var t = 0;
		    (t + 1) < lat_lng.length; t++) {
		    //Intialize the Direction Service
		    var service = new google.maps.DirectionsService();
		    var directionsDisplay = new google.maps.DirectionsRenderer();

		    var bounds = new google.maps.LatLngBounds();
		    if ((t + 1) < lat_lng.length) {
		      var src = lat_lng[t];
		      var des = lat_lng[t + 1];
		      service.route({
		        origin: src,
		        destination: des,
		        travelMode: google.maps.DirectionsTravelMode.DRIVING
		      }, function(result, status) {
		        if (status == google.maps.DirectionsStatus.OK) {
		          // new path for the next result
		          var path = new google.maps.MVCArray();
		          //Set the Path Stroke Color
		          // new polyline for the next result
		          var poly = new google.maps.Polyline({
		            map: map,
		            strokeColor: '#4986E7',
		            strokeWeight: 4
		          });
		          
		          poly.setPath(path);
		          for (var k = 0, len = result.routes[0].overview_path.length; k < len; k++) {
		            path.push(result.routes[0].overview_path[k]);
		            bounds.extend(result.routes[0].overview_path[k]);
		            map.fitBounds(bounds);
		          }
		        } else console.log("Directions Service failed:" + status);
		      });
		    }
		  }
}



function reset(){
	infowindow.close();
	map.fitBounds(bounds);
}

function geocodeLatLng(lat, lon ,callback) {
	var latlng = {lat:lat , lng:lon};
    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {
        	
        	console.log()
        	callback(results[0].formatted_address);
        }
      } else {
    	  console.log("Address Not Available");
    	  callback("Address Not Available");
    	  
      }
    });
}



function getEta(slat,slon,dlat,dlon,callback){
	var origin = new google.maps.LatLng(slat,slon);
	var destination = new google.maps.LatLng(dlat,dlon);

	var service = new google.maps.DistanceMatrixService();
	service.getDistanceMatrix(
	  {
	    origins: [origin],
	    destinations: [destination],
	    travelMode: 'DRIVING',
	    //transitOptions: TransitOptions,
	    //drivingOptions: DrivingOptions,
	    //unitSystem: UnitSystem,
	    avoidHighways: false,
	    avoidTolls: true,
	  },function (response, status) {
			if (status == 'OK') {
			    var origins = response.originAddresses;
			    var destinations = response.destinationAddresses;
			    for (var i = 0; i < origins.length; i++) {
			      var results = response.rows[i].elements;
			      for (var j = 0; j < results.length; j++) {
			        var element = results[j];
			        var distance = element.distance;
			        var duration = element.duration;
			        var from = origins[i];
			        var to = destinations[j];
			        callback({originAddresses:origins[0],destinationAddresses:destinations[0],distance:distance,duration:duration}) ;
			      }
			    }
			  }
		});
}







function getLatLon(callback){
	var places = searchBox.getPlaces();
	if(places){
		if (places.length == 0) {
	        return;
	    }
	          
		places.forEach(function(place) {
			if (!place.geometry) {
				callback({error:"Returned place contains no geometry"});
			}
	              
	        var latitude = place.geometry.location.lat();
	        var longitude = place.geometry.location.lng();  
	        callback({lat:latitude,lon:longitude,places:places[0].formatted_address});
	    });
	} else {
		var placesMob = searchBoxMob.getPlaces();
		if (placesMob && placesMob.length == 0) {
			callback({error:"Returned place contains no geometry"});
	    }
	          
		placesMob.forEach(function(place) {
			if (!place.geometry) {
				callback({error:"Returned place contains no geometry"});
			}
	              
	        var latitude = place.geometry.location.lat();
	        var longitude = place.geometry.location.lng();  
	        callback({lat:latitude,lon:longitude,places:placesMob[0].formatted_address});
	    });
	}
	

}


function getStringDate(number){
	var date=new Date(number);
	var d = date.getDate();
	var m = date.getMonth()+1;
	var y = date.getFullYear();
	
	
	var hours =  date.getHours();
	var mins = date.getMinutes();
 	mins= (mins <= 9)?('0'+mins):mins;
 	var thistime = (hours > 12) ? (hours-12 + ':' + mins +' PM') : (hours + ':' + mins +' AM');
 	return d+'/'+m+'/'+y+' at '+thistime;
	
}

