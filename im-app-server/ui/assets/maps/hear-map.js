//Initialize the Platform object:
var drivingMode;
var map;
var ui

function setHearMap(config,locs,markers,center) {
	drivingMode = config.drivingMode;
	var platform = new H.service.Platform({
	    app_id: config.appId,
	    app_code: config.appCode
	})
	
	//Get the default map types from the platform object:
	var defaultLayers = platform.createDefaultLayers();

	//Get an instance of the routing service:
	var router = platform.getRoutingService();
	
	// Instantiate the map:
	map = new H.Map(document.getElementById('mapContainer'),defaultLayers.normal.map);

	// Create the default UI:
	ui = H.ui.UI.createDefault(map, defaultLayers);
	
	// Create the parameters for the routing request:
	var routingParameters = locs;
	var markers = markers;
	
	
	// Success a callback function
	var onResult = function(result) {
		var route,routeShape,linestring;
		if(result && result.response && result.response.route) {
		  route = result.response.route[0];
		  routeShape = route.shape;
		  linestring = new H.geo.LineString();
		  routeShape.forEach(function(point) {
		    var parts = point.split(',');
		    linestring.pushLatLngAlt(parts[0], parts[1]);
		  });
	
		  // Retrieve the mapped positions of the requested waypoints:
		  startPoint = route.waypoint[0].mappedPosition;
		  endPoint = route.waypoint[1].mappedPosition;
		  if(route.waypoint[route.waypoint.length - 1].mappedPosition){
			  endPoint = route.waypoint[route.waypoint.length - 1].mappedPosition;
		  }
		  
		  // Create a polyline to display the route:
		  var routeLine = new H.map.Polyline(linestring, {
		    style: { strokeColor: 'blue', lineWidth: 5 }
		  });
	
		  // Add the route
		  map.addObjects([routeLine]);
		  map.setViewBounds(routeLine.getBounds());
		}
	};

	
	

	//every 100 point draw route
	for(var i=0;i<routingParameters.length;i++){
		routingParameters[i].mode = drivingMode;
		routingParameters[i].representation = 'display';
		router.calculateRoute(routingParameters[i], onResult, function(error) {
			console.log(error.message);
		});
	}
	
	
	var mapSettings = ui.getControl('mapsettings');
	var zoom = ui.getControl('zoom');
	var scalebar = ui.getControl('scalebar');
	mapSettings.setAlignment('top-left');
	zoom.setAlignment('top-left');
	scalebar.setAlignment('top-left');
	// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
	var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
	addInfoBubble(markers);
	
}


function addMarkerToGroup(iconSrc,size,group, coordinate, html) {
	
	var cubsIcon = new H.map.Icon(iconSrc,{size: {w: size, h: size}});
	var marker = new H.map.Marker(coordinate,{icon:cubsIcon});
	marker.setData(html);
	group.addObject(marker);
}


function addInfoBubble(markers) {
	
	  var group = new H.map.Group();
	  map.addObject(group);
	  group.addEventListener('tap', function (evt) {
	    var bubble =  new H.ui.InfoBubble(evt.target.getPosition(), {
	      content: evt.target.getData()
	    });
	    ui.addBubble(bubble);
	  }, false);

	  
	  for(var i=0;i<markers.length;i++){
		  if(markers[i].titleType === "Trailer"){
			  addMarkerToGroup("assets/images/markericon/truck.png",40,group, {lat:markers[i].lat, lng:markers[i].lng},getInfoHtml(markers[i].titleType,markers[i].titleDisc,markers[i].titleName,markers[i].timeStamp));
		  }else if(markers[i].titleType === "PICKUP"){
			  addMarkerToGroup("assets/images/markericon/start.png",40,group, {lat:markers[i].lat, lng:markers[i].lng},getAddressInfoHtml(markers[i].titleDisc,markers[i].titleName));
		  }else if (markers[i].titleType === "DROP"){
			  addMarkerToGroup("assets/images/markericon/end.png",40,group, {lat:markers[i].lat, lng:markers[i].lng},getAddressInfoHtml(markers[i].titleDisc,markers[i].titleName));
		  }
	  }
}




function getInfoHtml (titleType,titleDisc,titleName,timeStamp){
	return 	'<div>'+
			'<p style="font-size: 15px;">'+titleDisc+'</p>' +
	    	'<p style="font-size: 14px;">Time :'+getStringDate(timeStamp)+'</p>'+
	    	'</div>';
}


function getAddressInfoHtml (titleDisc,titleName){
	return 	'<div>'+
			'<p style="font-size: 15px;">'+titleName+'</p>' +
	    	'<p style="font-size: 12px;">'+titleDisc+'</p>'+
	    	'</div>';
}

function getStringDate(number){
	var date=new Date(number);
	var d = date.getDate();
	var m = date.getMonth()+1;
	var y = date.getFullYear();
	
	
	var hours =  date.getHours();
	var mins = date.getMinutes();
 	mins= (mins <= 9)?('0'+mins):mins;
 	var thistime = (hours > 12) ? (hours-12 + ':' + mins +' PM') : (hours + ':' + mins +' AM');
 	return d+'/'+m+'/'+y+' at '+thistime;
	
}


function getETA (lastLoc,desLoc,callback) {
	$.getJSON( "https://route.api.here.com/routing/7.2/calculateroute.json?waypoint0="+lastLoc+"&waypoint1="+desLoc+"&mode="+drivingMode+"&routeattributes=none,sm&maneuverattributes=sp&app_id="+appId+"&app_code="+appCode, function( data ) {
		callback(null,data);
	});
}


