//load AuthRole 
var roleModel;
var roleInfo;
var sideNav;
var code;


$.getJSON( "app/model/roleModel.json", function( data ) {
	roleModel = data;
});

$.getJSON( "app/model/roleInfo.json", function( data ) {
	roleInfo = data;
});

$.getJSON( "app/model/sideNav.json", function( data ) {
	sideNav = data;
});

$.getJSON( "app/model/code.json", function( data ) {
	code = data;
});



var smacsservicemodule = angular.module('smacsservicemodule', [ 'ngResource' ]);


//Auth
smacsservicemodule.factory('CommonAuth', function($http,$location) {
	return {
        roleAccess : function() {
        	
        	if(window.location.hash === "#/starttrailer" || window.location.hash === "#/managecontainer" || window.location.hash === "#/manageunload" || window.location.hash === "#/stoptrailer"){
        		return;
        	}
        	
        	
        	var token = getToken();
    		var roles = getRoles();
        	var loginPage = roleModel.login;
    		var registerPage = roleModel.register;
    		if(token){
        		var landingPage = roleModel.homepage[getLandingRole()];
        		var access = roleModel.urls[window.location.hash];
        		
        		//home
        		if(access && access.toString().indexOf(getLandingRole()) === -1){
        			window.location = roleModel.homepage[getLandingRole()];
        		}
        		//login
        		if(roleModel.login === window.location.hash){
        			window.location  = landingPage;
        		}
        		
        	}else if(registerPage !== window.location.hash){
        		presslogOut();
        		window.location  = loginPage;
        	}
    		
        },
        rederictPage : function () {
        	var token = getToken();
        	var loginPage = roleModel.login;
        	if(token){
        		window.location = roleModel.homepage[getLandingRole()];
        	}else{
        		window.location = loginPage;
        	}
    	},
    	getRoles : function () {
    		return getRoles();
    	},
    	setLandingRole : function (role){
    		setLandingRole(role);
    	},
    	getUserName : function () {
    		return getUserName();
    	},
    	getRoleInfo : function () {
    		return roleInfo;
    	},
    	getVendorAccess : function () {
    		return getVendorAccess();
    	},
    	getSideNav : function () {
    		var side = sideNav[getLandingRole()]
    		if(side){
    			return sideNav[getLandingRole()];
    		}else{
    			return [];
    		}
    	},
    	getCodes : function () {
    		return code;
    	}
    };
	 
});


//auth set
smacsservicemodule.factory('httpRequestInterceptor', function () {
	  return {
		request: function (config) {
			var token = getToken();
			if(token){
				config.headers['Authorization'] = token	;
			}
			return config;
		}
	};
});


//response.status
smacsservicemodule.factory('httpErrorResponseInterceptor', ['$q', '$location',
	  function($q, $location) {
	    return {
	      response: function(responseData) {
	        return responseData;
	      },
	      responseError: function error(response) {
	    	switch (response.status) {
	          case 401:
	        	$location.path('/signin-one');
	            break;
	          default:
	            //$location.path('/error');
	        }
	        return $q.reject(response);
	      }
	    };
	  }
]);





//http config
smacsservicemodule.config(function ($httpProvider) {
	$httpProvider.interceptors.push('httpRequestInterceptor');
	$httpProvider.interceptors.push('httpErrorResponseInterceptor');
});



//xls and xlsx

smacsservicemodule.directive("importSheetJs", [function SheetJSImportDirective() {
	  return {
		    scope: { opts: '=' },
		    link: function ($scope, $elm) {
		      $elm.on('change', function (changeEvent) {
		        var reader = new FileReader();
		        var xlsxflag = false;
		        $scope.xlsdata = [];
		        if(changeEvent.currentTarget.files[0].name.indexOf(".xlsx") > 0){
		        	xlsxflag = true;
		        }
		        reader.onload = function (e) {
		        	 
		             var data = e.target.result;  
		             if (xlsxflag) {  
		               var workbook = XLSX.read(data, { type: 'binary' });  
		             }  
		             else {  
		               var workbook = XLS.read(data, { type: 'binary' });  
		             }    
		             
		             var sheet_name_list = workbook.SheetNames;  
		             var cnt = 0; 
		             sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/  
		               if (xlsxflag) {  
		                 var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);  
		               }  
		               else {  
		                 var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);  
		               }   
		               if (exceljson.length > 0) {  
		                 for (var i = 0; i < exceljson.length; i++) {  
		                     $scope.xlsdata.push(exceljson[i]);
		                     $scope.$apply();
		                 }
		               }  
		             });
		        };

		        reader.readAsBinaryString(changeEvent.target.files[0]);
		      });
		    }
		  };
	}
]);



smacsservicemodule.directive('fileReader', function() {
	  return {
	    scope: {
	      fileReader:"="
	    },
	    link: function(scope, element) {
	    	$(element).on('change', function(changeEvent) {
	    		var reader = new FileReader();
	    		var xlsxflag = false;
			    var result = [];
			    if(changeEvent.currentTarget.files[0].name.indexOf(".xlsx") > 0){
			       	xlsxflag = true;
			    }
			    reader.onload = function (e) {
			    	var data = e.target.result;  
			        if (xlsxflag) {  
			        	var workbook = XLSX.read(data, { type: 'binary' });  
			        }  
			        else {  
			            var workbook = XLS.read(data, { type: 'binary' });  
			        }    
			            
			        var sheet_name_list = workbook.SheetNames;  
			        var cnt = 0; 
			        sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/  
			        	if (xlsxflag) {  
			        		var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);  
			            } else {  
			                var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);  
			            }   
			            if (exceljson.length > 0) {  
			                for (var i = 0; i < exceljson.length; i++) {  
			                	result.push(exceljson[i]);  
			                }
			            }
			            scope.fileReader = result;
			            scope.$apply();
			        });
			    };
			    reader.readAsBinaryString(changeEvent.target.files[0]);
	    	});
		}
	};
});


//WorkOrderRemove
smacsservicemodule.factory('HearMapApi', [ '$resource', function($resource) {
	return $resource('/controller/hearmapi/api', {},{
		query : { method : 'GET'}
	});
} ]);


//StatusCountAll
smacsservicemodule.factory('StatusCountAll', [ '$resource', function($resource) {
	return $resource('/controller/statistics/count/all', {},{
		query : { method : 'GET' }
	});
} ]);


//StatusCountAll
smacsservicemodule.factory('UserRegister', [ '$resource', function($resource) {
	return $resource('/basic/useraccount/user', {},{
		query : { method : 'POST' }
	});
} ]);

//ViewTrailer
smacsservicemodule.factory('ViewTrailer', [ '$resource', function($resource) {
	return $resource('/basic/container/list/all', {},{
		query : { method : 'POST' }
	});
} ]);






//AddWorkOrder
smacsservicemodule.factory('AddWorkOrder', [ '$resource', function($resource) {
	return $resource('/controller/workorder', {},{
		query : { method : 'POST' }
	});
} ]);

//AddWorkOrderBulk
smacsservicemodule.factory('AddWorkOrderBulk', [ '$resource', function($resource) {
	return $resource('/controller/workorder/bulk', {},{
		query : { method : 'POST' , isArray : true }
	});
} ]);

//AddTrailer
smacsservicemodule.factory('AddTrailer', [ '$resource', function($resource) {
	return $resource('/basic/trailer', {},{
		query : { method : 'POST' }
	});
} ]);



//TrailerById
smacsservicemodule.factory('TrailerById', [ '$resource', function($resource) {
	var id=$resource;
	return $resource('/basic/trailer/:id', {},{
		query : { method : 'GET', params : { id:id }}
	});
} ]);

//ContainerCountByCountry
smacsservicemodule.factory('ContainerCountByCountry', [ '$resource', function($resource) {
	var country=$resource;
	return $resource('/controller/container/summary/count/country/:country', {},{
		query : { method : 'GET', params : { country:country }}
	});
} ]);

//MetsByCountry
smacsservicemodule.factory('MetsByCountry', [ '$resource', function($resource) {
	var country=$resource;
	return $resource('/controller/workorder/country/:country', {},{
		query : { method : 'GET', params : { country:country }, isArray : true}
	});
} ]);

//CustomerList
smacsservicemodule.factory('CustomerList', [ '$resource', function($resource) {
	return $resource('/controller/customer/list/all', {},{
		query : { method : 'GET', isArray : true}
	});
} ]);

//CarrierList
smacsservicemodule.factory('CarrierList', [ '$resource', function($resource) {
	return $resource('/controller/carrier/list/all', {},{
		query : { method : 'GET', isArray : true}
	});
} ]);

//workorderList
smacsservicemodule.factory('workorderList', [ '$resource', function($resource) {
	return $resource('/controller/workorder/list/all', {},{
		query : { method : 'GET', isArray : true}
	});
} ]);



//workorderList
smacsservicemodule.factory('workorderList', [ '$resource', function($resource) {
	return $resource('/controller/workorder/list/all', {},{
		query : { method : 'GET', isArray : true}
	});
} ]);



//ContainerList
smacsservicemodule.factory('ContainerList', [ '$resource', function($resource) {
	return $resource('/basic/container/list/all', {},{
		query : { method : 'GET', isArray : true}
	});
} ]);

//VendorList
smacsservicemodule.factory('VendorList', [ '$resource', function($resource) {
	return $resource('/controller/vendor/list/all', {},{
		query : { method : 'GET', isArray : true}
	});
} ]);
//VendorHome
smacsservicemodule.factory('VendorHome', [ '$resource', function($resource) {
	return $resource('/controller/vendorhome/:id', {},{
		query : { method : 'GET', isArray : true}
	});
} ]);

//WorkOrderStatus
smacsservicemodule.factory('WorkOrderById', [ '$resource', function($resource) {
	var id=$resource;
	return $resource('/controller/workorder/:id', {},{
		query : { method : 'GET', params : { id:id }}
	});
} ]);

//WorkOrderRemove
smacsservicemodule.factory('WorkOrderRemove', [ '$resource', function($resource) {
	var workOrderNo=$resource;
	return $resource('/controller/workorder/remove/:workOrderNo', {},{
		query : { method : 'GET', params : { workOrderNo:workOrderNo }}
	});
} ]);


//WorkOrderDetails
smacsservicemodule.factory('WorkOrderDetails', [ '$resource', function($resource) {
	var no=$resource;
	return $resource('/controller/workorder/workorder/:no', {},{
		query : { method : 'GET', params : { no:no }}
	});
} ]);


//WorkOrderDetails
smacsservicemodule.factory('WorkOrderByCountryCode', [ '$resource', function($resource) {
	var code=$resource;
	var region=$resource;
	return $resource('/controller/workorder/country/code/:region/:code', {},{
		query : { method : 'GET', params : { code:code,region:region }, isArray : true}
	});
} ]);


//ContainerByTrailerId
smacsservicemodule.factory('ContainerByTrailerId', [ '$resource', function($resource) {
	var trailerId=$resource;
	return $resource('/basic/container/trailer/:trailerId', {},{
		query : { method : 'GET', params : { trailerId:trailerId }, isArray : true}
	});
} ]);

//ContainerByCountry
smacsservicemodule.factory('ContainerByCountry', [ '$resource', function($resource) {
	var country=$resource;
	return $resource('/controller/container/country/list/:country', {},{
		query : { method : 'GET', params : { country:country }, isArray : true}
	});
} ]);

//AddContainer
smacsservicemodule.factory('AddContainer', [ '$resource', function($resource) {
	return $resource('/basic/container', {},{
		query : { method : 'POST' }
	});
} ]);

//UnLoadedContainer
smacsservicemodule.factory('UnLoadedContainer', [ '$resource', function($resource) {
	var containerNo=$resource;
	return $resource('/basic/container/unloaded/:containerNo', {},{
		query : { method : 'GET', params : { containerNo:containerNo }}
	});
} ]);

//StopTrailerByTrailerId
smacsservicemodule.factory('StopTrailerByTrailerId', [ '$resource', function($resource) {
	var trailerNo=$resource;
	return $resource('/basic/trailer/stoptrip/:trailerNo', {},{
		query : { method : 'GET', params : { trailerNo:trailerNo }}
	});
} ]);

//FindByContainerNo
smacsservicemodule.factory('FindByContainerNo', [ '$resource', function($resource) {
	var containerNo=$resource;
	return $resource('/controller/container/containerno/:containerNo', {},{
		query : { method : 'GET', params : { containerNo:containerNo }, isArray : true}
	});
} ]);


//FindByTrailerId
smacsservicemodule.factory('FindByTrailerId', [ '$resource', function($resource) {
	var trailerId=$resource;
	return $resource('/controller/trailer/trailerno/:trailerId', {},{
		query : { method : 'GET', params : { trailerId:trailerId }}
	});
} ]);

//   
			/*------------------------------------------------*/
//FindContainersByStatus
smacsservicemodule.factory('FindContainersByStatus', [ '$resource', function($resource) {
	var status=$resource;
	var page=$resource;
	var limit=$resource;
	return $resource('/basic/container/listbystatus/:status/:page/:limit', {},{
		query : { method : 'GET', params : { status:status,page:page,limit:limit }}
	});
} ]);

				/*-------------------------------------------------------------*/

//FindWorkOrderByContainer
smacsservicemodule.factory('FindWorkOrderByContainer', [ '$resource', function($resource) {
	var containerNo=$resource;
	return $resource('/controller/workorder/listbycontainer/:containerNo', {},{
		query : { method : 'GET', params : { containerNo:containerNo }, isArray : true}
	});
} ]);


//SummaryCount
smacsservicemodule.factory('SummaryCount', [ '$resource', function($resource) {
	return $resource('/controller/container/summary/count', {},{
		query : { method : 'GET' }
	});
} ]);


//TrailerGpsDataByRange
smacsservicemodule.factory('TrailerGpsDataByRange', [ '$resource', function($resource) {
	var trailerId=$resource;
	var startTime=$resource;
	var endTime=$resource;
	return $resource('/controller/gpsdata/trailer/range/startend/:trailerId/:startTime/:endTime', {},{
		query : { method : 'GET', params : { trailerId:trailerId,startTime:startTime,endTime:endTime }, isArray : true}
	});
} ]);


//WorkOrderByContainer
smacsservicemodule.factory('WorkOrderByContainer', [ '$resource', function($resource) {
	var containerNo=$resource;
	return $resource('/controller/workorder/listbycontainer/:containerNo', {},{
		query : { method : 'GET', params : { containerNo:containerNo}}
	});
} ]);



//UnloadContainer
smacsservicemodule.factory('UnloadContainer', [ '$resource', function($resource) {
	var containerNo=$resource;
	return $resource('/basic/container/unloaded/:containerNo', {},{
		query : { method : 'GET', params : { containerNo:containerNo}}
	});
} ]);

//FindByContainerId
smacsservicemodule.factory('FindByContainerId', [ '$resource', function($resource) {
	var id=$resource;
	return $resource('/basic/container/:id', {},{
		query : { method : 'GET', params : { id:id}}
	});
} ]);

//ActiveTrips
smacsservicemodule.factory('ActiveTrips', [ '$resource', function($resource) {
	return $resource('/controller/trailer/list/active', {},{
		query : { method : 'GET' , isArray : true}
	});
} ]);



///ContainerPrefix
smacsservicemodule.factory('ContainerPrefix', [ '$resource', function($resource) {
	return $resource('/basic/containerprefix/list/all', {},{
		query : { method : 'GET' , isArray : true}
	});
} ]);



		/*--------------------------------------------------------------------------*/

//LiveStatus
smacsservicemodule.factory('LiveStatus', [ '$resource', function($resource) {
	var status=$resource;			
	return $resource('/basic/container/listbystatus/:status', {},{
		query : { method : 'GET', params : { status:status}, isArray : true}
	});
} ]);

//LiveStatusByCountry
smacsservicemodule.factory('LiveStatusByCountry', [ '$resource', function($resource) {
	var country=$resource;
	var status=$resource;				
	return $resource('/basic/container/listbystatus/country/:country/:status', {},{
		query : { method : 'GET', params : {country:country, status:status}, isArray : true}
	});
} ]);

//SearchContainers
smacsservicemodule.factory('SearchContainers', [ '$resource', function($resource) {
	var key=$resource;
	var value=$resource;
	var status=$resource;
	var page=$resource;
	var limit=$resource;
	return $resource('/basic/container/search/:key/:value/:status/:page/:limit', {},{
		query : { method : 'GET', params : {key:key, value:value,status:status,page:page,limit:limit}}
	});
} ]);


//GetVendorList
smacsservicemodule.factory('GetVendorList', [ '$resource', function($resource) {
	var id=$resource;
	return $resource('/controller/vendor/:id', {},{
		query : { method : 'GET', params : {id:id}}
	});
} ]);

smacsservicemodule.factory('GetVendorAllList', [ '$resource', function($resource) {
	return $resource('/controller/vendor/list/all', {},{
		query : { method : 'GET',isArray : true}
	});
} ]);




//ContainerByStatusCountry
smacsservicemodule.factory('ContainerByStatusCountry', [ '$resource', function($resource) {
	var country=$resource;
	var status=$resource;				
	return $resource('/controller/Container/listbystatus/country/:country/:status', {},{
		query : { method : 'GET', params : {country:country, status:status}, isArray : true}
	});
} ]);

				/*--------------------------------------------------------------------*/



