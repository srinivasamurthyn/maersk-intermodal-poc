/**
 * http://AUTH CONTROLLER/
 */

//auth controller
var AuthStorage = "LD:AUTH:MAERSK_IM";

function getUser(){
	
	var login;
	var loginLD=atob(sessionStorage.getItem(AuthStorage));
	if(loginLD !== undefined && loginLD !== null){
		try{
			login = JSON.parse(loginLD);
		}
		catch(e){
			var loginLDL=atob(localStorage.getItem(AuthStorage));
			if(loginLDL !== undefined && loginLDL !== null){
				try{
					login = JSON.parse(loginLDL);
		    	}
				catch(e){
					return null;
				}
			}else{
				return null;
			}
		}
		return login;
	}else{
		return null;
	}
}
	
function setUser(userSt,remember){
	var userEncode =btoa(JSON.stringify(userSt));
	if(remember){
		localStorage.setItem(AuthStorage,userEncode);
	}else{
		sessionStorage.setItem(AuthStorage,userEncode);
	}
}

function getToken(){
	var user=getUser(AuthStorage);
	if(user && user.token){
		return user.token;
	}else{
		return null;
	}
}

function getRoles(){
	var user=getUser(AuthStorage);
	if(user && user.userType){
		return user.userType;
	}else{
		return null;
	}
}


function setLandingRole(role){
	var user=getUser(AuthStorage);
	if(user){
		user.landingRole=role;
		setUser(user,user.remember);
	}
}


function getLandingRole(){
	var user=getUser(AuthStorage);
	if(user && user.landingRole){
		return user.landingRole;
	}else{
		return null;
	}
}

function getUserName(){
	var user=getUser(AuthStorage);
	if(user && user.userName){
		return user.userName;
	}else{
		return null;
	}
}


function getVendorAccess(){
	var user=getUser(AuthStorage);
	if(user && user.vendorAccess){
		return user.vendorAccess;
	}else{
		return null;
	}
}

function presslogOut(){
	var loginLD=sessionStorage.getItem(AuthStorage);
	if(loginLD !== undefined  && loginLD !==null ){
		sessionStorage.removeItem(AuthStorage);
	}else{
		var loginLDL=localStorage.getItem(AuthStorage);
		if(loginLDL !== undefined && loginLDL !== null){
			localStorage.removeItem(AuthStorage);
		}
	}
}