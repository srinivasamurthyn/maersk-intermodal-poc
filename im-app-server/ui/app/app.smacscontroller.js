var smacscontroller = angular.module('smacscontroller', ['ui.bootstrap']);

smacscontroller.controller('AccountCtrl', ['$scope','$location','$http','CommonAuth','UserRegister', function ($scope,$location,$http,CommonAuth,UserRegister) {
	
	//authCheck
	CommonAuth.roleAccess();
	
	//dropdown Role list
	$scope.userName=CommonAuth.getUserName();
	$scope.roleLists=CommonAuth.getRoles();
	
	//change landing page
	$scope.setLanding = function (role){
		if(getLandingRole() !== role.id){
			CommonAuth.setLandingRole(role.id);
			$scope.sideNav = CommonAuth.getSideNav();
			CommonAuth.rederictPage();
		}
	}
	
	//sideNav
	$scope.sideNav = CommonAuth.getSideNav();
	
	
	//register
	$scope.register = function (user){
		if(user){
			
			if(!user.userEmail){
				return alertify.error("Entered valid email ...");
			}
			if(user.password !== user.cpassword){
				return alertify.error("Password not match .... ");
			}
			
			if(user.cpassword.length < 8) {
				return alertify.error("Your password must be at least 8 characters"); 
		    }
		    
			if(user.cpassword.search(/[a-z]/i) < 0) {
		    	return alertify.error("Your password must contain at least one letter.");
		    }
		    
			if(user.cpassword.search(/[0-9]/) < 0) {
		    	return alertify.error("Your password must contain at least one digit."); 
		    }
		   
			UserRegister.query(user).$promise.then(function (res) {
				if(res.success){
					alertify.success(res.success);
				}else{
					alertify.error(res.error);
				}
			});
		}
	}
	
	
	//login
	$scope.user={remember:false};
	$scope.login = function (user) {
		if(user){
			if(user.userName && user.password){
				$http({method: 'POST',url: '/basic/useraccount/login',headers: {'Authorization': 'Basic ' + btoa(user.userName + ":" + user.password)}}).then(function successCallback(response) {
					if(response.data && response.data.error){
						alertify.error(response.data.error);
						return;
					}
					
					var roleArr =[];
					var roles = response.headers().usertype.split(',')
					var roleInfo = CommonAuth.getRoleInfo();
					for(i=0;i<roles.length;i++){
						var obj = roleInfo[roles[i]];
						obj.id = roles[i];
						roleArr.push(obj);
					}
					
					var sUser = {
						userName : user.userName,
						token : response.headers().token,
						userType : roleArr,
						landingRole : response.headers().landingrole,
						vendorAccess : response.headers().vendoraccess,
						remember : user.remember
					}
					
					
					setUser(sUser,user.remember);
					CommonAuth.rederictPage();
				});
			}else{
				return alertify.error("Please check username and password ...");
			}
		}
	}
	
	
	//logout
	$scope.logOut = function (){
		presslogOut();
		CommonAuth.rederictPage();
	}
	
}]);


smacscontroller.controller('VendorHomeCtrl', ['$scope','$http','CommonAuth','SummaryCount','GetVendorList','GetVendorAllList', function ($scope,$http,CommonAuth,SummaryCount,GetVendorList,GetVendorAllList) {
	//authCheck
	CommonAuth.roleAccess();
	
	
	$scope.vendorList = [];
	var access = CommonAuth.getVendorAccess();
	if(access === "ALL"){
		$scope.vendorList = GetVendorAllList.query();
	}else{
		var ids = access.split(",");
		for(i=0;i<ids.length;i++){
			console.log(ids[i]);
			GetVendorList.query({id:parseInt(ids[i])}).$promise.then(function (res) {
				$scope.vendorList.push(res);
			});
		}
	}
	
	
	
	
	
}]);



smacscontroller.controller('HomeCtrl', ['$scope','$http','CommonAuth','SummaryCount', function ($scope,$http,CommonAuth,SummaryCount) {
	
	
	//authCheck
	CommonAuth.roleAccess();

	

	//status count 
	$scope.countall = SummaryCount.query();
	
	
	/*
	 * jQuery code for vector map
	 */
	
	//common map config
	var vectorMs = {
	    backgroundColor: '#a5bfdd',
	    borderColor: '#818181',
	    borderOpacity: 0.25,
	    borderWidth: 1,
	    color: '#f4f3f0',
	    enableZoom: true,
	    hoverColor: '#c9dfaf',
	    hoverOpacity: null,
	    normalizeFunction: 'linear',
	    selectedColor: '#c9dfaf',
	    selectedRegions: null,
	    showTooltip: true,
	}
	
	
	//world map
	vectorMs.map='world_en';
	vectorMs.onRegionClick = function(e, code, region){
   	 	window.location.href = "#/app/country/"+code.toUpperCase();
    };
	jQuery('#vmap').vectorMap(vectorMs);
	
	
	
	/*//mx_en map
	vectorMs.map='mx_en';
	vectorMs.onRegionClick = function(e, code, region){
    	window.location.href = "#/app/country/region/MX/"+code.toUpperCase();
    };
	jQuery('#vmap1').vectorMap(vectorMs);

	
	
	//usa_en
	vectorMs.map='usa_en';
	vectorMs.onRegionClick = function(e, code, region){
    	window.location.href = "#/app/country/region/US/"+code.toUpperCase();
    };
	jQuery('#vmap2').vectorMap(vectorMs);
	
	
	
	//usa_en
	vectorMs.map='russia_en';
	vectorMs.onRegionClick = function(e, code, region){
    	window.location.href = "#/app/country/region/RU/"+code.toUpperCase();
    };
	jQuery('#vmap3').vectorMap(vectorMs);
	
	
	
	//canada_en
	vectorMs.map='canada_en';
	vectorMs.onRegionClick = function(e, code, region){
    	window.location.href = "#/app/country/region/CA/"+code.toUpperCase();
    };
	jQuery('#vmap4').vectorMap(vectorMs);
	
	
	
	//brazil_br
	vectorMs.map='brazil_br';
	vectorMs.onRegionClick = function(e, code, region){
    	window.location.href = "#/app/country/region/BR/"+code.toUpperCase();
    };
	jQuery('#vmap5').vectorMap(vectorMs);
	
	
	
	//germany_en
	vectorMs.map='germany_en';
	vectorMs.onRegionClick = function(e, code, region){
    	window.location.href = "#/app/country/region/FR/"+code.toUpperCase();
    };
	jQuery('#vmap6').vectorMap(vectorMs);*/
	
	
}]);





smacscontroller.controller('CountryCtrl', ['$scope','$stateParams','$http','CommonAuth','ContainerCountByCountry','ContainerByCountry','CustomerList','CarrierList', function ($scope,$stateParams,$http,CommonAuth,ContainerCountByCountry,ContainerByCountry,CustomerList,CarrierList) {	
	
	//authCheck
	CommonAuth.roleAccess();
	
	/*
	 * jQuery code for vector map
	 */
	var countrycode = $stateParams.code;
	$scope.countrycode = $stateParams.code;
	var map_code;
	var customerAllMaps = new Map();
	var carrierAllMaps = new Map();
	var customerMaps = new Map();
	if(countrycode){
		
		//status count 
		$scope.countall = ContainerCountByCountry.query({country:countrycode});
		
		//status list
		$scope.cList = ContainerByCountry.query({country:countrycode});
		
		
		var codes = CommonAuth.getCodes();
		for(var i=0;i<codes.length;i++){
			if(codes[i].Country_Code === countrycode){
				console.log(codes[i].Country_Name);
				$scope.countryName=codes[i].Country_Name;
			}
		}
	}
	
	$scope.change = function (seleted,selectData){
		$scope.metsList=[];
		if(seleted === "All"){
			$scope.metsList = selectData;
		}else{
			for(i=0;i<selectData.length;i++){
				if(seleted === selectData[i].customer.customerName){
					$scope.metsList.push(selectData[i]);
				}
			}
		}
	}
	
	
	
}]);



smacscontroller.controller('RegionCtrl', ['$scope','$stateParams','$http','CommonAuth','CarrierList','CustomerList','WorkOrderByCountryCode', function ($scope,$stateParams,$http,CommonAuth,CarrierList,CustomerList,WorkOrderByCountryCode) {
	 
	//authCheck
	CommonAuth.roleAccess();
	
	var country = $stateParams.country;
	var code = $stateParams.code;
	var customerAllMaps = new Map();
	var carrierAllMaps = new Map();
	var customerMaps = new Map();
	
	
	CarrierList.query().$promise.then(function (carList) {
		CustomerList.query().$promise.then(function (cusList) {
			WorkOrderByCountryCode.query({region:country,code:code}).$promise.then(function (res) {
				console.log(res);
				
				
				
				if(res){
					customerMaps.set("All",{name:"All"});
					
					//All customer list 
					for(j=0;j<cusList.length;j++){
						customerAllMaps.set(cusList[j].customerId,cusList[j]);
					}
					
					//All carList list 
					for(j=0;j<carList.length;j++){
						carrierAllMaps.set(carList[j].carrierId,carList[j]);
					}
					
					for(i=0;i<res.length;i++){
						if(carrierAllMaps.has(res[i].carrierCarrierId)){
							res[i].carrier = carrierAllMaps.get(res[i].carrierCarrierId);
						}
						
						if(customerAllMaps.has(res[i].customerCustomerId)){
							res[i].customer = customerAllMaps.get(res[i].customerCustomerId);
							customerMaps.set(res[i].customer.customerName,{name:res[i].customer.customerName});
						}
					}
				}
				
				$scope.seleted = "All";
				$scope.selectData = Array.from(customerMaps.values());
				$scope.metsList=res;
				$scope.metsListAll=res;
			});
		});
	});
	
	
	$scope.change = function (seleted,selectData){
		$scope.metsList=[];
		if(seleted === "All"){
			$scope.metsList = selectData;
		}else{
			for(i=0;i<selectData.length;i++){
				if(seleted === selectData[i].customer.customerName){
					$scope.metsList.push(selectData[i]);
				}
			}
		}
	}
	
	

	
	
}]);


smacscontroller.controller('TrackingCtrl', ['$scope','$stateParams','$http','$window','CommonAuth','WorkOrderStatus','WorkOrderDetails','CustomerList','CarrierList','FindByContainerNo','FindByTrailerId','TrailerGpsDataByRange','WorkOrderByContainer', function ($scope,$stateParams,$http,$window,CommonAuth,WorkOrderStatus,WorkOrderDetails,CustomerList,CarrierList,FindByContainerNo,FindByTrailerId,TrailerGpsDataByRange,WorkOrderByContainer) {
	
	//authCheck
	CommonAuth.roleAccess();
	
	var map = new google.maps.Map(document.getElementById('map_canvas'), {
         center: {lat: 6.826622, lng: -5.049226},
         zoom: 6
    });
	
	$scope.cont={}
	var id = $stateParams.id;
	if(id){
		$scope.cont.ContainerType = id.substring(0, 4);
		$scope.cont.ContainerNo = id.substring(4, 20);
	}
	
	$scope.tables=false;
	
	
	var customerAllMaps = new Map();
	var carrierAllMaps = new Map();
	
	CarrierList.query().$promise.then(function (carList) {
		CustomerList.query().$promise.then(function (cusList) {
			//All customer list 
			for(j=0;j<cusList.length;j++){
				customerAllMaps.set(cusList[j].customerId,cusList[j]);
			}
			
			//All carList list 
			for(j=0;j<carList.length;j++){
				carrierAllMaps.set(carList[j].carrierId,carList[j]);
			}
		});
	});
	
	
	
	
	$scope.search = function(containerNo){
		FindByContainerNo.query({containerNo:containerNo}).$promise.then(function (res) {
			$scope.containers = res;
		});
	}
	
	
	$scope.track =function (trailerId,cont){
		FindByTrailerId.query({trailerId:parseInt(trailerId)}).$promise.then(function (trailer) {
			if(trailer.trailerNo){
				
				if(!trailer.endTime){
					trailer.endTime=parseInt(new Date().getTime());
				}
				
				TrailerGpsDataByRange.query({trailerId:trailer.trailerNo,startTime:trailer.startTime,endTime:trailer.endTime}).$promise.then(function (gps) {
					
					$scope.trailerNo=trailer.trailerNo;
					$scope.containerNo = cont.containerNo;
					$scope.trailerId = trailerId;
					
					//declare
					var locs = [];
					var center = {};
					var trailerStartIcon = "assets/images/markericon/truck.png";
					var containerIcon = "assets/images/markericon/container.png";
					var dotsIcon = "assets/images/markericon/dot.png";
					var distanceLocs = 1000;
					var conLoad = false;
					var conunLoad = false;
					
					//sort by time
					gps.sort(function(x, y){
					    return x.gpsdateepoch - y.gpsdateepoch;
					})
					
					
					var lastlat = null;
					var lastlat = null;
					for(i=0;i<gps.length;i++){
						if(i === 0){
							lastlat = gps[i].gpslat;
							lastlon = gps[i].gpslon;
							center = {lat:gps[i].gpslat,lon:gps[i].gpslon}
							locs.push({lat:gps[i].gpslat,lon:gps[i].gpslon,timeStamp:parseInt(gps[i].gpsdateepoch),info:'Trailer Started',icon:trailerStartIcon,idInfo:'Trailer Id',id:trailer.trailerNo});
							if(!conLoad && cont && distanceGeoLoc(cont.sloclat,cont.sloclon,gps[i].gpslat,gps[i].gpslon) <= distanceLocs){
								locs.push({lat:cont.sloclat,lon:cont.sloclon,timeStamp:parseInt(cont.loadedTime),info:'Container Loaded',icon:containerIcon,idInfo:'Container Id',id:cont.containerNo});
								lastlat = gps[i].gpslat;
								lastlon = gps[i].gpslon;
								conLoad = true;
							}
						}else{
							if(!conLoad && cont && distanceGeoLoc(cont.sloclat,cont.sloclon,gps[i].gpslat,gps[i].gpslon) <= distanceLocs){
								locs.push({lat:cont.sloclat,lon:cont.sloclon,timeStamp:parseInt(cont.loadedTime),info:'Container Loaded',icon:containerIcon,idInfo:'Container Id',id:cont.containerNo});
								lastlat = gps[i].gpslat;
								lastlon = gps[i].gpslon;
								conLoad = true;
							}else if (!conunLoad && cont && distanceGeoLoc(cont.eloclat,cont.eloclon,gps[i].gpslat,gps[i].gpslon) <= distanceLocs){
								locs.push({lat:cont.eloclat,lon:cont.eloclon,timeStamp:parseInt(cont.unloadedTime),info:'Container Unloaded',icon:containerIcon,idInfo:'Container Id',id:cont.containerNo});
								lastlat = gps[i].gpslat;
								lastlon = gps[i].gpslon;
								conunLoad = true;
							}else{
								if(distanceGeoLoc(lastlat,lastlon,gps[i].gpslat,gps[i].gpslon) > 300){
									locs.push({lat:gps[i].gpslat,lon:gps[i].gpslon,timeStamp:parseInt(gps[i].gpsdateepoch),info:'Time',icon:dotsIcon,idInfo:'Trailer Id',id:trailer.trailerNo});
									lastlat = gps[i].gpslat;
									lastlon = gps[i].gpslon;
								}
							}
							
						}
					}
					
					if(locs[locs.length-1]){
						
						if(locs[locs.length-1].idInfo === "Container Id"){
							locs.push({lat:locs[locs.length-1].lat,lon:locs[locs.length-1].lon,timeStamp:parseInt(locs[locs.length-1].timeStamp),info:'Trailer LastSeen',icon:trailerStartIcon,idInfo:'Trailer Id',id:trailer.trailerNo});
						}else{
							locs[locs.length-1].info = "Trailer LastSeen";
							locs[locs.length-1].icon = trailerStartIcon;
						}
						
					}
					
					console.log(locs);
					
					
					//map init
					DrawRoute(trailerId,'map_canvas',locs,6,center);
					
					
					
					$scope.tables=true;
					
				});
				
				
				
				WorkOrderByContainer.query({containerNo:cont.containerNo}).$promise.then(function (wo) {
					if(carrierAllMaps.has(wo.carrierCarrierId)){
						wo.carrier = carrierAllMaps.get(wo.carrierCarrierId);
					}
							
					if(customerAllMaps.has(wo.customerCustomerId)){
						wo.customer = customerAllMaps.get(wo.customerCustomerId);
					}
						
					$scope.metsData=wo;
					
					
					
					
					
				});
				
				
			
				
				
				
			}	
		});
	}
	

	
}]);


smacscontroller.controller('CustomerCtrl', ['$scope','$http','CommonAuth', function ($scope,$http,CommonAuth) {
	//authCheck
	CommonAuth.roleAccess();
	
}]);

smacscontroller.controller('CustomerCareCtrl', ['$scope','$http','CommonAuth', function ($scope,$http,CommonAuth) {
	//authCheck
	CommonAuth.roleAccess();
	
}]);

smacscontroller.controller('WorkOrderCtrl', ['$scope','AddWorkOrder','CarrierList','CustomerList','VendorList', function ($scope,AddWorkOrder,CarrierList,CustomerList,VendorList) {

	$scope.carrierList = CarrierList.query();
	$scope.customerList = CustomerList.query();
	$scope.vendorList = VendorList.query();
	
	
	$scope.AddWorkOrder = function(record){
		
		if(record === undefined || record.workOrderNo === "" || record.workOrderNo === null || record.workOrderNo === undefined){
			alertify.error("Please Enter Work Order No");
			return;
		
		
		}
		
		if(record.containerType === undefined || record.containerType === "" || record.containerType === null ){
			alertify.error("Please Select Container Type");
			return;
		}
		
		if(record.containerNo === undefined || record.containerNo === "" || record.containerNo === null ){
			alertify.error("Please Enter Container Number");
			return;
		}
		
		if(record.beginAddressline1 === undefined || record.beginAddressline1 === "" || record.beginAddressline1 === null ){
			alertify.error("Please Enter the Begin Address");
			return;
		}
		
		if(record.endAddressline1 === undefined || record.endAddressline1 === "" || record.endAddressline1 === null ){
			alertify.error("Please Enter the End Address");
			return;
		}
		
		
		
		var woCreatedYearWeek = undefined;
		var woCompleteYearWeek = undefined;
		
		if(record.ccYear != undefined && record.ccYear != "" && record.ccYear != null ){
			woCompleteYearWeek = record.ccYear+''+record.ccWeek;
		}
		
		if(record.cYear != undefined && record.cYear != "" && record.cYear != null ){
			woCreatedYearWeek = record.cYear+''+record.cWeek;
		}
		
		
	//}
		
		
		
		//return;
		AddWorkOrder.query({
			workOrderNo:record.workOrderNo,
			woCreatedYearWeek : woCreatedYearWeek,

			woMode : record.woMode,
			woCompleteYearWeek : woCompleteYearWeek,
			woCompleteDate : record.woCompleteDate,
			woDetailType : record.woDetailType,
			equipmentNumbers : record.equipmentNumbers,
			beginAddressline1 : record.beginAddressline1,
			beginAddressline2 : record.beginAddressline2,
			beginAddressline3 : record.beginAddressline3,
			beginZip : record.beginZip,
			endAddressline1 : record.endAddressline1,
			endAddressline2 : record.endAddressline2,
			endAddressline3 : record.endAddressline3,
			endZip : record.endZip,
			countryCode : record.countryCode,
			stateCode : record.stateCode,
			clearencePort : record.clearencePort,
			blNumber : record.blNumber,
			blNumberOriginal : record.blNumberOriginal,
			comment : record.comment,
			commodityDescription : record.commodityDescription,

			departureDate : new Date(record.departureDate).getTime(),
			arrivalDate : new Date(record.arrivalDate).getTime(),
			
			carrier : record.carrier,
			customer : record.customer,
			vendor : parseInt(record.vendor),
			
			containerNo : record.containerType+record.containerNo
			
			
			
		}).$promise.then(function (res,err) {
			console.log(err);
			console.log(res);
			
			if(err){
				alertify.error("Error");
			}else{
				alertify.success("WorkOrder Added");
				$scope.record = null;
			}
			
		});
	}
		
}]);



smacscontroller.controller('VendorCtrl', ['$scope','$window','$stateParams','$http','$location','CommonAuth','GetVendorList', function ($scope,$window,$stateParams,$http,$location,CommonAuth,GetVendorList) {
	//authCheck
	CommonAuth.roleAccess();
	var vendorId = $stateParams.id;
	
	$scope.vendor = GetVendorList.query({id:vendorId});
	
	
}]);







smacscontroller.controller('workorderalistCtrl', ['$scope','$window','$http','$location','CommonAuth','TrailerById','workorderList','ContainerByTrailerId','AddWorkOrder','AddWorkOrderBulk','WorkOrderRemove', function ($scope,$window,$http,$location,CommonAuth,TrailerById,workorderList,ContainerByTrailerId,AddWorkOrder,AddWorkOrderBulk,WorkOrderRemove) {
	CommonAuth.roleAccess();
	
	$scope.listall=workorderList.query();
	$scope.uCount = 0;
	$scope.uLength = 0;
	
	
	$scope.showWorkOrder = function (wo){
		$scope.workOrder = wo;
		document.getElementById('id02').style.display='block'
	}
	
	$scope.showRemove = function (won){
		$scope.removeNo = won;
		document.getElementById('id03').style.display='block'
	}
	
	$scope.removeWo = function (won){
		console.log(won)
		WorkOrderRemove.query({workOrderNo:won}).$promise.then(function (res) {
			console.log(res);
			if(res.error){
				alertify.error("Error : "+ res.error);
				document.getElementById('id03').style.display='none';
			}else{
				alertify.success("Successfully Removed");
				$scope.listall=workorderList.query();
				document.getElementById('id03').style.display='none';
			}
		});
	}
	
	
	$scope.upload = function (data){
		//console.log(data);
		
		var array = [];
		$scope.uLength = data.length;
		for(i=0;i<data.length;i++){
			//addWorkOrder(data[i],i);
			
			var record = data[i];
			
			var woCreatedYearWeek = undefined;
			var woCompleteYearWeek = undefined;
			
			if(record.ccYear != undefined && record.ccYear != "" && record.ccYear != null ){
				woCompleteYearWeek = record.ccYear+''+record.ccWeek;
			}
			
			if(record.cYear != undefined && record.cYear != "" && record.cYear != null ){
				woCreatedYearWeek = record.cYear+''+record.cWeek;
			}
			
			
			var obj = {
			workOrderId : record.workOrderNo+"_"+record.containerNo,
			workOrderNo:record.workOrderNo,
			woCreatedYearWeek : woCreatedYearWeek,

			woMode : record.woMode,
			woCompleteYearWeek : woCompleteYearWeek,
			woCompleteDate : record.woCompleteDate,
			woDetailType : record.woDetailType,
			equipmentNumbers : record.equipmentNumbers,
			beginAddressline1 : record.beginAddressline1,
			beginAddressline2 : record.beginAddressline2,
			beginAddressline3 : record.beginAddressline3,
			beginZip : record.beginZip,
			endAddressline1 : record.endAddressline1,
			endAddressline2 : record.endAddressline2,
			endAddressline3 : record.endAddressline3,
			endZip : record.endZip,
			countryCode : record.countryCode,
			stateCode : record.stateCode,
			clearencePort : record.clearencePort,
			blNumber : record.blNumber,
			blNumberOriginal : record.blNumberOriginal,
			comment : record.comment,
			commodityDescription : record.commodityDescription,

			departureDate : new Date(record.departureDate).getTime(),
			arrivalDate : new Date(record.arrivalDate).getTime(),
			
			carrierCarrierCode : record.carrierCode,
			customerCustomerNumber : record.customerNumber,
			vendorVendorId : parseInt(record.vendorId),
			
			containerNo : record.containerNo,
			geoCodeStatus : "NO",
			linkWoToConStatus : "NO",
			
			status : "ACTIVE"
			
			};
			
			array.push(obj);
			
			if(data.length-1 === i){
				AddWorkOrderBulk.query({woArray:array}).$promise.then(function (res,err) {

					console.log('err',err);
					console.log('res',res);
					
					
					if(err){
						$scope.record = null;
						alertify.error("Error");
						document.getElementById('id01').style.display='none';
						$scope.listall=workorderList.query();
					}else{
						$scope.record = null;
						$scope.listall=workorderList.query();
						document.getElementById('id01').style.display='none';
						alertify.success(res.length+" records uploaded...");
					}
				});
			}
			
			
		}
		
		
	}
	
	
	
	
	
	
	function addWorkOrder(record,index){
		
		if(record === undefined || record.workOrderNo === "" || record.workOrderNo === null || record.workOrderNo === undefined){
			alertify.error("Please Check Work Order No");
			return false;
		}
		
		
		if(record.containerNo === undefined || record.containerNo === "" || record.containerNo === null ){
			alertify.error("Please Enter Container Number");
			return false;
		}
		
		if(record.beginAddressline1 === undefined || record.beginAddressline1 === "" || record.beginAddressline1 === null ){
			alertify.error("Please Enter the Begin Address");
			return false;
		}
		
		if(record.endAddressline1 === undefined || record.endAddressline1 === "" || record.endAddressline1 === null ){
			alertify.error("Please Enter the End Address");
			return false;
		}
		
		
		
		var woCreatedYearWeek = undefined;
		var woCompleteYearWeek = undefined;
		
		if(record.ccYear != undefined && record.ccYear != "" && record.ccYear != null ){
			woCompleteYearWeek = record.ccYear+''+record.ccWeek;
		}
		
		if(record.cYear != undefined && record.cYear != "" && record.cYear != null ){
			woCreatedYearWeek = record.cYear+''+record.cWeek;
		}
		
		
	//}
		
		
		
		//return;
		AddWorkOrder.query({
			workOrderNo:record.workOrderNo,
			woCreatedYearWeek : woCreatedYearWeek,

			woMode : record.woMode,
			woCompleteYearWeek : woCompleteYearWeek,
			woCompleteDate : record.woCompleteDate,
			woDetailType : record.woDetailType,
			equipmentNumbers : record.equipmentNumbers,
			beginAddressline1 : record.beginAddressline1,
			beginAddressline2 : record.beginAddressline2,
			beginAddressline3 : record.beginAddressline3,
			beginZip : record.beginZip,
			endAddressline1 : record.endAddressline1,
			endAddressline2 : record.endAddressline2,
			endAddressline3 : record.endAddressline3,
			endZip : record.endZip,
			countryCode : record.countryCode,
			stateCode : record.stateCode,
			clearencePort : record.clearencePort,
			blNumber : record.blNumber,
			blNumberOriginal : record.blNumberOriginal,
			comment : record.comment,
			commodityDescription : record.commodityDescription,

			departureDate : new Date(record.departureDate).getTime(),
			arrivalDate : new Date(record.arrivalDate).getTime(),
			
			carrier : record.carrierCode,
			customer : record.customerNumber,
			vendor : parseInt(record.vendorId),
			
			containerNo : record.containerNo
			
			
			
		}).$promise.then(function (res,err) {
			console.log(err);
			console.log(res);
			console.log($scope.uCount+'/'+$scope.uLength+'='+index);
			
			
			if(err){
				alertify.error("Error");
				if(index === $scope.uLength-1){
					alertify.success($scope.uCount + " out of " + $scope.uLength+" WorkOrder Added");
					console.log($scope.uCount+'/'+$scope.uLength);
					$scope.listall=workorderList.query();
					document.getElementById('id01').style.display='none';
				}
				return false;
			}else{
				$scope.uCount++;
				//alertify.success("WorkOrder Added");
				$scope.record = null;
				if(index === $scope.uLength-1){
					alertify.success($scope.uCount + " out of " + $scope.uLength+" WorkOrder Added");
					console.log($scope.uCount+'/'+$scope.uLength);
					$scope.listall=workorderList.query();
					document.getElementById('id01').style.display='none';
				}
				return true;
			}
			
		});
	}
	
	
	
	$scope.searchTrailer = function(trailerNo){
		TrailerById.query({id:trailerNo}).$promise.then(function (res,err) {
		
			$scope.trailer = res; 
			if(res.trailerId){
			ContainerByTrailerId.query({trailerId:res.trailerId}).$promise.then(function (conts) {
				for(i=0;i<conts.length;i++){
					if(conts[i].status == "LOADED"){
						$scope.StopTrailer = false;
					}
					conts[i].ContainerType = conts[i].containerNo.substring(0, 4);
					conts[i].ContainerNo = conts[i].containerNo.substring(4, 20);
				}
				$scope.trailer.containers=conts;
			});
			}
		});
		$scope.trailerFound = true;
	}

      }]);







smacscontroller.controller('DriverCtrl', ['$scope','$window','$http','$ocLazyLoad', 'ASSETS','CommonAuth','AddTrailer','TrailerById','ContainerByTrailerId','AddContainer','UnLoadedContainer','StopTrailerByTrailerId','ContainerPrefix', function ($scope,$window,$http,$ocLazyLoad,ASSETS,CommonAuth,AddTrailer,TrailerById,ContainerByTrailerId,AddContainer,UnLoadedContainer,StopTrailerByTrailerId,ContainerPrefix) {
	//authCheck
	CommonAuth.roleAccess();
	
/*	$ocLazyLoad.load([
        ASSETS.js('utility','lock'),
    ]);*/
	
	
	$scope.containerprefix = ContainerPrefix.query();
	
	var trailer = GetContainers();
	$scope.StopTrailer = true;
	
	if(trailer && trailer.trailerNo){
		TrailerById.query({id:trailer.trailerNo}).$promise.then(function (res) {
			$scope.trailer = res;
			if(res.trailerId){
				ContainerByTrailerId.query({trailerId:res.trailerId}).$promise.then(function (conts) {
					for(i=0;i<conts.length;i++){
						if(conts[i].status == "LOADED"){
							$scope.StopTrailer = false;
						}
						conts[i].ContainerType = conts[i].containerNo.substring(0, 4);
						conts[i].ContainerNo = conts[i].containerNo.substring(4, 20);
					}
					$scope.trailer.containers=conts;
				});
			}
			
			
			if(res.status === "STARTED"){
				if(window.location.hash !== "#/managecontainer"){
					window.location  = "#/managecontainer";
				}
			}else{
				if(window.location.hash !== "#/starttrailer"){
					window.location  = "#/starttrailer";
				}
			}
		});
	}else{
		if(window.location.hash !== "#/starttrailer"){
			window.location  = "#/starttrailer";
		}
	}
	
	
	var cap = [
		{Q:'app/views/utility/images/c2.png',A:'51515'},
		{Q:'app/views/utility/images/c3.png',A:'433841'},
		{Q:'app/views/utility/images/c1.jpg',A:'1232345'}
	]
	
	var index = 0;
	$scope.capdis = cap[index];
	$scope.reload = function (){
		index = index+1;
		$scope.capdis = cap[index];
		if(index === 2){
			index = 0;
		}
	}
	//startTrailer
	$scope.startTrailer = function(trailer){
		if(trailer){
			var currentTime = new Date().getTime();
			AddTrailer.query({trailerNo:trailer.toUpperCase().trim(),status:"STARTED",startTime:currentTime}).$promise.then(function (res) {
				console.log(res);
				if(res.error){
					alertify.error("Error : "+ res.error);
				}else{
					SaveContainers({trailerNo:trailer,status:"STARTED",startTime:currentTime});
					window.location  = "#/managecontainer";
				}
			});
		}else{
			alertify.error("Error : Invalid Trailer No");
		}
		
	}
	
	
	
	
	
	//add new row
	$scope.addRow = function (){
		$scope.trailer.containers.push({id:new Date().getTime(),isSaved : false, ContainerType : "MAEU"});
	}
	
	//remove new row
	$scope.RemoveRow = function (index){
		$scope.trailer.containers.splice(index, 1);
	}
	
	//unloaded
	$scope.unloadRow = function (con,index){
		
		if (index > -1) {
			
			if(!con.containerNo){
				con.containerNo = con.ContainerType+con.ContainerNo;
			}
			
			UnLoadedContainer.query({containerNo:con.containerNo.trim()}).$promise.then(function (res) {
				if(res.error){
					alertify.error("Error : "+ res.error);
				}else{
					con.status="UNLOADED";
					$scope.trailer.containers[index]=con;
					$scope.StopTrailer = true;
					for(i=0;i<$scope.trailer.containers.length;i++){
						if($scope.trailer.containers[i].status == "LOADED"){
							$scope.StopTrailer = false;
							window.location.reload();
						}
					}
					alertify.success("Unloaded Successfully");
				}
			});
		}
	}
	
	//loaded 
	$scope.saveRow = function (con,index) {
		if(con.ContainerNo=== undefined || con.ContainerNo === "" || con.ContainerNo=== null ||con.ContainerNo <10 ){
			alertify.error("Please enter Valid  ContainerNo");
			return;
		}
		
		AddContainer.query({containerNo:con.ContainerType+con.ContainerNo,status:"LOADED",trailerId:$scope.trailer.trailerId}).$promise.then(function (res) {
			if(res.error){
				alertify.error("Error : "+ res.error);
			}else{
				con.status="LOADED";
				$scope.trailer.containers[index]=con;
				$scope.StopTrailer = true;
				for(i=0;i<$scope.trailer.containers.length;i++){
					if($scope.trailer.containers[i].status == "LOADED"){
						$scope.StopTrailer = false;
						
					}
				}
				alertify.success("Container Loaded ");
			}
		});
	}
	
	
	//stop tailer
	$scope.stopTrailer = function(trailerNo){
		StopTrailerByTrailerId.query({trailerNo:trailerNo}).$promise.then(function (res) {
			if(res.error){
				alertify.error("Error : "+ res.error);
			}else{
				RemoveContainer();
				window.location  = "#/starttrailer";
			}
		});
	}
	
	
	
	
	
	
	
	
}]);






smacscontroller.controller('ListContainerCtrl', ['$scope','$stateParams','CommonAuth','FindContainersByStatus','LiveStatus','LiveStatusByCountry','ContainerByStatusCountry','SearchContainers', function ($scope,$stateParams,CommonAuth,FindContainersByStatus,LiveStatus,LiveStatusByCountry,ContainerByStatusCountry,SearchContainers) {
	//authCheck
	CommonAuth.roleAccess();
	$scope.status = $stateParams.status;
	$scope.country = $stateParams.country;
	var limit = 1000;
	var page = 1;
	$scope.searchEnable = false;
	
	if($scope.country){
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			$scope.contlist=ContainerByStatusCountry.query({country:$scope.country,status:$scope.status});
		}else{
			$scope.contlist=LiveStatusByCountry.query({country:$scope.country,status:$scope.status});
		}
	}else{
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			$scope.contlist = FindContainersByStatus.query({status:$scope.status,page:page,limit:limit});
		}else{
			$scope.contlist=LiveStatus.query({status:$scope.status});
		}
	}

	$scope.goPage = function (cpage){
		var call = false;
		if(cpage.page === "pre" && cpage.vclass !== "prev disabled"){
			page = page - 1 ;
			call = true;
		}else if (cpage.page === "next" && cpage.vclass !== "next disabled"){
			page = page + 1 ;
			call = true;
		}else if(cpage.page !== "pre" && cpage.page !== "next"){
			page = cpage.page
			call = true;
		}
		if(call){
			$scope.contlist = FindContainersByStatus.query({status:$scope.status,page:page,limit:limit});
		}
	}
	
	
	$scope.goSearchPage = function (cpage){
		
		console.log(cpage);
		
		var call = false;
		if(cpage.page === "pre" && cpage.vclass !== "prev disabled"){
			page = page - 1 ;
			call = true;
		}else if (cpage.page === "next" && cpage.vclass !== "next disabled"){
			page = page + 1 ;
			call = true;
		}else if(cpage.page !== "pre" && cpage.page !== "next"){
			page = cpage.page
			call = true;
		}
		if(call){
			if($scope.scontainerNo){
				$scope.contlist = SearchContainers.query({key:"containerNo",value:$scope.scontainerNo,status:$scope.status,page:page,limit:limit});
			}else if($scope.sworder){
				$scope.contlist = SearchContainers.query({key:"worder",value:$scope.sworder,status:$scope.status,page:page,limit:limit});
			}else if($scope.svendor){
				$scope.contlist = SearchContainers.query({key:"vendor",value:$scope.svendor,status:$scope.status,page:page,limit:limit});
			}
			$scope.searchEnable = true;
		}
	}
	
	
	$scope.searchContainers = function (key,value){
		switch (key) {
		case "worder":
			$scope.scontainerNo = null;
			$scope.svendor = null;
			break;
		case "containerNo":
			$scope.sworder = null;
			$scope.svendor = null;	
			break;
		case "vendor":
			$scope.scontainerNo = null;
			$scope.sworder = null;
			break;
		default:
			break;
		}
		$scope.contlist = SearchContainers.query({key:key,value:value,status:$scope.status,page:page,limit:limit});
		$scope.searchEnable = true;
	}
	
	$scope.clearSearch = function (){
		$scope.searchEnable = false;
		$scope.sworder = null;
		$scope.svendor = null;	
		$scope.scontainerNo = null;
		$scope.contlist = FindContainersByStatus.query({status:$scope.status,page:page,limit:limit});
	}
		
		
}]);


/*----------------------------------------------------------------------*/

smacscontroller.controller('ListByStatusCtrl', ['$scope','$stateParams','CommonAuth','FindContainersByStatus','LiveStatus','LiveStatusByCountry','ContainerByStatusCountry', function ($scope,$stateParams,CommonAuth,FindContainersByStatus,LiveStatus,LiveStatusByCountry,ContainerByStatusCountry) {
	//authCheck
	CommonAuth.roleAccess();
	$scope.status = $stateParams.status;
	$scope.country = $stateParams.country;
	
	$scope.filteredcList,$scope.cList = [];
	$scope.currentPage = 1;
	$scope.numPerPage = 10;
	$scope.maxSize = 5;
	
		if($scope.country){
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			$scope.cList=ContainerByStatusCountry.query({country:$scope.country,status:$scope.status});
		}else{
			$scope.cList=LiveStatusByCountry.query({country:$scope.country,status:$scope.status});
		}
	}else{
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			FindContainersByStatus.query({status:$scope.status}).$promise.then(function (res) {
				$scope.cList=res;
				$scope.filteredcList = res.slice(0, $scope.numPerPage);
			});
			
		}else{
			$scope.cList=LiveStatus.query({status:$scope.status});
		}
	}


	  $scope.$watch("currentPage + numPerPage", function() {
	    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
	    , end = begin + $scope.numPerPage;

	    $scope.filteredcList = $scope.cList.slice(begin, end);
	  });
		
		
}]);




smacscontroller.controller('ListByStatusloadCtrl', ['$scope','$stateParams','CommonAuth','FindContainersByStatus','LiveStatus','LiveStatusByCountry','ContainerByStatusCountry', function ($scope,$stateParams,CommonAuth,FindContainersByStatus,LiveStatus,LiveStatusByCountry,ContainerByStatusCountry) {
	//authCheck
	CommonAuth.roleAccess();
	$scope.status = $stateParams.status;
	$scope.country = $stateParams.country;
	
	$scope.filteredcList,$scope.cList = [];
	$scope.currentPage = 1;
	$scope.numPerPage = 10;
	$scope.maxSize = 5;
	
		if($scope.country){
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			$scope.cList=ContainerByStatusCountry.query({country:$scope.country,status:$scope.status});
		}else{
			$scope.cList=LiveStatusByCountry.query({country:$scope.country,status:$scope.status});
		}
	}else{
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			FindContainersByStatus.query({status:$scope.status}).$promise.then(function (res) {
				$scope.cList=res;
				$scope.filteredcList = res.slice(0, $scope.numPerPage);
			});
			
		}else{
			$scope.cList=LiveStatus.query({status:$scope.status});
		}
	}


	  $scope.$watch("currentPage + numPerPage", function() {
	    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
	    , end = begin + $scope.numPerPage;

	    $scope.filteredcList = $scope.cList.slice(begin, end);
	  });
		
		
}]);




smacscontroller.controller('ListByStatusloadCtrl1',['$scope','$stateParams','$http','$timeout','CommonAuth','FindContainersByStatus','LiveStatus','LiveStatusByCountry','ContainerByStatusCountry', function ($scope,$stateParams,$http,$timeout,CommonAuth,FindContainersByStatus,LiveStatus,LiveStatusByCountry,ContainerByStatusCountry) {
	
	
	$http.get('cList').success(function(user_data) {
        $$scope.file=user_data;
        $scope.current_grgid=1;
       /* $scope.data_limit=10;*/
        $scope.filter_data=$scope.file.length;
        $scope.entire_user=$scope.file.length;
	});
	
	$scope.page_position=function(page_number) {
		    $scope.current_grid=page_number;
	};
		
	$scope.filter=function() {
		    $timeout(function() {
		        $scope.filter_data=$scope.length;
		    }, 20);
	};
	
	$scope.sort_with=function(base) {
		    $scope.base=base;
		    $scope.reverse=$scope.reverse;
	};
	
	
	
	
	
	
	
	
	//authCheck
	CommonAuth.roleAccess();
	$scope.status = $stateParams.status;
	$scope.country = $stateParams.country;
	
	
	
		if($scope.country){
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			$scope.cList=ContainerByStatusCountry.query({country:$scope.country,status:$scope.status});
		}else{
			$scope.cList=LiveStatusByCountry.query({country:$scope.country,status:$scope.status});
		}
	}else{
		if($scope.status === 'LOADED' || $scope.status === "UNLOADED"){
			FindContainersByStatus.query({status:$scope.status}).$promise.then(function (res) {
				$scope.cList=res;
				$scope.filteredcList = res.slice(0, $scope.numPerPage);
			});
			
		}else{
			$scope.cList=LiveStatus.query({status:$scope.status});
		}
	}

		 $scope.filter=function() {
		        $timeout(function() {
		            $scope.filter_data=$scope.searched.length;
		        }, 20);
		    };
		    smacscontroller.filter('beginning_data',function() {
		        return function(input,begin) {
		            
		                if(input) {
		                        begin=+begin;
		                        return input.slice(begin);
		                }
		            return [];
		        }
		});
	 		
		
}]);
													






























smacscontroller.controller('TrackingContainerCtrl', ['$scope','$stateParams','$http','$window','CommonAuth','FindByContainerId','FindByTrailerId','TrailerGpsDataByRange','WorkOrderById','HearMapApi', function ($scope,$stateParams,$http,$window,CommonAuth,FindByContainerId,FindByTrailerId,TrailerGpsDataByRange,WorkOrderById,HearMapApi) {
	
	//authCheck
	CommonAuth.roleAccess();
	
	
	
	
	$scope.goBack = function (){
	    window.history.back();
	}
	var id = parseInt($stateParams.id);
	FindByContainerId.query({id:id}).$promise.then(function (res) {
		$scope.containers = res;
		var cont = res;
		if($scope.containers.trailerTrailerId){
			FindByTrailerId.query({trailerId:parseInt($scope.containers.trailerTrailerId)}).$promise.then(function (trailer) {
				if(trailer.trailerNo){
					var endTime = cont.unloadedTime;
					if(!endTime){
						endTime=parseInt(new Date().getTime());
					}
					
					TrailerGpsDataByRange.query({trailerId:trailer.trailerNo,startTime:cont.loadedTime,endTime:endTime}).$promise.then(function (gps) {
						
						$scope.trailerNo=trailer.trailerNo;
						$scope.containerNo = cont.containerNo;
						$scope.trailerId = trailer.trailerId;
						$scope.startTime=trailer.startTime;
						$scope.endTime=trailer.endTime;
						$scope.loadedTime=trailer.loadedTime;
						
						//sort by time
						gps.sort(function(x, y){
						    return x.gpsdateepoch - y.gpsdateepoch;
						})
						
						
						//slice every 100 record
						var before=[];
						for(i=0;i<gps.length;i++){
							var i,j,temparray,chunk = 100;
							for (i=0,j=gps.length; i<j; i+=chunk) {
							    temparray = gps.slice(i,i+chunk);
							    before.push(temparray);
							}
						}
						
						//declare
						var locations = [];
						var markers = [];
						var center = {};
						var i;
						
						//ETA
						var lastLoc;
						var desLoc;
						
						
						
						for(i=0;i<before.length;i++){
							
							//trailer start marker
							if(i === 0){
								markers.push({lat:before[i][0].gpslat,lng:before[i][0].gpslon,timeStamp:parseInt(before[i][0].gpsdateepoch),titleType:"Trailer",titleDisc:"Trailer Started",titleName:trailer.trailerNo});
							}
							
							var locs = {};
							for(var j=0;j<before[i].length;j++){
								if(i !== 0 && j === 0){
									locs['waypoint'+j] = 'geo!'+center.lat+','+center.lng;
								}else{
									locs['waypoint'+j] = 'geo!'+before[i][j].gpslat+','+before[i][j].gpslon;
									center.lat=before[i][j].gpslat;
									center.lng=before[i][j].gpslon;
								}
								
								//trailer stop marker
								if((i === before.length - 1 ) && (j === before[i].length - 1 )){
									lastLoc = before[i][j].gpslat+','+before[i][j].gpslon;
									markers.push({lat:before[i][j].gpslat,lng:before[i][j].gpslon,timeStamp:parseInt(before[i][j].gpsdateepoch),titleType:"Trailer",titleDisc:"Trailer LastSeen",titleName:trailer.trailerNo});
								}
								
							}
							locations.push(locs);
						}
						
						if(cont.eloclat && cont.eloclon){
							var desLoc = cont.eloclat+','+cont.eloclon;
							getETA(lastLoc,desLoc,function(err,etaData){
								if(etaData && etaData.response && etaData.response.route[0] && etaData.response.route[0].summary){
									var summary = etaData.response.route[0].summary.text;
									summary = summary.replace('</span','');
									summary = summary.replace('</span','');
									var arr = summary.split('>');
									$scope.etaSummary = {etaDistance:arr[1],etaDuration:arr[3]};
								}
							});
						}
						
						
						
						
						if(res.workOrderId){
							WorkOrderById.query({id:res.workOrderId}).$promise.then(function (wo) {
								if(wo){
									$scope.metsData = wo;
									//start marker
									if(wo.beginLat && wo.beginLon){
										var startAddress = "";
										if(wo.beginAddressline1){
											startAddress = wo.beginAddressline1;
										}
										if(wo.beginAddressline2){
											startAddress = startAddress +','+ wo.beginAddressline2;
										}
										if(wo.beginAddressline3){
											startAddress = startAddress +','+ wo.beginAddressline3;
										}
										markers.push({lat:wo.beginLat,lng:wo.beginLon,titleType:"PICKUP",titleDisc:startAddress,titleName:"Begin Address"});
									}
									
									//end marker
									if(wo.endLat && wo.endLon){
										var endAddress = "";
										if(wo.endAddressline1){
											endAddress = wo.endAddressline1;
										}
										if(wo.endAddressline2){
											endAddress = endAddress +','+ wo.endAddressline2;
										}
										if(wo.endAddressline3){
											endAddress = endAddress +','+ wo.endAddressline3;
										}
										markers.push({lat:wo.endLat,lng:wo.endLon,titleType:"DROP",titleDisc:endAddress,titleName:"End Address"});
									}
									
									HearMapApi.query().$promise.then(function (config) {
										setHearMap(config,locations,markers,center);
									});
								}else{
									HearMapApi.query().$promise.then(function (config) {
										setHearMap(config,locations,markers,center);
									});
								}
							});
						}else{
							HearMapApi.query().$promise.then(function (config) {
								setHearMap(config,locations,markers,center);
							});
						}
					});
				}
			});
		}
	});
}]);







//TrackingTrailerCtrl

smacscontroller.controller('TrackingTrailerCtrl', ['$scope','$stateParams','$http','$window','CommonAuth','FindByContainerId','FindByTrailerId','TrailerGpsDataByRange','WorkOrderById', function ($scope,$stateParams,$http,$window,CommonAuth,FindByContainerId,FindByTrailerId,TrailerGpsDataByRange,WorkOrderById) {
	
	//authCheck
	CommonAuth.roleAccess();
	
	
	
	$scope.trackTrailer = function (trailerId){
		console.log(trailerId);
	}
	
	
	
	/*FindByContainerId.query({id:id}).$promise.then(function (res) {
		$scope.containers = res;
		var cont = res;
		if($scope.containers.trailerTrailerId){
			FindByTrailerId.query({trailerId:parseInt($scope.containers.trailerTrailerId)}).$promise.then(function (trailer) {
				if(trailer.trailerNo){
					if(!trailer.endTime){
						trailer.endTime=parseInt(new Date().getTime());
					}
					
					TrailerGpsDataByRange.query({trailerId:trailer.trailerNo,startTime:trailer.startTime,endTime:trailer.endTime}).$promise.then(function (gps) {
						
						$scope.trailerNo=trailer.trailerNo;
						$scope.containerNo = cont.containerNo;
						$scope.trailerId = trailer.trailerId;
						
						//declare
						var locs = [];
						var center = {};
						var trailerStartIcon = "assets/images/markericon/truck.png";
						var containerIcon = "assets/images/markericon/container.png";
						var dotsIcon = "assets/images/markericon/dot.png";
						var distanceLocs = 1000;
						var conLoad = false;
						var conunLoad = false;
						
						//sort by time
						gps.sort(function(x, y){
						    return x.gpsdateepoch - y.gpsdateepoch;
						})
						
						
						var lastlat = null;
						var lastlat = null;
						for(i=0;i<gps.length;i++){
							if(i === 0){
								lastlat = gps[i].gpslat;
								lastlon = gps[i].gpslon;
								center = {lat:gps[i].gpslat,lon:gps[i].gpslon}
								locs.push({lat:gps[i].gpslat,lon:gps[i].gpslon,timeStamp:parseInt(gps[i].gpsdateepoch),info:'Trailer Started',icon:trailerStartIcon,idInfo:'Trailer Id',id:trailer.trailerNo});
								if(!conLoad && cont && distanceGeoLoc(cont.sloclat,cont.sloclon,gps[i].gpslat,gps[i].gpslon) <= distanceLocs){
									locs.push({lat:cont.sloclat,lon:cont.sloclon,timeStamp:parseInt(cont.loadedTime),info:'Container Loaded',icon:containerIcon,idInfo:'Container Id',id:cont.containerNo});
									lastlat = gps[i].gpslat;
									lastlon = gps[i].gpslon;
									conLoad = true;
								}
							}else{
								if(!conLoad && cont && distanceGeoLoc(cont.sloclat,cont.sloclon,gps[i].gpslat,gps[i].gpslon) <= distanceLocs){
									locs.push({lat:cont.sloclat,lon:cont.sloclon,timeStamp:parseInt(cont.loadedTime),info:'Container Loaded',icon:containerIcon,idInfo:'Container Id',id:cont.containerNo});
									lastlat = gps[i].gpslat;
									lastlon = gps[i].gpslon;
									conLoad = true;
								}else if (!conunLoad && cont && distanceGeoLoc(cont.eloclat,cont.eloclon,gps[i].gpslat,gps[i].gpslon) <= distanceLocs){
									locs.push({lat:cont.eloclat,lon:cont.eloclon,timeStamp:parseInt(cont.unloadedTime),info:'Container Unloaded',icon:containerIcon,idInfo:'Container Id',id:cont.containerNo});
									lastlat = gps[i].gpslat;
									lastlon = gps[i].gpslon;
									conunLoad = true;
								}else{
									if(distanceGeoLoc(lastlat,lastlon,gps[i].gpslat,gps[i].gpslon) > 300){
										locs.push({lat:gps[i].gpslat,lon:gps[i].gpslon,timeStamp:parseInt(gps[i].gpsdateepoch),info:'Time',icon:dotsIcon,idInfo:'Trailer Id',id:trailer.trailerNo});
										lastlat = gps[i].gpslat;
										lastlon = gps[i].gpslon;
									}
								}
								
							}
						}
						
						if(locs[locs.length-1]){
							if(locs[locs.length-1].idInfo === "Container Id"){
								locs.push({lat:locs[locs.length-1].lat,lon:locs[locs.length-1].lon,timeStamp:parseInt(locs[locs.length-1].timeStamp),info:'Trailer LastSeen',icon:trailerStartIcon,idInfo:'Trailer Id',id:trailer.trailerNo});
							}else{
								locs[locs.length-1].info = "Trailer LastSeen";
								locs[locs.length-1].icon = trailerStartIcon;
							}
						}
						
						//map init
						DrawRoute($scope.containers.trailerTrailerId,'map_canvas',locs,6,center);
						$scope.tables=true;
						
					});
					
					
					
					
				}
				
			});
		}
		
		if(res.workOrderId){
			WorkOrderById.query({id:parseInt(res.workOrderId)}).$promise.then(function (wo) {
				$scope.metsData = wo;
			});
		}
	});*/
}]);













































//for temp db 


function SaveContainers(containers){
	var userEncode =btoa(JSON.stringify(containers));
	localStorage.setItem('db_containers',userEncode);
}

function GetContainers(){
	var loginLDL=atob(localStorage.getItem('db_containers'));
	if(loginLDL !== undefined && loginLDL !== null){
		try{
			return JSON.parse(loginLDL);
    	}
		catch(e){
			return null;
		}
	}else{
		return null;
	}
}

function RemoveContainer(){
	localStorage.removeItem('db_containers');
}















