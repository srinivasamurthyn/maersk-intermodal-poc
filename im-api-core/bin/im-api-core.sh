#! /bin/sh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#title				: im-api-core.sh
#description   		: This script for handling im-api-core-stream.
#author           	: Linkeddots development team
#version         	: 1.0   
#usage           	: ./smacs-service.sh
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

export JAVA_HOME=/opt/linkeddots/jdk1.8.0_121
#export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home

export SMACS_CORE_HOME=/opt/linkeddots/im-api-core
export SMACS_CORE_CONF=$SMACS_CORE_HOME'/conf'
export LOG_CONFIG_FILE=$SMACS_CORE_CONF'/im-api-core-log4j2.xml'
export CONFIG_FILE=$SMACS_CORE_CONF'/im-api-core.properties'
export CHECK_POINT_LOCATION=/opt/linkeddots/checkpointlocation/imapicore

CP=$(echo ../lib/*.jar | tr ' ' ':')
export CLASSPATH=$SMACS_SERVICE_CONF':'$CP
export CURRENT_DATE_TIME=$(date +"%m-%d-%Y-%H-%M-%S")

case "$1" in
start)
	(
		pid=`pgrep -f 'com.linkeddots.im.api.core.WorkOrderLocationProcessor'`
		if [ ! -z $pid ]; then 
			echo "process found with pid "$pid
			echo "use $0 stop"
		else 
	    	echo 'Starting...'
	    		rm -r $CHECK_POINT_LOCATION
			mv ../logs/im-api-core-sysout.log ../logs/im-api-core-sysout-$CURRENT_DATE_TIME.log 
			gzip ../logs/im-api-core-sysout-$CURRENT_DATE_TIME.log
			$JAVA_HOME/bin/java -Dlog4j.configurationFile=$LOG_CONFIG_FILE -DCONFIG_FILE=$CONFIG_FILE com.linkeddots.im.api.core.WorkOrderLocationProcessor > ../logs/im-api-core-sysout.log 2>&1
			echo $!
		fi
	) &
;;

status)
    pid=`pgrep -f 'com.linkeddots.im.api.core.WorkOrderLocationProcessor'`
	if [ ! -z $pid ]; then 
		echo "process found with pid "$pid
	else 
		echo "process not found"
	fi
;;

stop)
	pid=`pgrep -f 'com.linkeddots.im.api.core.WorkOrderLocationProcessor'`
	if [ ! -z $pid ]; then 
		echo "stopping ..."$pid
		pkill -f 'com.linkeddots.im.api.core.WorkOrderLocationProcessor'
	else 
		echo "process not found"
	fi
;;

kill)
	pid=`pgrep -f 'com.linkeddots.im.api.core.WorkOrderLocationProcessor'`
	if [ ! -z $pid ]; then 
		echo "killing ..."$pid
		pkill -9 -f 'com.linkeddots.im.api.core.WorkOrderLocationProcessor'
	else 
		echo "process not found"
	fi
;;

restart)
    $0 stop
    $0 start
;;

*)
    echo "Usage: $0 {status|start|stop}"
    exit 1
esac

tail -f ../logs/im-api-core-sysout.log
