/**
 * 
 */
package com.linkeddots.im.api.core;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.kinesis.KinesisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.kinesis.clientlibrary.lib.worker.InitialPositionInStream;

import scala.Tuple2;

/**
 * @author srinkira
 *
 */
public class WorkOrderLocationProcessor extends AbstractProcessor {
	
	private static final Logger logger = 
		LoggerFactory.getLogger(WorkOrderLocationProcessor.class);
	
	private static long lastConfigLoadingTime = System.currentTimeMillis();
	
	private static void start() throws Exception {
		
		logger.debug("start()");
		
		JavaReceiverInputDStream<byte[]> kinesisStream = KinesisUtils.createStream(
		     jssc, 
		     "intermodal-kinesis-app", 
		     "work-order-location-stream",
		     "https://kinesis.ap-south-1.amazonaws.com",
		     "ap-south-1", 
		     InitialPositionInStream.TRIM_HORIZON, 
		     kinesisCheckpointInterval, 
		     StorageLevel.MEMORY_AND_DISK_2());
		
		JavaDStream<String> words = kinesisStream.flatMap(new FlatMapFunction<byte[], String>() {
		      @Override
		      public Iterator<String> call(byte[] line) {
		        String s = new String(line, StandardCharsets.UTF_8);
		        System.out.println(s);
		        return Arrays.asList(",".split(s)).iterator();
		      }
		    });
		
		// Map each word to a (word, 1) tuple so we can reduce by key to count the words
	    JavaPairDStream<String, Integer> wordCounts = words.mapToPair(
	        new PairFunction<String, String, Integer>() {
	          @Override
	          public Tuple2<String, Integer> call(String s) {
	            return new Tuple2<>(s, 1);
	          }
	        }
	    ).reduceByKey(
	        new Function2<Integer, Integer, Integer>() {
	          @Override
	          public Integer call(Integer i1, Integer i2) {
	            return i1 + i2;
	          }
	        }
	    );

	    // Print the first 10 wordCounts
	    wordCounts.print();
		
		jssc.start();
	    jssc.awaitTermination();

	}
		

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			loadConfigPropeties();
			initialize();
			start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
