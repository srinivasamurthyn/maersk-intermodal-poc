/*
 * Copyright (c) 2018, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.api.core;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.StreamingContext;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.linkeddots.im.api.data.Mets;

/**
 * @author Linkeddots Development Team.
 *
 */
public class AbstractProcessor {
	private static final Logger logger = LoggerFactory.getLogger(AbstractProcessor.class);
	
	protected static SparkConf sparkConf = null;
	protected static SparkSession sparkSession = null;
	
	protected static Properties configProperties = null;
	protected static Properties dbProperties = null;
	
	protected static Map<String, Mets> metsMap = new HashMap<>();
	
	protected static JavaStreamingContext jssc = null;
	// Spark Streaming batch interval
	protected static Duration batchInterval;

    // Kinesis checkpoint interval.
	protected static Duration kinesisCheckpointInterval;
	
	/**
	 * Load Config Properties
	 *
	 * @param configPropertiesFile
	 * @throws Exception
	 */
	protected static void loadConfigPropeties() throws Exception {
		
		logger.debug("loadConfigPropeties() : Start.");
		
		configProperties = new Properties();
		configProperties.load(new FileInputStream(new File(System.getProperty("CONFIG_FILE"))));
		
		dbProperties = getDatabaseProperties(
				configProperties.getProperty("LINKEDM_DB_USERNAME"),
				configProperties.getProperty("LINKEDM_DB_PASSWORD")
			);
		
		logger.debug("loadConfigPropeties() : End.");
	}
	
	/**
	 * 
	 */
	protected static void initialize() throws Exception {
		sparkConf = new SparkConf()
			.setAppName(configProperties.getProperty("APPLICATION_NAME"))
			.setMaster(configProperties.getProperty("MASTER_URL"));
		
		sparkSession =  SparkSession.builder().config(sparkConf).getOrCreate();
		
		batchInterval = new Duration(new Integer(configProperties.getProperty("STREAMING_BATCH_DURATION")));
		kinesisCheckpointInterval = batchInterval;
	
		jssc = new JavaStreamingContext(new StreamingContext(sparkSession.sparkContext(), batchInterval));
			
		loadAllMasterData();
	}
	
	/**
	 * 
	 */
	protected static void loadAllMasterData() throws Exception {
		
		logger.debug("loadAllMasterData() : Start.");
		
		loadAllMetsData();
		
		logger.debug("loadAllMasterData() : End.");
	}
	
	/**
	 * 
	 */
	protected static void loadAllMetsData() throws Exception {
		
		Dataset<Row> metsDS = getData("mets");//.select("VendorId", "VendorName");
		
		if (metsDS != null) {
			metsDS.foreach( 
				metsRow -> {
					Mets mets = new Mets();
					
					String workOrderNumber = metsRow.getAs("work_order_number");
					mets.setWork_order_number(workOrderNumber);
					mets.setWo_create_year_week(metsRow.getAs("wo_create_year_week"));
					mets.setBegin_addressline1(metsRow.getAs("begin_addressline1"));
					mets.setBegin_addressline2(metsRow.getAs("begin_addressline2"));
					mets.setBegin_addressline3(metsRow.getAs("begin_addressline3"));
					mets.setBegin_zip(metsRow.getAs("begin_zip"));
					mets.setBl_number(metsRow.getAs("bl_number"));
					mets.setBl_number_original(metsRow.getAs("bl_number_original"));
					mets.setCarrier_code(metsRow.getAs("carrier_code"));
					mets.setCarrier_name(metsRow.getAs("carrier_name"));
					mets.setClearence_port(metsRow.getAs("clearence_port"));
					mets.setComment(metsRow.getAs("comment"));
					mets.setCommodity_description(metsRow.getAs("commodity_description"));
					mets.setComplete_date(metsRow.getAs("complete_date"));
					mets.setComplete_year_week(metsRow.getAs("complete_year_week"));
					mets.setConcern_code(metsRow.getAs("concern_code"));
					mets.setCorridor_preferred_alternate(metsRow.getAs("corridor_preferred_alternate"));
					mets.setCustomer_name(metsRow.getAs("customer_name"));
					mets.setCustomer_number(metsRow.getAs("customer_number"));
					mets.setCustoms_release(metsRow.getAs("customs_release"));
					
					metsMap.put(workOrderNumber, mets);
					
				}
			); 
		}
	}
	
	/**
	 * @param tableName
	 * @return
	 */
	protected static Dataset<Row> getData(String tableName) throws Exception {
		
		Dataset<Row> tableData = null;
		
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		
		tableData = 
			sparkSession.read().jdbc(
				configProperties.getProperty("LINKEDM_DB_URL"), 
				tableName, 
				dbProperties);
				
		return tableData;
	}

	/**
	 * @param dbUserId
	 * @param dbPassword
	 */
	protected static Properties getDatabaseProperties(String dbUserId, String dbPassword) {
		Properties connectionProperties = new Properties();
		
		connectionProperties.put("user", dbUserId);
		connectionProperties.put("password", dbPassword);
		
		return connectionProperties;
	}
}
