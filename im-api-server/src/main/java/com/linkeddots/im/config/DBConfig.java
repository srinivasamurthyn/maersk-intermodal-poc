/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.config;



import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;

/**
 * @author Linkeddots Development Team.
 *
 */
@Configuration
public class DBConfig {
		
	@Value("${hibernate.dialect}")
	private String dbDialect;

	@Value("${hibernate.connection.driver_class}")
	private String dbDriverClass;

	@Value("${hibernate.connection.url}")
	private String dbUrl;

	@Value("${hibernate.connection.username}")
	private String dbUsername;

	@Value("${hibernate.connection.password}")
	private String dbPassword;
	
	@Value("${hibernate.show_sql:false}")
	private Boolean showSql;
	
	@Value("${hibernate.format_sql:false}")
	private Boolean formatSql;
	
	@Value("${hibernate.hbm2ddl.auto:update}")
	private String autoCreate;
	
	private static final Logger logger = LoggerFactory.getLogger(DBConfig.class);
	
	
	
	
	
	
	@Bean(name="entityManagerFactory")
	public EntityManagerFactory getEntityManagerFactory() {
		
		logger.debug("getEntityManagerFactory() : Start.");
		
		Map<String, Object> properties = new HashMap<>();
				
		properties.put("hibernate.dialect", dbDialect);
		properties.put("hibernate.connection.driver_class", dbDriverClass);
		properties.put("hibernate.connection.url", dbUrl);
		properties.put("hibernate.connection.username", dbUsername);
		properties.put("hibernate.connection.password", dbPassword);
		properties.put("hibernate.show_sql", showSql);
		properties.put("hibernate.format_sql", formatSql);
		properties.put("hibernate.hbm2ddl.auto", autoCreate);
		properties.put("hibernate.connection.pool_size", 100);
		
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("presence-management", properties);
		
		logger.debug("getEntityManagerFactory() : End : entityManagerFactory={}", entityManagerFactory);
	
		return entityManagerFactory;
	}

	@Bean(name="transactionManager")
	public JpaTransactionManager getJpaTransactionManager() {
		logger.debug("getJpaTransactionManager() : Start.");
		
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(getEntityManagerFactory());
		
		logger.debug("getJpaTransactionManager() : End : jpaTransactionManager={}", jpaTransactionManager);
		
		return jpaTransactionManager;
	}

}
