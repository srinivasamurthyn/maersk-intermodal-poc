package com.linkeddots.im.config;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@EnableCaching
public class CacheConfig extends CachingConfigurerSupport {
	
	
	@Value("${im.redis.cache.hostname}")
	private String hostname;
	
	@Value("${im.redis.cache.port}")
	private int port;
	
	@Value("${im.redis.cache.duration.insec}")
	private int cacheDuration;
	
	

	private static final Logger logger = LoggerFactory.getLogger(CacheConfig.class);
	
	  @Bean
	  public JedisConnectionFactory redisConnectionFactory() {
	    JedisConnectionFactory redisConnectionFactory = new JedisConnectionFactory();
	
	    // Defaults
	    redisConnectionFactory.setHostName(hostname);
	    redisConnectionFactory.setPort(port);
	    
	    logger.debug("redisConnectionFactory {} ",redisConnectionFactory);
	    return redisConnectionFactory;
	  }

	  @Bean
	  public RedisTemplate<String, String> redisTemplate(RedisConnectionFactory cf) {
	    RedisTemplate<String, String> redisTemplate = new RedisTemplate<String, String>();
	    redisTemplate.setConnectionFactory(cf);
	    logger.debug("redisTemplate {} ",redisTemplate);
	    return redisTemplate;
	  }

	  @Bean(name ="cacheManager")
	  @Primary
	  public CacheManager cacheManager(RedisTemplate redisTemplate) {
	    RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);
	    // Number of seconds before expiration. Defaults to unlimited (0)
	    cacheManager.setDefaultExpiration(cacheDuration);
	    //cacheManager.setDefaultExpiration(0);
	    logger.debug("cacheManager {} ",cacheManager);
	    return cacheManager;
	  }
  
	  @Bean(name ="centralCacheManager")
	  public CacheManager centralCacheManager(RedisTemplate redisTemplate) {
	    RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);
	    logger.debug("centralCacheManager {} ",cacheManager);
	    return cacheManager;
	  }
	  
	  @Bean
	  public KeyGenerator keyGenerator() {
	    return new KeyGenerator() {
	      @Override
	      public Object generate(Object o, Method method, Object... objects) {
	        // This will generate a unique key of the class name, the method name,
	        // and all method parameters appended.
	        StringBuilder sb = new StringBuilder();
	        sb.append(o.getClass().getName());
	        sb.append(method.getName());
	        for (Object obj : objects) {
	          sb.append(obj.toString());
	        }
	        
	        return sb.toString();
	      }
	    };
	  }
}