/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.config;

import org.apache.camel.component.spring.security.SpringSecurityAccessPolicy;
import org.apache.camel.component.spring.security.SpringSecurityAuthorizationPolicy;
import org.apache.camel.spi.AuthorizationPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.authentication.AuthenticationManager;

/**
 * @author Linkeddots Development Team.
 *
 */
@Configuration
@ImportResource("classpath:config/security.xml")
public class SecurityConfig {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * Create Policy for API Users
	 * 
	 * @param accessDecisionManager
	 * @param manager
	 * @return
	 */
	@Bean(name = "apiUserPolicy")
	public AuthorizationPolicy createLDUserPolicy(AccessDecisionManager accessDecisionManager,
			AuthenticationManager manager) {
		
		logger.debug("createLDUserPolicy() : Start : accessDecisionManager={}, manager={}", accessDecisionManager, manager);
		
		SpringSecurityAuthorizationPolicy policy = new SpringSecurityAuthorizationPolicy();
		policy.setAccessDecisionManager(accessDecisionManager);
		policy.setAuthenticationManager(manager);
		policy.setId("User");
		SpringSecurityAccessPolicy access = new SpringSecurityAccessPolicy("ROLE_USER");
		policy.setSpringSecurityAccessPolicy(access);
		
		logger.debug("createLDUserPolicy() : End : policy={}", policy);
		
		return policy;
	}
	
	/**
	 * Create Policy for Admin Users
	 * 
	 * @param accessDecisionManager
	 * @param manager
	 * @return
	 */
	@Bean(name = "adminUserPolicy")
	public AuthorizationPolicy createAdminUserPolicy(AccessDecisionManager accessDecisionManager,AuthenticationManager manager) {
		SpringSecurityAuthorizationPolicy policy = new SpringSecurityAuthorizationPolicy();
		policy.setAccessDecisionManager(accessDecisionManager);
		policy.setAuthenticationManager(manager);
		policy.setId("AdminUser");
		SpringSecurityAccessPolicy access1 = new SpringSecurityAccessPolicy("ROLE_USER");
		policy.setSpringSecurityAccessPolicy(access1);
		SpringSecurityAccessPolicy access2 = new SpringSecurityAccessPolicy("ROLE_ADMIN");
		policy.setSpringSecurityAccessPolicy(access2);
		return policy;
	}
}
