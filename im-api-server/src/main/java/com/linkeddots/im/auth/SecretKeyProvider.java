package com.linkeddots.im.auth;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import org.springframework.stereotype.Component;

@Component
public class SecretKeyProvider {
    public static byte[] getKey() throws URISyntaxException, IOException {
        String key ="dots";
    	return key.getBytes(StandardCharsets.UTF_8);
    }
}
