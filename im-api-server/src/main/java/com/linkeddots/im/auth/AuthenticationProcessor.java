/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.auth;

import javax.security.auth.Subject;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.linkeddots.im.auth.JwtService;
import com.linkeddots.im.exception.ErrorMessage;;

/**
 * @author Linkeddots Development Team.
 *
 */
@Component("authenticationProcessor")
public class AuthenticationProcessor implements Processor {
	
	private final static Logger logger = LoggerFactory.getLogger(AuthenticationProcessor.class);

 	@Autowired
	private JwtService jwtService;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		logger.debug("process() : Start : exchange={}", exchange);
		
		try {
			String authorization = exchange.getIn().getHeader("Authorization", String.class);
			String token =null;
			if(null != authorization)token = authorization.replaceAll("Bearer ", "");
			logger.debug("process() : token={}", token);
			logger.debug("process() : authorization={}", authorization);
			
			//Step 1
			if(null != token && !StringUtils.isEmpty(token)){
				logger.debug("user", token);
				boolean refreshedToken =false;
				UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(token,token);
				logger.debug("process() : authToken={}", authToken);
				if(null != authToken){
					Subject subject = new Subject();
					subject.getPrincipals().add(authToken);
					logger.debug("Subject: {}", subject);
					exchange.getIn().setHeader(Exchange.AUTHENTICATION, subject);
				}
				
			}else {
				throw new Exception("Token is null or empty");
			}
			
		
		} catch (Exception ex) {
			logger.error("process() : error={}" +  ex);
			
			ex.printStackTrace();
			
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setErrorCode("ERROR_AUTHENTICATION_PROCESSOR");
			errorMessage.setErrorMessage(ex.getMessage());
			
			exchange.getOut().setHeader("STATUS", "FAILURE");
			exchange.getOut().setBody(errorMessage);
		}
	}

}
