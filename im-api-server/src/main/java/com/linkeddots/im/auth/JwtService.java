package com.linkeddots.im.auth;

import static java.time.ZoneOffset.UTC;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.linkeddots.im.dao.auth.UserProfileDao;
import com.linkeddots.im.data.auth.Role;
import com.linkeddots.im.data.auth.UserProfile;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	 private static final String ISSUER = "com.linkeddots.jwt";
	
	 private SecretKeyProvider secretKeyProvider;

	 	@Value("${im.jwt.access.token.duration.min}")
		private int accessTime;
	 	
		@Value("${im.jwt.refresh.token.duration.min}")
		private int refreshTime;
	 	
	 	public JwtService() {
	        this(null);
	    }

	    @Autowired
	    public JwtService(SecretKeyProvider secretKeyProvider) {
	        this.secretKeyProvider = secretKeyProvider;
	    }

	    public String tokenFor(UserProfile user) throws IOException, URISyntaxException {
	        byte[] secretKey = secretKeyProvider.getKey();  //Get the secretKey
	        
	         List<String> rolesPayload = new ArrayList<String>();
	        for(Role role : user.getRoles()) {
	        	rolesPayload.add(role.getRole());
	        }
	         
	        logger.debug("rolesPayload:{}",rolesPayload);
	        
	        Claims claims = Jwts.claims().setSubject(user.getUserName());
	        claims.put("scopes", rolesPayload);
	        Date currentTime = new Date();

	        logger.debug("claims:{}",claims);
	        return Jwts.builder()
	        		.setClaims(claims)
	                .setSubject(user.getUserName())
	                .setIssuedAt(currentTime)
	                .setId(UUID.randomUUID().toString())
	                //.setExpiration(expiration)
	                .setIssuer(ISSUER)
	                .signWith(SignatureAlgorithm.HS512, secretKey)
	                .compact();
	    }
	    
	    
	/**    
	 * Get RefreshToken using existing valid token. This will be used to continue the session.
	 * @param token
	 * @return
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	    public String getRefreshToken(String token) throws IOException, URISyntaxException {
	    	Jws<Claims> oldclaims = getClaims(token);
	    	
	    	  
	        logger.debug("oldclaims:{}",oldclaims);
	    	String username = oldclaims.getBody().getSubject().toString();
	    	
	    	List<String> rolesPayload = (List<String>) oldclaims.getBody().get("scopes");
	    	
	    	  logger.debug("oldclaims scopes:{}",rolesPayload);
	    	  //TODO set extension of session timeout
	        Date expiration = Date.from(LocalDateTime.now().plusMinutes(refreshTime).toInstant(UTC)); //Get the Expiration 2hrs
	        
	         
	        logger.debug("rolesPayload:{}",rolesPayload);
	        
	        Claims claims = Jwts.claims().setSubject(username);
	        claims.put("scopes", rolesPayload);
	        Date currentTime = new Date();
	        
	        logger.debug("claims:{}",claims);
	        return Jwts.builder()
	        		.setClaims(claims)
	                .setSubject(username)
	                .setId(UUID.randomUUID().toString())
	                .setIssuedAt(currentTime)
	                .setExpiration(expiration)
	                .setIssuer(ISSUER)
	                .signWith(SignatureAlgorithm.HS512, SecretKeyProvider.getKey())
	                .compact();
	    }
	    
	    public boolean authenticate(String token) throws URISyntaxException, IOException{
	     	try {
	     		
	     		Jws<Claims> claims = getClaims(token);
				logger.debug("token claims {}", claims);
				String username = claims.getBody().getSubject().toString();
				
				if(StringUtils.isEmpty(username)) return false;
				
				logger.debug("token username {}", username);
				
				return true;
			} catch (ExpiredJwtException e) {
				e.printStackTrace();
			} catch (UnsupportedJwtException e) {
				e.printStackTrace();
			} catch (MalformedJwtException e) {
				e.printStackTrace();
			} catch (SignatureException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			return false;
	    }

		private Jws<Claims> getClaims(String token) throws URISyntaxException, IOException {
			byte[] secretKey = SecretKeyProvider.getKey();
			Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
			return claims;
		}
	    
	    
		/**
		 * Check the expiration of current token within 5 min or not.
		 * @param token
		 * @return
		 * @throws URISyntaxException
		 * @throws IOException
		 */
	    public boolean recentlyExpired(String token) throws URISyntaxException, IOException{
	     	try {
				Jws<Claims> claims = Jwts.parser().setSigningKey(SecretKeyProvider.getKey()).parseClaimsJws(token);
				logger.debug("token recentlyExpired check {}", claims);
				String username = claims.getBody().getSubject().toString();

				//DO not change
				if(StringUtils.isEmpty(username)) return false; //Let it fail with authentication
				
				//Get the expiredDate 
				Date expiredDate = claims.getBody().getExpiration();
				//Check within x min. Session idle time
				Date withinLimit = Date.from(LocalDateTime.now().plusMinutes(5).toInstant(UTC)); 
				
				
				logger.debug("withinLimit.getTime(){}, expiredDate.getTime(){}, withinLimit.getTime(){},  expiredDate.getTime() {}",
						withinLimit.getTime(), expiredDate.getTime(), withinLimit.getTime(),  expiredDate.getTime());
				
					
				if( (withinLimit.getTime() - expiredDate.getTime()) >0   
						&& (withinLimit.getTime() - expiredDate.getTime()) < 300000 ){
					//Expiring in 5 min.

					logger.debug("token expiring in 5 min for user: {}", username);
					return true;
				}
				
				return false; 
			} catch (ExpiredJwtException e) {
				e.printStackTrace();
			} catch (UnsupportedJwtException e) {
				e.printStackTrace();
			} catch (MalformedJwtException e) {
				e.printStackTrace();
			} catch (SignatureException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			return false;
	    }
	    
	    public UserContext getUserContext(String token) throws Exception {
	    	  byte[] secretKey = SecretKeyProvider.getKey();
		        Jws<Claims> jwsClaims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
		        logger.debug("token claims {}", jwsClaims);
		        String subject = jwsClaims.getBody().getSubject();
		        List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
		        List<GrantedAuthority> authorities = scopes.stream()
		                .map(authority -> new SimpleGrantedAuthority(authority))
		                .collect(Collectors.toList());
		        logger.debug("authorities {}", authorities);
		        UserContext context = UserContext.create(subject, authorities);
		        return context;
		        
	    }
	  
}