package com.linkeddots.im.auth;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import com.linkeddots.im.data.auth.Role;
import com.linkeddots.im.data.auth.UserProfile;



@Transactional
public class CustomUserDetailsService implements UserDetailsService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JwtService jwtService;
	


  	
	@Override
	public UserDetails loadUserByUsername(String token) throws UsernameNotFoundException {
		try {
			
			logger.debug("jwtToken:"  + token);
			if(true){
				UserContext userContext = jwtService.getUserContext(token);
				
				
				if(null!= userContext){
					logger.debug("user context.user:"  + userContext.getUsername());
					logger.debug("user context.authorities:"  + userContext.getAuthorities());
				
					//Authenticated Successfully.
					UserDetails userDetails= new org.springframework.security.core.userdetails.User(userContext.getUsername(), token, userContext.getAuthorities());
					logger.debug("userDetails:{}", userDetails);
			        return userDetails;
				}
				
			}	
			
        }
        catch (Exception e){
            throw new UsernameNotFoundException("User not found");
        }
		
		return null;
    }

    private Set<GrantedAuthority> getAuthorities(UserProfile user){
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        for(Role role : user.getRoles()) {
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRole());
            authorities.add(grantedAuthority);
        }
        logger.debug("user authorities are " + authorities.toString());
        return authorities;
    }


}
