/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im;

import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.apache.camel.spring.javaconfig.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.AbstractApplicationContext;

import com.linkeddots.im.config.CacheConfig;
import com.linkeddots.im.config.CamelConfig;
import com.linkeddots.im.config.DBConfig;
import com.linkeddots.im.config.JPAConfig;
import com.linkeddots.im.config.SecurityConfig;

/**
 * @author Linkeddots Development Team.
 *
 */
@Configuration
@ComponentScan
public class Server extends CamelConfiguration {

	private final static Logger log = LoggerFactory.getLogger(Server.class);

	private static Main main;

	public static Server runner;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static final void main(String[] args) throws Exception {

		log.debug("main() : Start SMACS Service Server.");
		runner = new Server();
		runner.boot();
	}

	/**
	 * 
	 */
	private void boot() {
		try {
			AbstractApplicationContext appContext = 
				new AnnotationConfigApplicationContext(
					DBConfig.class,
					JPAConfig.class, 
					CamelConfig.class,
					SecurityConfig.class,
					CacheConfig.class //Enable or disable caching
					);

			main = new Main();
			main.setApplicationContext(appContext);
			// run until you terminate the JVM
			log.debug("Started Camel API.\n");
			main.run();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 
	 */
	public static void shutdown() {

		if (main != null && main.isStarted() && main.isStoppingOrStopped() == false) {
			try {

				Thread stop = null;

				// stop this route using a thread that will stop
				// this route gracefully while we are still running
				if (stop == null) {
					stop = new Thread() {
						@Override
						public void run() {
							try {
								main.shutdown();
							} catch (Exception e) {
								// ignore
							}
						}
					};
				}
				// start the thread that stops this route
				stop.start();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}

}

