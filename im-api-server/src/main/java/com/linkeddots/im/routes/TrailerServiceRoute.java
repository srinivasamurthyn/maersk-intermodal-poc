/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.routes;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Linkeddots Development Team.
 *
 */
@Component
@Slf4j
public class TrailerServiceRoute extends AbstractRoute {

	/**
	 * @see org.apache.camel.builder.RouteBuilder#configure()
	 */
	@Override
	public void configure() throws Exception {

		log.debug("configure() : Started.");

		super.configure();
		
		//Create a Trailer
		rest("/trailers").post().description("Create a Trailer.")
			.route()
				.routeId("CreateTrailer")
				.to("authenticationProcessor")
				.policy("apiUserPolicy")
				.setHeader("method", simple("create"))
				.to("trailerProcessor")
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201)).endRest();

		//Get all trailers
		rest("/trailers").get().description("Get all trailers.")
			.route()
				.routeId("GetTrailers")
				.description("Get all trailers.")
				.to("authenticationProcessor")
				.policy("apiUserPolicy")
				.setHeader("method", simple("findAll"))
				.setHeader("Cache-Control",constant("private, max-age=0,no-store"))
				.to("trailerProcessor")
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200)).endRest();
		
		//Returns Trailer details for a specific trailer id.
		rest("/trailers/{TrailerId}").get().description("Returns Trailer details for a specific trailer id.")
			.route()
				.routeId("GetTrailerById")
				.to("authenticationProcessor")
				.policy("apiUserPolicy")
				.setHeader("Id", simple("${header['TrailerId']}"))
				.setHeader("method", simple("findOne"))
				.setHeader("Cache-Control",constant("private, max-age=0,no-store"))
				.to("trailerProcessor")
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200)).endRest();
		
		//Update the trailer details.
		rest("/trailers/{TrailerId}").put().description("Update the trailer details.")
			.route()
				.routeId("UpdateTrailer")
				.to("authenticationProcessor")
				.policy("apiUserPolicy")
				.setHeader("Id", simple("${header['TrailerId']}"))
				.setHeader("method", simple("update"))
				.to("trailerProcessor")
				.setBody(simple(""))
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(204)).endRest();
		
		//Delete a specified trailer
		rest("/trailers/{TrailerId}").delete().description("Delete a specified trailer.")
			.route()
				.routeId("DeleteTrailer")
				.to("authenticationProcessor")
				.policy("apiUserPolicy")
				.setHeader("Id", simple("${header['TrailerId']}"))
				.setHeader("method", simple("delete"))
				.to("trailerProcessor")
				.setBody(simple(""))
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(204)).endRest();
		
		//Count all trailers
		rest("/trailers/count/all").get().description("Returns all Trailer count ")
			.route()
				.routeId("CountAllTrailer")
				.to("authenticationProcessor")
				.policy("apiUserPolicy")
				.setHeader("method", simple("countAll"))
				.setHeader("Cache-Control",constant("private, max-age=0,no-store"))
				.to("trailerProcessor")
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200)).endRest();
	
	}

}
