/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.routes.auth;

import org.apache.camel.Exchange;
import org.springframework.stereotype.Component;

import com.linkeddots.im.routes.AbstractRoute;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Linkeddots Development Team.
 *
 */
@Component
@Slf4j
public class LoginServiceRoute extends AbstractRoute {

	/**
	 * @see org.apache.camel.builder.RouteBuilder#configure()
	 */
	@Override
	public void configure() throws Exception {

		log.debug("configure() : Started.");

		super.configure();
	
		rest("/login").post().description("Returns User details for a specific user id.")
			.route()
				.routeId("AuthenticateUser")
					.setHeader("method", simple("login"))
					.setHeader("Cache-Control",constant("private, max-age=0,no-store"))
					.to("loginProcessor")
					.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201)).endRest();

	}

}
