/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.routes;

import java.nio.file.AccessDeniedException;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.builder.PredicateBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

import com.linkeddots.im.exception.ResourceNotFoundException;

/**
 * @author Linkeddots Development Team.
 *
 */
@Component
public class AbstractRoute extends RouteBuilder {

	@Value("${im.service.port:9090}")
	private Integer port;

	@Value("${im.service.host:0.0.0.0}")
	private String hostname;

	@Override
	public void configure() throws Exception {

		String scheme = "http";
		
		restConfiguration().component("jetty").host(hostname).port(port).bindingMode(RestBindingMode.json)
				.contextPath("/im/api/").scheme(scheme).enableCORS(true).dataFormatProperty("prettyPrint", "true")
				.dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES");

		// Authentication failure
		onException(AccessDeniedException.class).logStackTrace(true).handled(true)
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(401)).setBody(exceptionMessage()).end();

		onException(BadCredentialsException.class).logStackTrace(true).handled(true)
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(401)).setBody(exceptionMessage()).end();

		// Resource Not Found Exception
		onException(ResourceNotFoundException.class).logStackTrace(true).handled(true)
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(404)).end();

		// Condition to send Java Stacktrace in response
		Predicate ifStackTraceEnabled = PredicateBuilder
				.toPredicate(simple("${properties:logging.trace.exception.enabled} == 'true'"));

		// Unhandled exception
		onException(RuntimeException.class).logStackTrace(true).handled(true)
				.log("${body}")
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
				.setHeader(Exchange.CONTENT_TYPE, constant("text/plain")).choice().when(ifStackTraceEnabled)
				.setBody(simple("${exception.stacktrace}")).otherwise().setBody(exceptionMessage()).end();
		

	}

	
}
