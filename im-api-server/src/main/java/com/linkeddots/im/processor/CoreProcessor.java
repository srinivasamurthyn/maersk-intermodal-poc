/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.processor;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * @author Linkeddots Development Team.
 *
 */
@Component("coreProcessor")
public abstract class CoreProcessor implements Processor {
	
	private final static Logger logger = LoggerFactory.getLogger(CoreProcessor.class);
	
	protected static Properties configProperties = null;
	
	public void process(Exchange exchange) throws Exception {
		
		logger.debug("Core process() : Start.");
		doProcess(exchange);
		logger.debug("Core process() : End.");
	}
	
	public boolean centralCacheProcess (Exchange exchange,String cc_IdS) throws Exception {
		
		configProperties = new Properties();
		configProperties.load(new FileInputStream(new File(System.getProperty("CONFIG_FILE"))));
		boolean centralCacheEnable = Boolean.parseBoolean(configProperties.getProperty("im.centralcache.enabled"));
		boolean isNoChange = false;
		
		if(centralCacheEnable) {
			String cc_type = (String) exchange.getIn().getHeader("CC-Type");
			String cc_Id = (String) exchange.getIn().getHeader("CC-Id");
			String cc_status = "CHANGE";
			
			exchange.getOut().setHeader( "CC-Id", cc_IdS);
			exchange.getOut().setHeader( "CC-Status",cc_status);
			if(cc_IdS.equals(cc_Id) && !cc_type.equals("NEWCALL")) {
				cc_status = "NOCHANGE";
				exchange.getOut().setHeader( "CC-Status",cc_status);
				exchange.getOut().setBody(cc_IdS);
				exchange.getOut().setHeader( "STATUS", "SUCCESS" );
				isNoChange = true;
			}
		}
		
		return isNoChange;
	
	}
	
	public abstract void doProcess(Exchange exchange) throws Exception;
	
}
