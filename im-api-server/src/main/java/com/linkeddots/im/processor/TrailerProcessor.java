/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.processor;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linkeddots.im.dao.TrailerDao;
import com.linkeddots.im.data.Trailer;
import com.linkeddots.im.exception.ErrorMessage;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Linkeddots Development Team.
 *
 */
@Component("trailerProcessor")
@Slf4j
public class TrailerProcessor extends CoreProcessor {

	@Autowired
	private TrailerDao trailerDao;
	
	@Autowired
	private ObjectMapper mapper;
	
	
	public void doProcess(Exchange exchange) throws Exception {
		
		log.debug("process() : Start.");
		
		String method =  (String) exchange.getIn().getHeader("method");
		log.debug("process() : method={}", method);
		

		/**
		 *  * * * * * * * * * * CentralCache Start * * * * * * * * * * * * 
		 */
		
		
		String cc_IdS = trailerDao.getCentralCacheId();
		boolean isNoChanage = centralCacheProcess(exchange, cc_IdS);
		if(isNoChanage) {
			return;
		}
		
		
		/**
		 *  * * * * * * * * * * CentralCache End * * * * * * * * * * * * 
		 */
		
		
		
		HashMap<String, Object> outputValues = new HashMap<>();
		
		try {
			if ("findAll".equals(method)) {
				exchange.getOut().setBody( trailerDao.findAll() );
				
			} else if ("findOne".equals(method)) {
				Long id = exchange.getIn().getHeader("Id", Long.class);
				Trailer trailer = trailerDao.findOne(id);
				
				if (trailer != null) {
					exchange.getOut().setBody( trailer );
				} else {
					exchange.getOut().setBody( outputValues );
				}
			} else if ("create".equals(method)) {				
				@SuppressWarnings("unchecked")
				Map<String, Object> inputValues = (HashMap<String, Object>) exchange.getIn().getBody();
				
				Trailer trailer = mapper.convertValue(inputValues, Trailer.class);
				exchange.getOut().setBody( trailerDao.create(trailer) );
				
			} else if ("update".equals(method)) {
				@SuppressWarnings("unchecked")
				Map<String, Object> inputValues = (HashMap<String, Object>) exchange.getIn().getBody();
				
				Long id =  exchange.getIn().getHeader("Id", Long.class);
				Trailer trailer = mapper.convertValue(inputValues, Trailer.class);
				trailerDao.update(id, trailer);
				
			} else if ("delete".equals(method)) {
				Long id = exchange.getIn().getHeader("Id", Long.class);
				trailerDao.delete(id);
			} else if ("countAll".equals(method)) {
				exchange.getOut().setBody( trailerDao.count() );
			}
			
			exchange.getOut().setHeader( "STATUS", "SUCCESS" );
		} catch (Exception ex) {
			
			log.error("process() : error={}" +  ex);
			
			ex.printStackTrace();
			
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setErrorCode("ERROR_TAG_PROCESSOR");
			errorMessage.setErrorMessage(ex.getMessage());
			
			exchange.getOut().setHeader("STATUS", "FAILURE");
			exchange.getOut().setBody(errorMessage);
		}
		
		log.debug("process() : End.");
	}


	
}
