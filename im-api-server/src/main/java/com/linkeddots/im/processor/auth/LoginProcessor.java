/*
 * Copyright (c) 2017, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.processor.auth;

import java.util.HashMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import com.linkeddots.im.dao.auth.UserProfileDao;
import com.linkeddots.im.data.auth.UserProfile;
import com.linkeddots.im.exception.ChangePasswordException;
import com.linkeddots.im.exception.InvalidUserStatusException;
import com.linkeddots.im.exception.LoginFailedException;
import com.linkeddots.im.auth.JwtService;
import com.linkeddots.im.exception.ErrorMessage;


import lombok.extern.slf4j.Slf4j;

/**
 * @author Linkeddots Development Team.
 * Important Class. Authenticate User first by UserId and Password then creates the JwtToken.
 */
@Component("loginProcessor")
@Slf4j
public class LoginProcessor implements Processor {
	
	@Autowired
	private UserProfileDao userProfileDao;
	
	
	@Autowired
	private JwtService jwtService;
	
	private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	public void process(Exchange exchange) throws Exception {
		
		log.debug("process() : Start.");
		String method =  (String) exchange.getIn().getHeader("method");
		log.debug("process() : method={}", method);
		
		HashMap<String, Object> outputValues = new HashMap<>();
		
		try {
			if ("login".equals(method)) {
				String authorization = exchange.getIn().getHeader("Authorization", String.class);
				log.debug("process() : authorization={}", authorization);
				String pieces[] = authorization.split(" ");
				String userpass = new String(Base64.decodeBase64(pieces[1]));
				log.debug("process() : userpass={}", userpass);
				String[] tokens = userpass.split(":");
				log.debug("process() : tokens[0]={}, tokens[1]={}", tokens[0], tokens[1]);
				
				//Check of token validity 
				String username = tokens[0];
				String credentials = tokens[1];
				
				if(StringUtils.isEmpty(username) || StringUtils.isEmpty(credentials)){
					throw new Exception("Credentials  are null or empty!.");
				}
				
				
				UserProfile user = userProfileDao.findByUserName(username);
				//Step 1 - user is there
				if (user != null) { 
					log.debug("login step1:"+user.getUserName());
					//Step 2 -validate profile
					if(!user.isUserEnable()){
						throw new InvalidUserStatusException("User is not ACTIVE. Please check with your administrator");
					}
					
					exchange.getOut().setHeader( "User-Id", user.getId());
					exchange.getOut().setHeader( "User-UserName", user.getUserName());
					exchange.getOut().setHeader( "User-UserType",user.getUserType());
					exchange.getOut().setHeader( "User-LandingRole", user.getLandingRole());
					
					//Authenticate
					boolean authenticated = passwordEncoder.matches(credentials, user.getPassword());
					log.debug("login step2 authenticated:"+authenticated);
					
					if(authenticated){
						String token =  "Bearer "+jwtService.tokenFor(user);
						log.debug("Login step 3 Jwt token:"+token);
						exchange.getOut().setHeader( "Authorization", token );
						exchange.getOut().setHeader( "STATUS", "SUCCESS" );
					} else {
						throw new LoginFailedException("User login failed.");
					}
				} else {
					exchange.getOut().setBody( outputValues );
					throw new Exception("Invalid Username");
				}
				exchange.getOut().setBody( outputValues );
			}
			
		} catch (InvalidUserStatusException ex) {
			
			log.error("process() : error={}" +  ex);
			
			ex.printStackTrace();
			
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setErrorCode("ERROR_INVALID_USERSTATUS");
			errorMessage.setErrorMessage(ex.getMessage());
			
			exchange.getOut().setHeader("STATUS", "FAILURE");
			exchange.getOut().setBody(errorMessage);
			throw ex;
		}catch (ChangePasswordException ex) {
			
			log.error("process() : error={}" +  ex);
			
			ex.printStackTrace();
			
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setErrorCode("ERROR_CHANGE_PASSWORD");
			errorMessage.setErrorMessage(ex.getMessage());
			
			exchange.getOut().setHeader("STATUS", "FAILURE");
			exchange.getOut().setBody(errorMessage);
			throw ex;
		}catch (LoginFailedException ex) {
			
			log.error("process() : error={}" +  ex);
			
			ex.printStackTrace();
			
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setErrorCode("ERROR_LOGIN_FAILED");
			errorMessage.setErrorMessage(ex.getMessage());
			
			exchange.getOut().setHeader("STATUS", "FAILURE");
			exchange.getOut().setBody(errorMessage);
			throw ex;
		}catch (Exception ex) {
			
			log.error("process() : error={}" +  ex);
			
			ex.printStackTrace();
			
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setErrorCode("ERROR_LOGIN_PROCESSOR");
			errorMessage.setErrorMessage(ex.getMessage());
			
			exchange.getOut().setHeader("STATUS", "FAILURE");
			exchange.getOut().setBody(errorMessage);
			throw ex;
		}
		
		log.debug("process() : End.");
	}

 
	 
	
}
