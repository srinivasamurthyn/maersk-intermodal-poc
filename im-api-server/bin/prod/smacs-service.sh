#! /bin/sh

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#title		: smacs-service.sh
#description   : This script manages smacs-service.
#author           : Linkeddots development team
#version         : 1.0   
#usage           : bash smacs-service.sh
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

export JAVA_HOME=/opt/linkeddots/jdk1.8.0_121/
#export JAVA_HOME=/opt/linkeddots/jdk1.8.0_111
#export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_45.jdk/Contents/Home
#Local changes

SMACS_SERVICE_VERSION='1.0.0'
PWD=`pwd`
SCRIPT_HOME=$(dirname "$0")
export SMACS_SERVICE_HOME=/opt/linkeddots/smacs-service
export SMACS_SERVICE_CONF=$SMACS_SERVICE_HOME'/conf'
export LOG_HOME=$SMACS_SERVICE_HOME'/log'
export LOG_CONFIG_FILE=$SMACS_SERVICE_CONF'/smacs-service-log4j2.xml'
export CONFIG_FILE=$SMACS_SERVICE_CONF'/smacs-service.properties'

# OS specific support.
cygwin=false
case "`uname`" in
   CYGWIN*) cygwin=true ;;
esac

if $cygwin ; then
    echo 'cygwin shell detected'
    LOG_HOME=`cygpath -w "$LOG_HOME"`
    LOG_CONFIG_FILE=`cygpath -w "$LOG_CONFIG_FILE"`
fi

CP=$(echo ../lib/*.jar | tr ' ' ':')
export CLASSPATH=$SMACS_SERVICE_CONF':'$CP

echo $CONFIG_FILE

case "$1" in
start)
	(
		pid=`pgrep -f 'com.linkeddots.smacs.service.Server'`
		if [ ! -z $pid ]; then 
			echo "process found with pid "$pid
			echo "use $0 stop"
		else 
	    	echo 'Starting...'
	    	$JAVA_HOME/bin/java -Dlog4j.configurationFile=$LOG_CONFIG_FILE -DCONFIG_FILE=$CONFIG_FILE com.linkeddots.smacs.service.Server > /opt/linkeddots/smacs-service/logs/smacs-service-sysout.log 2>&1
			echo $!
		fi
	) &
;;

status)
    pid=`pgrep -f 'com.linkeddots.smacs.service.Server'`
	if [ ! -z $pid ]; then 
		echo "process found with pid "$pid
	else 
		echo "com.linkeddots.smacs.service.Server process not found"
	fi
;;

stop)
	pid=`pgrep -f 'com.linkeddots.smacs.service.Server'`
	if [ ! -z $pid ]; then 
		echo "stopping ..."$pid
		pkill -f 'com.linkeddots.smacs.service.Server'
	else 
		echo "com.linkeddots.smacs.service.Server process not found"
	fi
;;

kill)
	pid=`pgrep -f 'com.linkeddots.smacs.service.Server'`
	if [ ! -z $pid ]; then 
		echo "killing ..."$pid
		pkill -9 -f 'com.linkeddots.smacs.service.Server'
	else 
		echo "com.linkeddots.smacs.service.Server process not found"
	fi
;;

restart)
    $0 stop
    $0 start
;;

*)
    echo "Usage: $0 {status|start|stop}"
    exit 1
esac
