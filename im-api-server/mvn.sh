cd ../im-api-data/
mvn clean install
cp -r target/im-api-data-1.0.0.jar /opt/linkeddots/im-api-server/lib/
cd ../im-api-server/
mvn clean install
cp -r target/im-api-server-1.0.0.jar /opt/linkeddots/im-api-server/lib/
