package com.linkeddots.im.api.server;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import com.linkeddots.im.api.data.GPSMovement;
import com.linkeddots.im.api.data.ResponseDetails;
import com.linkeddots.im.api.data.WorkOrderLocation;

@SpringBootApplication
public class ApplicationClient {

	private static final Logger log = LoggerFactory.getLogger(ApplicationClient.class);
	
	private static String url = null;
	private static int maxNumberOfThreads = 0;
	private static long targetNumberOfCalls = 0L;
	private static int delayBetweenConcurrentRequestsInSeconds = 1;
	
	private static SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S");

	public static void main(String args[]) {
		if(args.length>0){
			url = args[0]; //https://fa520xnlcc.execute-api.ap-south-1.amazonaws.com/test/streams/work-order-location-stream/records
			maxNumberOfThreads = new Integer(args[1]).intValue();
			targetNumberOfCalls = new Long(args[2]).longValue();
			delayBetweenConcurrentRequestsInSeconds = new Integer(args[3]).intValue();
			SpringApplication.run(ApplicationClient.class);
		}else {
			System.out.println();
			System.out.println();
			System.out.println("ERROR missing params: url,maxNumberOfThreads,targetNumberOfCalls,delayBetweenConcurrentRequestsInSeconds");
			System.out.println();
			System.out.println();
		}
		
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		
		ExecutorService executorService = Executors.newFixedThreadPool(maxNumberOfThreads);
		
		Callable<String> callableTask = () -> {
		    TimeUnit.SECONDS.sleep(delayBetweenConcurrentRequestsInSeconds);
		    
		    GPSMovement gpsMovement = new GPSMovement();
		    
		    gpsMovement.setWorder(55978761);
		    gpsMovement.setGpslat(38.9055883);
		    gpsMovement.setGpslong(-94.6707917);
		    gpsMovement.setGpsdate(dateformat.format(new Date()));
		    
		    WorkOrderLocation workOrderLocation = new WorkOrderLocation();
		    workOrderLocation.getGpsMovement().add(gpsMovement);
		    workOrderLocation.setVendor("skyangel");
		    
		    HttpHeaders headers = new HttpHeaders();
		    //headers.set("x-api-key", "fouiBtMR8v5VORn9nuoNz2l6K4LtdI1y8nY3Sb5z");
		    headers.set("Content-Type", "application/json");
		    //headers.set("X-Amz-Date", "20180402T040038Z");
		    //headers.set("Authorization", "AWS4-HMAC-SHA256 Credential=undefined/20180402/us-east-1/execute-api/aws4_request, SignedHeaders=content-length;content-type;host;x-amz-date;x-api-key, Signature=ded417f7e9c109c013ec0da28c8e514c49a6928083e07d59c227fefddc586ec8");

		    HttpEntity<WorkOrderLocation> request = new HttpEntity<>(workOrderLocation, headers);
		    
		    ResponseDetails responseDetails = restTemplate.postForObject(url, request, ResponseDetails.class);
		    
			log.info(responseDetails.toString());
			
		    return responseDetails.toString();
		};
		 
		List<Callable<String>> callableTasks = new ArrayList<>();
		
		for (int i = 0; i < targetNumberOfCalls; i ++) {
			callableTasks.add(callableTask);
		}
		
		List<Future<String>> futures;
		try {
			futures = executorService.invokeAll(callableTasks);
			
			futures.forEach(x -> {
				String result;
				try {
					result = x.get(200, TimeUnit.MILLISECONDS);
					log.info("result=" + result);
				} catch (InterruptedException | ExecutionException | TimeoutException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		executorService.shutdown();
		try {
		    if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
		        executorService.shutdownNow();
		    } 
		} catch (InterruptedException e) {
		    executorService.shutdownNow();
		}
		
		return args -> {

		};
	}
}