exports.isArray = function(array){
   return Array.isArray(array);
};

exports.isObjectNotEmpty = function(obj){
	return (obj !== undefined && obj !== null)?true:false;
};

exports.isObjectEmpty =function (obj){
	return (null===obj || undefined === obj)?true:false;
};

exports.getEpochTime = function(date){
    var date = new Date(date);
    if(typeof(date) === "date"){
       return date.getTime();
    }else{
        return new Date().getTime();
    }
};

exports.isStringNotEmpty = function(obj){
	return (this.isObjectNotEmpty(obj) && obj !== '')?true:false;
};

exports.isStringEmpty =function (obj){
	return (this.isObjectEmpty(obj) || obj === '')?true:false;
};

exports.getStringDate = function (){
    
    var today= new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
	     dd='0'+dd;
	} 
	if(mm<10){
	     mm='0'+mm;
	} 
	return dd+'-'+mm+'-'+yyyy;
	
}