var AWS = require('aws-sdk');
AWS.config.region = 'ap-south-1';
var lambda = new AWS.Lambda();
//var s3bucket = new AWS.S3({region:'ap-south-1'});
const EventEmitter = require('events');
EventEmitter.defaultMaxListeners = Infinity;

//user package
const rdsservices = require ('./services/rdsservices.js');
const utils = require ('./utils/utils.js');

//env config
const lambdaFName = require('./config/config.json').GeoCodelambdaFName;


exports.handler = (event, context, callback) => {
  console.log('Event : ', JSON.stringify(event));
  if(!utils.isArray(event.records)){
    rdsservices.insertGpsSingleData (event);
    this.invokeLambdaFun(event,context);
    //this.saveToS3bucket(event);
    callback(null,{});
  }else{
    rdsservices.insertGpsDataBulk (event.records);
    //this.invokeLambdaFun(event,context);
    //this.saveToS3bucket(event);
    callback(null,{});
  }
};





exports.invokeLambdaFun = function (event,context){
  var params = {
      FunctionName: lambdaFName,
      InvocationType: 'Event',
      Payload: JSON.stringify(event)
    };
    
    lambda.invoke(params, function(err, data) {
      if (err) {
        context.fail(err);
      } else {
        context.succeed('lambdaFName said '+ data.Payload);
      }
    });
}

/*exports.saveToS3bucket = function (data){
  s3bucket.createBucket(function () {
      var params = {
        Bucket: 'im-gps-s3-backup',
        Key: "backup"+utils.getStringDate()+".txt",
        Body: JSON.stringify(data)
      };
      s3bucket.putObject(params, function (err, data) {
        if (err) {
          console.log('error in callback');
          console.log(err);
        }
        console.log('success');
        console.log(data);
      });
  });
}
*/