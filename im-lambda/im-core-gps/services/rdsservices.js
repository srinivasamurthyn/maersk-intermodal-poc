const sql = require('mssql');
const config = require('../config/config.json').mssqlConfig;
const utils = require ('../utils/utils.js');


//sql request for insert
exports.insertGpsSingleData = function(event) {
  
  
  var vendor = utils.isStringNotEmpty(event.vendor)?event.vendor:'';
  var trailerId = utils.isStringNotEmpty(event.id)?event.id:'';
  var worder = utils.isStringNotEmpty(event.worder)?event.worder:'';
  var containerno = utils.isStringNotEmpty(event.containerno)?event.containerno:'';
  var bookingno = utils.isStringNotEmpty(event.bookingno)?event.bookingno:'';
  var gpslat = utils.isStringNotEmpty(event.gpslat)?event.gpslat:'';
  var gpslon = utils.isStringNotEmpty(event.gpslong)?event.gpslong:'';
  var gpsdate = utils.isStringNotEmpty(event.gpsdate)?event.gpsdate:'';
  var gpsdateepoch = utils.getEpochTime(event.gpsdate);
  var stringEvent = utils.isStringNotEmpty(event.event)?event.event:'';
  var eventstatus = utils.isStringNotEmpty(event.eventstatus)?event.eventstatus:'';
  var apikeyid = utils.isStringNotEmpty(event.apikeyid)?event.apikeyid:'';
  var sourceip = utils.isStringNotEmpty(event.sourceip)?event.sourceip:'';
  var reqtime = utils.isStringNotEmpty(event.reqtime)?event.reqtime:'';
  
  //connect mssql
  sql.connect(config).then(function() {
    console.log("SQL Connected ...");
    console.log("INSERT INTO gpsdata (vendor, worder, containerno, trailerId, bookingno, gpslat, gpslon, gpsdate, event, eventstatus, apikeyid, sourceip, reqtime, gpsdateepoch) VALUES ('"+vendor+"','"+worder+"','"+containerno+"','"+trailerId+"','"+bookingno+"',"+gpslat+","+gpslon+",'"+gpsdate+"','"+stringEvent+"','"+eventstatus+"','"+apikeyid+"','"+sourceip+"','"+reqtime+"',"+gpsdateepoch+")");
    var request = new sql.Request();
    request.query("INSERT INTO gpsdata (vendor, worder, containerno, trailerId, bookingno, gpslat, gpslon, gpsdate, event, eventstatus, apikeyid, sourceip, reqtime, gpsdateepoch) VALUES ('"+vendor+"','"+worder+"','"+containerno+"','"+trailerId+"','"+bookingno+"',"+gpslat+","+gpslon+",'"+gpsdate+"','"+stringEvent+"','"+eventstatus+"','"+apikeyid+"','"+sourceip+"','"+reqtime+"',"+gpsdateepoch+")").then(function(recordset) {
      sql.close();
      console.log('Completed...');
    }).catch(function(err) {
      sql.close();
      console.log(err);
    });
    
    
  //error handler  
  }).catch(function(err) {
    if (err) {
      console.log("SQL Error :"+err);
    }
  });
  
  //erorr handler
  sql.on('error', (err) => {
    console.log("SQL Error :"+err);
  });
  
  
  
}




//sql request for multiple insert
exports.insertGpsDataBulk = function(gpsdata) {
  
  
  //connect mssql
  sql.connect(config).then(function() {
    console.log("SQL Connected ...");
    
   
    
    for(var i=0;i<gpsdata.length;i++){
      
      (function(i,gpsdata){
        
         console.log('loop : '+gpsdata);
        
        var vendor = utils.isStringNotEmpty(gpsdata[i].vendor)?gpsdata[i].vendor:'';
        var trailerId = utils.isStringNotEmpty(gpsdata[i].id)?gpsdata[i].id:'';
        var worder = utils.isStringNotEmpty(gpsdata[i].worder)?gpsdata[i].worder:'';
        var containerno = utils.isStringNotEmpty(gpsdata[i].containerno)?gpsdata[i].containerno:'';
        var bookingno = utils.isStringNotEmpty(gpsdata[i].bookingno)?gpsdata[i].bookingno:'';
        var gpslat = utils.isStringNotEmpty(gpsdata[i].gpslat)?gpsdata[i].gpslat:'';
        var gpslon = utils.isStringNotEmpty(gpsdata[i].gpslong)?gpsdata[i].gpslong:'';
        var gpsdate = utils.isStringNotEmpty(gpsdata[i].gpsdate)?gpsdata[i].gpsdate:'';
        var gpsdateepoch = utils.getEpochTime(gpsdata[i].gpsdate);
        var stringEvent = utils.isStringNotEmpty(gpsdata[i].event)?gpsdata[i].event:'';
        var eventstatus = utils.isStringNotEmpty(gpsdata[i].eventstatus)?gpsdata[i].eventstatus:'';
        var apikeyid = utils.isStringNotEmpty(gpsdata[i].apikeyid)?gpsdata[i].apikeyid:'';
        var sourceip = utils.isStringNotEmpty(gpsdata[i].sourceip)?gpsdata[i].sourceip:'';
        var reqtime = utils.isStringNotEmpty(gpsdata[i].reqtime)?gpsdata[i].reqtime:'';
        
        
        console.log("INSERT INTO gpsdata (vendor, worder, containerno, trailerId, bookingno, gpslat, gpslon, gpsdate, event, eventstatus, apikeyid, sourceip, reqtime, gpsdateepoch) VALUES ('"+vendor+"','"+worder+"','"+containerno+"','"+trailerId+"','"+bookingno+"',"+gpslat+","+gpslon+",'"+gpsdate+"','"+stringEvent+"','"+eventstatus+"','"+apikeyid+"','"+sourceip+"','"+reqtime+"',"+gpsdateepoch+")");
        var request = new sql.Request();
        request.query("INSERT INTO gpsdata (vendor, worder, containerno, trailerId, bookingno, gpslat, gpslon, gpsdate, event, eventstatus, apikeyid, sourceip, reqtime, gpsdateepoch) VALUES ('"+vendor+"','"+worder+"','"+containerno+"','"+trailerId+"','"+bookingno+"',"+gpslat+","+gpslon+",'"+gpsdate+"','"+stringEvent+"','"+eventstatus+"','"+apikeyid+"','"+sourceip+"','"+reqtime+"',"+gpsdateepoch+")").then(function(recordset) {
           if(i+1 === gpsdata.length){
              sql.close();
              console.log('Completed...')
            }
        }).catch(function(err) {
           if(i+1 === gpsdata.length){
              sql.close();
              console.log(err)
            }
        });
        
        
       
        
      })(i,gpsdata);
    
      
    }
    
    
    
  //error handler  
  }).catch(function(err) {
    if (err) {
      console.log("SQL Error :"+err);
    }
  });
  
  //erorr handler
  sql.on('error', (err) => {
    console.log("SQL Error :"+err);
  });
  
  
  
}