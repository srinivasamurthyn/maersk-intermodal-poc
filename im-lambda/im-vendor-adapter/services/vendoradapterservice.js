var request = require('sync-request');
const utils = require('../utils/utils.js');
const mapFields = require('../config/config.json').mapFields;
const vendorList = require('../config/config.json').vendorList;
var imApiGateway = require('../config/config.json').imApiGateway;


exports.processVendorAdapter = function (callback) {
	
	
		for(var i=0;i<vendorList.length;i++){
			(function(i, vendorList) {
			
			try{	
					let vendorApi = vendorList[i];
					
					//get vendor gps data
					let resGet = request('GET', vendorApi.apiUrl);
					let body=null;
					if(resGet){
						body = JSON.parse(resGet.getBody('utf8'));
					}
					
					console.log('Starting processing vendor:',vendorApi.apiUrl);
					
					if(body){
						
						
						let gpsData = utils.mapGpsVendorData(body,vendorApi.mapField,mapFields);
						console.log('gpsdata from vendor:',gpsData.length);
						if(gpsData.length >0){
							
								console.log('gpsdata [0]:',gpsData[0]);
								
							//post api gateway
							let imVendorApi =  imApiGateway.replace("{vendor-id}",vendorApi.id);
							console.log('imVendorApi',imVendorApi);
						
							let respost = request('POST', imVendorApi, { headers: {'x-api-key' : vendorApi.imApiKey } , json: {records:gpsData} });
							var result = JSON.parse(respost.getBody('utf8'));
							console.log(vendorApi.id +' -- ', result);
						}else{
							console.log('Empty data from vendor:',gpsData);
						}
						
						
					}else{
						console.log('Empty/null data from vendor:', body);
					}
					console.log('Completed vendor Execution:',vendorApi.apiUrl);
				}catch(err){
					callback(err, 'Error from Lambda');
				}
				
			})(i, vendorList);
		
			
		}
		callback(null, 'Completed all Execution');
	
		
	

}