const EventEmitter = require('events');
const sql = require('mssql');

const utils = require('./utils/utils.js');
const vendoradapterservice = require('./services/vendoradapterservice.js');

EventEmitter.defaultMaxListeners = Infinity;
const config = require('./config/config.json').mssqlConfig;
var localConfig = { "lastExeTime":1534232618668 };
var lastExeTime = 0;

const waitTimeInMs=600000;

//const waitTimeInMs=6000;


exports.handler = (event, context, callback) => {
   console.log('Starting...');
  
  
  sql.connect(config, (err) => {
        if (err) {
            callback(err);
        } else {
            let req = new sql.Request();
            req.query("SELECT * FROM settings WHERE  keycol = 'lambda-lastExeTime'", (error, results) => {
                if (error) {
                    sql.close();
                    callback(error);
                } else {
                	    var settings = results.recordset;
						if (utils.isArrayNotEmpty(settings) && settings[0]) {
						    settings= settings[0];
					        console.log('settings',settings);
					        console.log('lambda-lastExeTime:',settings.value);
					        lastExeTime = parseInt(settings.value);
					        
					        var now = new Date().getTime();
					        console.log('now:',now);
                            if(lastExeTime+waitTimeInMs< now){
                                console.log('Due to execute now. Last executed TimeStamp:', new Date().setTime(lastExeTime));
                                //Execute
                                
                                vendoradapterservice.processVendorAdapter(function(err,data){
        
                                  if(err){
                                        console.log('Error:',err);
                                    }
                                    console.log('data:',data); 
                                     //record
                                     
                                    req.query("UPDATE settings SET value = '"+now+"'WHERE  keycol = 'lambda-lastExeTime'",(error, data) => {  
                                     	if (error) {
                                     	   sql.close();
                                     	   callback(error);
                                         } else {
                                                console.log('Updated settings', data);
                                             	sql.close();
                                             	callback(null,"Completed Execution, updated Settings and exiting...");
                                         }
                                    });
                                    
                                });
   
                                
                               
                               
                                
                                
                            }else{
                                console.log('Already executed within 15 min. Skipping now...');
                                
                                 sql.close();
							    callback(null, "Already executed skipping...");
                            }
					        
					        
					        
					       
						}else {
							sql.close();
							callback(null, "Empty data...");
						}
                    
                }
            });
           }  
    });   
             
};



