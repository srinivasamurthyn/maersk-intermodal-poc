exports.isArray = function(array){
   return Array.isArray(array);
};

exports.isArrayNotEmpty = function(obj){
	return (null!==obj && undefined !== obj && obj.length >0)?true:false;
};


exports.isObjectNotEmpty = function(obj){
	return (obj !== undefined && obj !== null)?true:false;
};

exports.isObjectEmpty =function (obj){
	return (null===obj || undefined === obj)?true:false;
};

exports.isStringNotEmpty = function(obj){
	return (this.isObjectNotEmpty(obj) && obj !== '')?true:false;
};

exports.isStringEmpty =function (obj){
	return (this.isObjectEmpty(obj) || obj === '')?true:false;
};


exports.mapGpsVendorData = function(vendorGps,maps,mapFields) {
	var fields = mapFields.split(",");
	var result=[];
	for(var i=0;i<vendorGps.length;i++){
		var obj = {};
		for(var j=0;j<fields.length;j++){
			var field = maps[fields[j]];
			if(field){
				if(field.indexOf(".") !== -1){
					var nested = field.split(".");
					obj[fields[j]] = vendorGps[i][nested[0]][nested[1]];
				}else if(vendorGps[i][maps[fields[j]]]){
					if(fields[j]+"Type" !== undefined && maps[fields[j]+"Type"] === "N"){
						obj[fields[j]]=parseFloat(vendorGps[i][maps[fields[j]]]);
					}else{
						obj[fields[j]]=vendorGps[i][maps[fields[j]]].toString();
					}
				}else{
					if(maps[fields[j]+"Type"] === "N"){
						obj[fields[j]]=0;
					}else{
						obj[fields[j]]=null;
					}
				}
			}else{
				obj[fields[j]]=null;
			}
		}
		result.push(obj);
	}
	return result;
}