const sql = require('mssql');
const config = require('../config/config.json').mssqlConfig;
const utils = require('../utils/utils.js');


exports.linkWorkOrderToContainer = function (callback){
	sql.connect(config, (err) => {
        if (err) {
            callback(err);
        } else {
        	
        	
        	var containerslist =[];
        	var workorderslist =[];
        	
        	
        	//select containers from table
        	let req = new sql.Request();
            req.query("SELECT * FROM containers WHERE linkConToWoStatus = 'NO' AND status = 'LOADED'",(error, containers) => {
	            if (error) {
	            	sql.close();
	            	callback(error);
	            } else {
	            	containerslist = containers.recordset;
	            	let req = new sql.Request();
	            	req.query("SELECT * FROM workorders WHERE linkWoToConStatus = 'NO'",(error, workorders) => {
	    	            if (error) {
	    	            	sql.close();
	    	            	callback(error);
	    	            } else {
	    	            	workorderslist = workorders.recordset;
	    	            	//chech each other
	    	            	if(utils.isArrayNotEmpty(containerslist) && utils.isArrayNotEmpty(workorderslist)){
	    	            		for(var i = 0 ; i < containerslist.length; i++ ){
		    	            		for(var j = 0 ; j < workorderslist.length; j++ ){
		    	            			
		    	            			//sync function
		    	                		(function(i,j,containerslist,workorderslist){
		    	                			if(containerslist[i].containerNo === workorderslist[j].containerNo){
		    	                				
		    	                				console.log(containerslist[i].loadedDate >= workorderslist[j].departureDate);
		    	                				console.log(containerslist[i].loadedDate >= workorderslist[j].arrivalDate);
		    	                				
			    	            				if(containerslist[i].loadedDate >= workorderslist[j].departureDate && containerslist[i].loadedDate <= workorderslist[j].arrivalDate){
			    	            					let req = new sql.Request();
	                                           		req.query("UPDATE containers SET workOrderId = '"+workorderslist[j].workOrderId+"',linkConToWoStatus ='YES' WHERE containerId = "+containerslist[i].containerId+"",(error, result) => {
	                                           			if(error){
	                                           				sql.close();
	                                           				callback(error);
	                                           			}else{
	                                           				let req = new sql.Request();
	                                           				req.query("UPDATE workorders SET containerId = '"+containerslist[i].containerId+"',linkWoToConStatus ='YES' WHERE workOrderId = "+workorderslist[j].workOrderId+"",(error, result) => {
	                                                   			if(error){
	                                                   				sql.close();
	                                                   				callback(error);
	                                                   			}else{
	                                                   				if((i+1 === containerslist.length) && (j+1 === workorderslist.length)){
	                                                               		sql.close();
	                                                               		callback(null,"Completed...");
	                                                               	}
	                                                   			}
	                                                   		});
	                                           			}
	                                           		});
			    	            				}else{
			    	            					if((i+1 === containerslist.length) && (j+1 === workorderslist.length)){
		                                           		sql.close();
		                                           		callback(null,"Completed...");
		                                           	}
			    	            				} 				
			    	            			}else{
			    	            				if((i+1 === containerslist.length) && (j+1 === workorderslist.length)){
	                                           		sql.close();
	                                           		callback(null,"Completed...");
	                                           	}
			    	            			}
		    	                		})(i,j,containerslist,workorderslist);
		    	                	}
		    	            	}
	    	            	}else{
	    	            		sql.close();
                           		callback(null,"Completed...");
	    	            	}
	    	            }
	    	        });
	            }
            });
        }
	});
 
	
	//sql connection error handle
    sql.on('error', (err) => {
        callback(err);
    });
	
	
}