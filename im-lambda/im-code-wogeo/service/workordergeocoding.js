const sql = require('mssql');
const config = require('../config/config.json').mssqlConfig;
const googleMapsClientConfig = require('../config/config.json').googleMapsClient;
const googleMapsClient = require('@google/maps').createClient(googleMapsClientConfig);
const utils = require('../utils/utils.js');


exports.processGeoCoding = function (callback){
    //Geocode an address.
    
	sql.connect(config, (err) => {
        if (err) {
            callback(err);
        } else {
        	
        	//select workorders from table where geoCodeStatus = "NO"
        	let req = new sql.Request();
            req.query("SELECT * FROM workorders WHERE geoCodeStatus !='YES'",(error, result) => {
            if (error) {
               	console.log('Error 1 : '+error);
            } else {
            	
            	//for each workorders update geocode
            	var workorders = result.recordset;
                if(utils.isArrayNotEmpty(workorders)){
                	for(var i=0;i<workorders.length;i++){
                		
                		//sync function
                		(function(i,workorders){
                           	
                			//beginAddressline1 proccess
                			var startAddress;
                			if(workorders[i].beginAddressline1){
                			    startAddress = workorders[i].beginAddressline1;
                			}
                			
                			if(workorders[i].beginAddressline2){
                			    startAddress = startAddress +","+ workorders[i].beginAddressline2;
                			}
                			
                			if(workorders[i].beginAddressline2){
                			    startAddress = startAddress +","+ workorders[i].beginAddressline3;
                			}
                			
                			
                			
                			if(workorders[i].beginAddressline1){
                                   googleMapsClient.geocode({
                                   	address: startAddress
                                   }, function(err, response) {
                                   	if (!err) {
                                   		let req = new sql.Request();
                                   		var loc = response.json.results[0].geometry.location;
                                   		var address_components = response.json.results[0].address_components;
                                   		var countryCode;
                                   		
                                   		if(address_components[address_components.length-1].types.toString().indexOf("country") === 0){
                                   		    countryCode = address_components[address_components.length-1].short_name;
                                   		}else if(address_components[address_components.length-2].types.toString().indexOf("country") === 0){
                                   		    countryCode = address_components[address_components.length-2].short_name;
                                   		}else if(address_components[address_components.length-3].types.toString().indexOf("country") === 0){
                                   		    countryCode = address_components[address_components.length-3].short_name;
                                   		}
                                   		
                                   		req.query("UPDATE workorders SET beginLat = "+loc.lat+",beginLon = "+loc.lng+", countryCode = '"+countryCode+"'  WHERE workOrderId = '"+workorders[i].workOrderId+"'",(error, result) => {
                                   			if(error){
                                   				console.log('Error2 : '+error);
                                   			}else{
                                   			    //endAddressline1 proccess
                                    			var endAddress;
                                    			if(workorders[i].endAddressline1 ){
                                    			    endAddress = workorders[i].endAddressline1;
                                    			}
                                    			
                                    			if(workorders[i].endAddressline2){
                                    			    endAddress = endAddress +","+ workorders[i].endAddressline2;
                                    			}
                                    			
                                    			if(workorders[i].endAddressline3){
                                    			    endAddress = endAddress +","+ workorders[i].endAddressline3;
                                    			}
                                    			
                                    			
                                                if(workorders[i].endAddressline1){
                                                	googleMapsClient.geocode({
                                                		address: endAddress
                                                    }, function(err, response) {
                                                      	if (!err) {
                                                       		let req = new sql.Request();
                                                       		var loc = response.json.results[0].geometry.location;
                                                       		req.query("UPDATE workorders SET endLat = "+loc.lat+",endLon = "+loc.lng+",geoCodeStatus = 'YES' WHERE workOrderId = '"+workorders[i].workOrderId+"'",(error, result) => {
                                                       			if(error){
                                                       				console.log('Error4 : '+error);
                                                       			}
                                                       			
                                                       			//check last record and return
                                                       			if(i+1 === workorders.length){
                                                               		sql.close();
                                                               		callback(null,"Completed...");
                                                               	}
                                                       		});
                                                       	}else{
                                                       		console.log('Error5 : '+err);
                                                       		
                                                       		
                                                       		//check last record and return
                                                       		if(i+1 === workorders.length){
                                                        		sql.close();
                                                           		callback(null,"Completed...");
                                                           	}
                                                       	}
                                                    });
                                                }
                                   			}
                                   		});
                                   	}else{
                                   		console.log('Error3 : '+err);
                                	}
                                });
                            }
                            
                			
                        })(i,workorders);
                    }
                }else{
                	sql.close();
                	callback(null,"Completed...");
                }
            	}
            });
        }
	});
 
	
	//sql connection error handle
    sql.on('error', (err) => {
        callback(err);
    });
    
    
}