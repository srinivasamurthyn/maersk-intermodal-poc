const EventEmitter = require('events');
EventEmitter.defaultMaxListeners = Infinity;
const etageocodingservice = require('./services/etageocodingservice.js');

exports.handler = (event, context, callback) => {
	etageocodingservice.processGeoCoding(function(err,data){
		callback(err,data);
	});
};
