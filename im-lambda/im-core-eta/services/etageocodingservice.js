const sql = require('mssql');
const request = require('request');
const config = require('../config/config.json');
const googleUrl = config.googleMapsClient.url;
const key = "&key=" + config.googleMapsClient.key;
const mssqlConfig = config.mssqlConfig;
const utils = require('../utils/utils.js');




exports.processGeoCoding = function(callback) {

    sql.connect(mssqlConfig, (err) => {
        if (err) {
            callback(err);
        } else {
            let req = new sql.Request();
            req.query("SELECT * FROM containers WHERE status='LOADED' AND geoCodeStatus = 'YES'", (error, containers) => {
                if (error) {
                    sql.close();
                    callback(error);
                } else {
                    var containers = containers.recordset;
                    if (utils.isArrayNotEmpty(containers)) {
                        for (var i = 0; i < containers.length; i++) {
                            (function(i, containers) {
                                req.query("SELECT * FROM trailers WHERE trailerId = " + containers[i].trailerTrailerId, (error, trailers) => {
                                    if (error) {
                                        sql.close();
                                        callback(error);
                                    } else {
                                        var trailers = trailers.recordset;
                                        if (utils.isArrayNotEmpty(trailers)) {
                                            req.query("SELECT  TOP 1 gpslat,gpslon FROM gpsdata WHERE trailerId='" + trailers[0].trailerNo + "' ORDER BY gpsdateepoch DESC", (error, gpsdata) => {
                                                if (error) {
                                                    sql.close();
                                                    callback(error);
                                                } else {
                                                    var gpsdata = gpsdata.recordset;
                                                    if (utils.isArrayNotEmpty(gpsdata)) {
                                                        var origins = "&origins=" + gpsdata[0].gpslat + "," + gpsdata[0].gpslon;
                                                        var destinations = "&destinations=" + containers[i].eloclat + "," + containers[i].eloclon;
                                                        request(googleUrl + origins + destinations + key, {
                                                            json: true
                                                        }, (err, res, body) => {
                                                            if (err) {
                                                            	callback(err);
                                                            }else if(body && body.rows[0] && body.rows[0].elements[0]){
                                                            	var etaData = body.rows[0].elements[0];
                                                            	req.query("UPDATE containers SET etaDistance = '"+etaData.distance.text+"',etaDuration ='"+etaData.duration.text+"' WHERE containerId = "+containers[i].containerId,(error, result) => {
    	                                                   			if(error){
    	                                                   				sql.close();
    	                                                   				callback(error);
    	                                                   			}else{
    	                                                   				if (i + 1 === containers.length) {
    	                                                                    sql.close();
    	                                                                    callback(null, "Completed...");
    	                                                                }
    	                                                   			}
    	                                                   		});
                                                            }else{
                                                            	if (i + 1 === containers.length) {
                                                                    sql.close();
                                                                    callback(null, "Completed...");
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        if (i + 1 === containers.length) {
                                                            sql.close();
                                                            callback(null, "Completed...");
                                                        }
                                                    }
                                                }
                                            });
                                        } else {
                                            if (i + 1 === containers.length) {
                                                sql.close();
                                                callback(null, "Completed...");
                                            }
                                        }
                                    }
                                });

                            })(i, containers);
                        }
                    } else {
                        sql.close();
                        callback(null, "Completed...");
                    }
                }
            });
        }
    });


    //sql connection error handle
    sql.on('error', (err) => {
        callback(err);
    });


};