const utils = require('./utils/utils.js'); 
const geocodingservices = require('./service/geocodingservices.js');
const EventEmitter = require('events');
EventEmitter.defaultMaxListeners = Infinity;



exports.handler = (event, context, callback) => {
    console.log("Event : " +JSON.stringify(event));
    if(utils.isObjectNotEmpty(event)){
        geocodingservices.processGeoCoding(event,function(err,data){
             if(err){
                context.succeed('Error' + err);
            }else{
                console.log(data);
                context.succeed('Success ' + data);
            }
        });
    }
};


