exports.isArray = function(array){
   return Array.isArray(array);
};

exports.isArrayNotEmpty = function(obj){
	return (null!==obj && undefined !== obj && obj.length >0)?true:false;
};

exports.isObjectNotEmpty = function(obj){
	return (obj !== undefined && obj !== null)?true:false;
};

exports.isObjectEmpty =function (obj){
	return (null===obj || undefined === obj)?true:false;
};

exports.getEpochTime = function(date){
    var date = new Date(date);
    if(typeof(date) === "date"){
       return date.getTime();
    }else{
        return new Date().getTime();
    }
};

exports.isStringNotEmpty = function(obj){
	return (this.isObjectNotEmpty(obj) && obj !== '')?true:false;
};

exports.isStringEmpty =function (obj){
	return (this.isObjectEmpty(obj) || obj === '')?true:false;
};