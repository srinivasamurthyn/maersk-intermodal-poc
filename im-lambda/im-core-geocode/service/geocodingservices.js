const sql = require('mssql');
const config = require('../config/config.json').mssqlConfig;
const geodistance = require('../config/config.json').geodistance;
const utils = require('../utils/utils.js');
const geoutils = require('../utils/geoutils.js');

exports.processGeoCoding = function(gpsdata, callback) {

    sql.connect(config, (err) => {
        if (err) {
            callback(err);
        } else {


            //select trailers by id
            let req = new sql.Request();
            req.query("SELECT * FROM trailers WHERE trailerNo = '" + gpsdata.id + "' AND status = 'STARTED'", (error, trailers) => {
                if (error) {
                    sql.close();
                    callback(error);
                } else {
                    //select containers by trailerId
                    var trailers = trailers.recordset;
                    if (utils.isArrayNotEmpty(trailers)) {
                    	let req = new sql.Request();
                        req.query("SELECT * FROM containers WHERE trailerTrailerId = " + trailers[0].trailerId + " AND status = 'LOADED'", (error, containers) => {
                            if (error) {
                                sql.close();
                                callback(error);
                            } else {
                                var containers = containers.recordset;
                                 if (utils.isArrayNotEmpty(containers)) {
                                    for (var i = 0; i < containers.length; i++) {
                                        (function(i, containers) {
                                            if (containers[i].geoCodeStatus === "YES") {
                                                var sgfStatus = containers[i].sgfStatus;
                                                if(sgfStatus === "EXITED"){
                                                	//update status geoCoding end loc distance
                                                    var geoDist = geoutils.findDistance(containers[i].eloclat, containers[i].eloclon, gpsdata.gpslat, gpsdata.gpslong);
                                                    if(geoDist <  geodistance){
                                                    	egfStatus = "IN";
                                                    	egfInTime = utils.getEpochTime(gpsdata.gpsdate);
                                                    	req.query("UPDATE containers SET egfStatus = '"+egfStatus+"',egfInTime = "+egfInTime+" WHERE containerId = "+containers[i].containerId+"",(error, data) => {  
                                                         	if (error) {
                                                         	   sql.close();
                                                         	   callback(error);
                                                             } else {
                                                                 if(i+1 === containers.length){
                                                                 	sql.close();
                                                                 	callback(null,"Completed...");
                                                                 } 
                                                             }
                                                        });
                                                    }else{
                                                    	if(i+1 === containers.length){
                                                         	sql.close();
                                                         	callback(null,"Completed...");
                                                         } 
                                                    }
                                                }else{
                                            		//update status geoCoding start loc distance
                                                    var geoDist = geoutils.findDistance(containers[i].sloclat, containers[i].sloclon, gpsdata.gpslat, gpsdata.gpslong);
                                                    var sgfInTime = containers[i].sgfInTime;
                                                    var sgfExitTime = containers[i].sgfInTime;
                                                    if(sgfStatus !== "IN" && geoDist < geodistance){
                                                    	sgfStatus = "IN";
                                                    	sgfInTime = utils.getEpochTime(gpsdata.gpsdate);
                                                    }else if (sgfStatus === "IN" && geoDist > geodistance){
                                                    	sgfStatus = "EXITED";
                                                    	sgfExitTime = utils.getEpochTime(gpsdata.gpsdate);
                                                    }
                                                    
                                                    req.query("UPDATE containers SET sgfStatus = '"+sgfStatus+"',sgfInTime = "+sgfInTime+",sgfExitTime = "+sgfExitTime+" WHERE containerId = "+containers[i].containerId+"",(error, data) => {  
                                                     	if (error) {
                                                     	   sql.close();
                                                     	   callback(error);
                                                         } else {
                                                             if(i+1 === containers.length){
                                                             	sql.close();
                                                             	callback(null,"Completed...");
                                                             } 
                                                         }
                                                    });
                                                    
                                                    
                                            	}
                                            } else {
                                                //update containers table  first time
                                                req.query("SELECT * FROM workorders WHERE containerId = '" + containers[i].containerId + "' AND geoCodeStatus = 'YES'", (error, workorder) => {
                                                    if (error) {
                                                        console.log(error);
                                                        sql.close();
                                                        
                                                        
                                                        callback(error);
                                                    } else {
                                                        
                                                        console.log(workorder);
                                                        
                                                        var workorder = workorder.recordset;
                                                        if (utils.isArrayNotEmpty(workorder)) {
                                                            req.query("UPDATE containers SET sloclat = " + workorder[0].beginLat + ",sloclon = " + workorder[0].beginLon + ",eloclat = " + workorder[0].endLat + ",eloclon = " + workorder[0].endLon + ",ctryCode = '" + workorder[0].countryCode + "',geoCodeStatus ='YES' WHERE containerId = " + workorder[0].containerId + "", (error, data) => {
                                                                if (error) {
                                                                    sql.close();
                                                                    callback(error);
                                                                } else {
                                                                    if (i + 1 === containers.length) {
                                                                        sql.close();
                                                                        callback(null, "Completed...");
                                                                    }
                                                                }
                                                            });
                                                        } else {
                                                            if (i + 1 === containers.length) {
                                                                sql.close();
                                                                callback(null, "Completed...");
                                                            }
                                                        }

                                                    }
                                                });
                                            }


                                        })(i, containers);
                                    }
                                } else {
                                    sql.close();
                                    callback(null, "Completed...");
                                }
                            }
                        });
                    } else {
                        sql.close();
                        callback(null, "Completed...");
                    }
                }
            });
        }
    });

    sql.on('error', (err) => {
        callback(err);
    });



}