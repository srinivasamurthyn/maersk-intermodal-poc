package com.linkeddots.im.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkeddots.im.data.Trailer;

@Repository("trailerRepository")
public interface TrailerRepository extends JpaRepository<Trailer, Long>{


}