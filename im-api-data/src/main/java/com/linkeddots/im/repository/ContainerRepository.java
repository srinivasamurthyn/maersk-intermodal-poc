package com.linkeddots.im.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkeddots.im.data.Container;

@Repository("containerRepository")
public interface ContainerRepository extends JpaRepository<Container, Long>{


}