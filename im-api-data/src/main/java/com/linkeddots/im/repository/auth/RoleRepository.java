package com.linkeddots.im.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkeddots.im.data.auth.Role;

@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role, Long>{


}