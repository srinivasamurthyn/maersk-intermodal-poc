package com.linkeddots.im.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkeddots.im.data.GpsData;

@Repository("gpsdataRepository")
public interface GpsDataRepository extends JpaRepository<GpsData, Long>{


}