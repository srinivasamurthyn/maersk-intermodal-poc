package com.linkeddots.im.repository.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.linkeddots.im.data.auth.UserProfile;

@Repository("userProfileRepository")
public interface UserProfileRepository extends JpaRepository<UserProfile, Long>{

	public UserProfile findByUserName(String username);


}