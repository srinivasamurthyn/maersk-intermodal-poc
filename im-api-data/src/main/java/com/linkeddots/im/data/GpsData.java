/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.Getter;
import lombok.Setter;

/**
 * @author Linkeddots Development Team.
 *
 */

//Done
@Entity
@Table(name = "GpsData")
@Getter
@Setter
public class GpsData implements Serializable {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5058631590750750203L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "vendor")
	private String vendor;
	
	@Column(name = "worder")
	private String worder;
	
	@Column(name = "containerno")
	private String containerno;
	
	@Column(name = "trailerId")
	private String trailerId;
	
	@Column(name = "bookingno")
	private String bookingno;
	
	@Column(name = "gpslat")
	private double gpslat;
	
	@Column(name = "gpslon")
	private double gpslon;
	
	@Column(name = "gpsdate")
	private String gpsdate;
	
	@Column(name = "event")
	private String event;
	
	@Column(name = "eventstatus")
	private String eventstatus;
	
	@Column(name = "apikeyid")
	private String apikeyid;
	
	@Column(name = "sourceip")
	private String sourceip;
	
	@Column(name = "reqtime")
	private String reqtime;
	
	@Column(name = "gpsdateepoch")
	private long gpsdateepoch;

 

}