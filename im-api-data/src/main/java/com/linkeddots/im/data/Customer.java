/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



import lombok.Getter;
import lombok.Setter;

/**
 * @author Linkeddots Development Team.
 *
 */

//Done
@Entity
@Table(name = "Customer")
@Getter
@Setter
public class Customer implements Serializable {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5058631590750750203L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "customerNumber")
	private String customerNumber;
	
	@Column(name = "customerName")
	private String customerName;
	
	@Column(name = "addressLine1")
	private String addressLine1;
	
	@Column(name = "addressLine2")
	private String addressLine2;
	
	@Column(name = "addressLine3")
	private String addressLine3;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "zipCode")
	private String zipCode;
	
	@Column(name = "emailAddress")
	private String emailAddress;
	
	@Column(name = "contactNo")
	private String contactNo;
	
	@Column(name = "vipFlag")
	private String vipFlag;
	

}