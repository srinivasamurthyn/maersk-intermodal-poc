package com.linkeddots.im.data.auth;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component("passwordEncoderGenerator")
public class PasswordEncoderGenerator {

	public static void main(String[] args) {
	//	String password = "admin"; //admin, user
		String password = "12345"; 
		
		PasswordEncoderGenerator pg = new PasswordEncoderGenerator();
		
		String enc = pg.enCrypt(password);
		
		System.out.println("Encrypted token for:" + password);
		System.out.println(enc);
		System.out.println("\n");
		
	}
	
	
	public static String enCrypt(String password){
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		String hashedPassword = passwordEncoder.encode(password);
		System.out.println("Password verfied:"+ passwordEncoder.matches(password, hashedPassword));
		return hashedPassword;
	}
	
	
	

}
