/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



import lombok.Getter;
import lombok.Setter;

/**
 * @author Linkeddots Development Team.
 *
 */

//Done
@Entity
@Table(name = "WorkOrder")
@Getter
@Setter
public class WorkOrder implements Serializable {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5058631590750750203L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "workOrderNo")
	private String workOrderNo;
	
	@Column(name = "woCreatedYearWeek")
	private String woCreatedYearWeek;
	
	@Column(name = "woMode")
	private String woMode;
	
	@Column(name = "woCompleteYearWeek")
	private String woCompleteYearWeek;
	
	@Column(name = "woCompleteDate")
	private String woCompleteDate;
	
	@Column(name = "woDetailType")
	private String woDetailType;
	
	@Column(name = "beginAddressline1")
	private String beginAddressline1;
	
	@Column(name = "beginAddressline2")
	private String beginAddressline2;
	
	@Column(name = "beginAddressline3")
	private String beginAddressline3;
	
	@Column(name = "beginZip")
	private String beginZip;
	
	@Column(name = "endAddressline1")
	private String endAddressline1;
	
	@Column(name = "endAddressline2")
	private String endAddressline2;
	
	@Column(name = "endAddressline3")
	private String endAddressline3;
	
	@Column(name = "countryCode")
	private String countryCode;
	
	@Column(name = "stateCode")
	private String stateCode;
	
	@Column(name = "clearencePort")
	private String clearencePort;
	
	@Column(name = "blNumber")
	private String blNumber;
	
	@Column(name = "blNumberOriginal")
	private String blNumberOriginal;
	
	@Column(name = "comment")
	private String comment;
	
	@Column(name = "commodityDescription")
	private String commodityDescription;
	
	@Column(name = "departureDate")
	private Long departureDate;
	
	@Column(name = "arrivalDate")
	private Long arrivalDate;
	
	@Column(name = "beginLat")
	private double beginLat;
	
	@Column(name = "beginLon")
	private double beginLon;
	
	@Column(name = "endLat")
	private double endLat;
	
	@Column(name = "endLon")
	private double endLon;
	
	@Column(name = "containerNo")
	private Long containerNo;
	
	@Column(name = "containerId")
	private String containerId;
	
	@Column(name = "linkWoToConStatus")
	private String linkWoToConStatus;
	
	@Column(name = "status")
	private String status;


}