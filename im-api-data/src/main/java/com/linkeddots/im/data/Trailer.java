/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;



import lombok.Getter;
import lombok.Setter;

/**
 * @author Linkeddots Development Team.
 *
 */

//Done
@Entity
@Table(name = "Trailer")
@Getter
@Setter
public class Trailer implements Serializable {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5058631590750750203L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "trailerId")
	private long trailerId;
	
	@Column(name = "trailerNo")
	private String trailerNo;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "startTime")
	private long startTime;
	
	@Column(name = "endTime")
	private long endTime;
	
	@Column(name = "sloclat")
	private double sloclat;
	
	@Column(name = "sloclon")
	private double sloclon;
	
	@Column(name = "slocgcStatus")
	private String slocgcStatus;
	
	@Column(name = "sCtryCode")
	private String sCtryCode;
	
	@Column(name = "eloclat")
	private double eloclat;
	
	@Column(name = "eloclon")
	private double eloclon;
	
	@Column(name = "elocgcStatus")
	private String elocgcStatus;
	
	@Column(name = "eCtryCode")
	private String eCtryCode;
 

}