/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.data.auth;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "UserProfile")
@Getter
@Setter
public class UserProfile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6815379861643922076L;
	
		
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
    private long id;
	
	@Column(name = "userType")
	private String userType;

	@Column(name = "userName")
	private String userName;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "userEnable")
	private boolean userEnable;
	
	@Column(name = "userEmail")
	private String userEmail;
	
	@Column(name = "landingRole")
	private String landingRole;
	
	@ManyToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Role> roles;
	
}

