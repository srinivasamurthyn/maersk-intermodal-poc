/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



import lombok.Getter;
import lombok.Setter;

/**
 * @author Linkeddots Development Team.
 *
 */

//Done
@Entity
@Table(name = "Container")
@Getter
@Setter
public class Container implements Serializable {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5058631590750750203L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "containerId")
	private long containerId;
	
	@Column(name = "containerNo")
	private String containerNo;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "loadedDate")
	private long loadedDate;
	
	@Column(name = "loadedTime")
	private long loadedTime;
	
	@Column(name = "unloadedDate")
	private long unloadedDate;
	
	@Column(name = "unloadedTime")
	private long unloadedTime;
	
	@Column(name = "sloclat")
	private double sloclat;
	
	@Column(name = "sloclon")
	private double sloclon;
	
	@Column(name = "eloclat")
	private double eloclat;
	
	@Column(name = "eloclon")
	private double eloclon;
	
	@Column(name = "ctryCode")
	private String ctryCode;
	
	@Column(name = "geoCodeStatus")
	private String geoCodeStatus;
	
	@Column(name = "sgfStatus")
	private String sgfStatus;
	
	@Column(name = "sgfInTime")
	private long sgfInTime;
	
	@Column(name = "egfStatus")
	private String egfStatus;
	
	@Column(name = "egfInTime")
	private long egfInTime;
	
	@Column(name = "egfExitTime")
	private long egfExitTime;
	
	@Column(name = "etaDistance")
	private String etaDistance;
	
	@Column(name = "etaDuration")
	private String etaDuration;
	
	@Column(name = "linkConToWoStatus")
	private String linkConToWoStatus;
	
	@Column(name = "workOrderId")
	private String workOrderId;
	
	@ManyToOne
	private Trailer trailer;  
	

}