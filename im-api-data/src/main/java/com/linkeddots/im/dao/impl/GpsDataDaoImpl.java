/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.linkeddots.im.dao.GpsDataDao;
import com.linkeddots.im.data.GpsData;
import com.linkeddots.im.repository.GpsDataRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Linkeddots Development Team.
 *
 */

@CacheConfig(cacheNames="gpsdatas" , cacheManager="cacheManager")
@Component("GpsDataDao")
@Slf4j
public class GpsDataDaoImpl implements GpsDataDao {
	
	
	@Autowired
	private GpsDataRepository repository;

	@Cacheable
	public List<GpsData> findAll() throws Exception {
		log.debug("findAll() : Start."); 
        return repository.findAll();
    }
	
	@Cacheable
	public GpsData findOne(long id) throws Exception {
		return repository.findOne(id);
    }
	
	@CacheEvict(key = "#id", allEntries=true)
	public GpsData create(GpsData GpsData) throws Exception {
		clearCentralCacheId();
        return repository.save(GpsData);
    }
	
	@CachePut(key="#id")
	@CacheEvict(key="#id",allEntries=true)
	public void update(long id, GpsData GpsData) throws Exception {
		clearCentralCacheId();
		repository.save(GpsData);
    }
	
	@CacheEvict(key="#id",allEntries=true)
	public void delete(long id) throws Exception {
		clearCentralCacheId();
        repository.delete(id);
    }

	@Override
	public long count() throws Exception {
		return repository.count();
	}
	
	@CacheEvict(cacheManager="centralCacheManager", allEntries=true)
	public void clearCentralCacheId() throws Exception {
		//clear clearCentralCache
    }
	
	@Override
	@Cacheable(cacheManager="centralCacheManager")
	public String getCentralCacheId() throws Exception {
		return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
    }
	
	
}
