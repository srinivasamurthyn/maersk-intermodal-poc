/*
\ * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao;

import java.util.List;

import com.linkeddots.im.data.Container;

/**
 * @author Linkeddots Development Team.
 *
 */
public interface ContainerDao {
	
	public List<Container> findAll() throws Exception;
	
	public Container findOne(long id) throws Exception;
	
	public Container create(Container container) throws Exception;
	
	public void update(long id, Container container) throws Exception;

	public void delete(long id) throws Exception;
	 
	public long count() throws Exception;
 
	public String getCentralCacheId()throws Exception;


}
