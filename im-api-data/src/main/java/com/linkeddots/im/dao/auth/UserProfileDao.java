/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao.auth;

import java.util.List;
import com.linkeddots.im.data.auth.UserProfile;


public interface UserProfileDao {
	
	public List<UserProfile> findAll() throws Exception;
	
	public void update(Long id, UserProfile user) throws Exception;
	
	public void changeStatus(Long id, Boolean password) throws Exception;
	
	public void delete(Long id) throws Exception;

	public String getCentralCacheId() throws Exception;

	public UserProfile create(UserProfile user) throws Exception;

	public UserProfile findByUserName(String username) throws Exception;

	public UserProfile changePassword(long id, String password) throws Exception;

	public UserProfile findOne(long id) throws Exception;
	
	

}
