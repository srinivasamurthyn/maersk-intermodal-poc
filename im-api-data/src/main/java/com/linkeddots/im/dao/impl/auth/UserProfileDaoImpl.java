/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao.impl.auth;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.linkeddots.im.data.auth.PasswordEncoderGenerator;
import com.linkeddots.im.dao.auth.UserProfileDao;
import com.linkeddots.im.data.auth.UserProfile;
import com.linkeddots.im.exception.InvalidDataException;
import com.linkeddots.im.exception.PasswordPolicyException;
import com.linkeddots.im.exception.UserExistException;
import com.linkeddots.im.repository.auth.UserProfileRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Linkeddots Development Team.
 *
 */

@CacheConfig(cacheNames="users" , cacheManager="cacheManager")
@Component("userProfileDao")
@Slf4j
public class UserProfileDaoImpl implements UserProfileDao {
	
	@Autowired
	private UserProfileRepository repository;
	
	/**
	 * @return
	 */
	@Override
	@Cacheable
	public List<UserProfile> findAll() throws Exception {
		log.debug("findAll() : Start."); 
        return repository.findAll();
    }
	
	
	@Override
	@CacheEvict(key = "#id", allEntries=true)
	public UserProfile create(UserProfile user) throws Exception {
		clearCentralCacheId();
		validateUser(user);
		UserProfile existingUser = findByUserName(user.getUserName());
		if(null != existingUser){
			throw new UserExistException ("User Already Exist in the System");
		}
		checkPasswordPolicy(user.getPassword());
		user.setPassword(PasswordEncoderGenerator.enCrypt(user.getPassword()));
		UserProfile newUser= repository.save(user);
		newUser.setPassword(null);
        return newUser;
    }


	@Override
	@CachePut(key = "#id")
	@CacheEvict(key = "#id", allEntries=true)
	public void update(Long id, UserProfile user) throws Exception {
		clearCentralCacheId();
		validateUserUpdate(user);
		UserProfile existingUser = findOne(id);
		if(null == existingUser){
			throw new InvalidDataException ("User does not Exist in the System");
		}
		user.setPassword(existingUser.getPassword());
		repository.save(user);
    }
	
	@Override
	@CachePut(key = "#id")
	@CacheEvict(key = "#id", allEntries=true)
	public void changeStatus(Long id, Boolean enable) throws Exception {
		clearCentralCacheId();
		UserProfile existingUser = findOne(id);
		if(null == existingUser){
			throw new InvalidDataException ("User does not Exist in the System");
		}
		existingUser.setUserEnable(enable);
		repository.save(existingUser);
	}
	
	@Override
	public UserProfile changePassword(long id, String password) throws Exception {
		clearCentralCacheId();
		log.debug("Change Password:{}, {},{},{},{},{}",id, password);
		UserProfile user = findOne(id);
		if(null == user){
			throw new InvalidDataException ("User does not Exist in the System");
		}
		user.setPassword(password);
		return repository.save(user);
		
		
	}
	
	

	@Override
	@Cacheable
	public UserProfile findOne(long id) throws Exception {
		return repository.findOne(id);
	}
	
	

	@Override
	@Cacheable
	public UserProfile findByUserName(String username) throws Exception {
		log.debug("username:"+username);
		UserProfile user = repository.findByUserName(username);
		log.debug("user:"+user);
		return user;
	}
	
	


	@Override
	@CacheEvict(key = "#id", allEntries=true)
	public void delete(Long id) throws Exception {
		clearCentralCacheId();
		repository.delete(id);
	}


	private void validateUser(UserProfile user) throws InvalidDataException {
		if(StringUtils.isEmpty(user) || StringUtils.isEmpty (user.getUserName()) || StringUtils.isEmpty(user.getPassword())){
				throw new InvalidDataException("Invalid User Input data");
		}
	}
	
	private void validateUserUpdate(UserProfile user) throws InvalidDataException {
		if(  StringUtils.isEmpty(user) 
				|| StringUtils.isEmpty (user.getUserName())				
				||StringUtils.isEmpty(user.getUserEmail())
				
				){
			throw new InvalidDataException("Invalid User Input data");
		}
	}


	private void checkPasswordPolicy(String pass) throws PasswordPolicyException {
		
		
		if (StringUtils.isEmpty(pass) 
				|| !StringUtils.hasLength(pass)
				|| StringUtils.containsWhitespace(pass)
				|| !org.apache.commons.lang3.StringUtils.isMixedCase(pass)
				|| pass.length() < 8 
				|| pass.length() > 20
				){
			throw new PasswordPolicyException("Password does not comply to standards");
			
		}
	}


	@CacheEvict(cacheManager="centralCacheManager", allEntries=true)
	public void clearCentralCacheId() throws Exception {
		//clear clearCentralCache
	}
	
	@Override
	@Cacheable(cacheManager="centralCacheManager")
	public String getCentralCacheId() throws Exception {
		return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	}


	
}