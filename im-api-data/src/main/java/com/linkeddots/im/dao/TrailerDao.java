/*
\ * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao;

import java.util.List;

import com.linkeddots.im.data.Trailer;

/**
 * @author Linkeddots Development Team.
 *
 */
public interface TrailerDao {
	
	public List<Trailer> findAll() throws Exception;
	
	public Trailer findOne(long id) throws Exception;
	
	public Trailer create(Trailer trailer) throws Exception;
	
	public void update(long id, Trailer trailer) throws Exception;

	public void delete(long id) throws Exception;
	 
	public long count() throws Exception;
 
	public String getCentralCacheId()throws Exception;

}
