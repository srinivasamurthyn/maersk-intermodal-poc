/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao.auth;

import java.util.List;

import com.linkeddots.im.data.auth.Role;


public interface RoleDao {
	
	public List<Role> findAll() throws Exception;
	
	public Role findOne(long id) throws Exception;
	
	public Role create(Role role) throws Exception;
	
	public void update(long id, Role role) throws Exception;
	
	public void delete(long id) throws Exception;
	
	public long count() throws Exception;
	
	public String getCentralCacheId() throws Exception;
	
}
