/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao.impl.auth;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.linkeddots.im.dao.auth.RoleDao;
import com.linkeddots.im.data.auth.Role;
import com.linkeddots.im.repository.auth.RoleRepository;

/**
 * @author Linkeddots Development Team.
 *
 */
@CacheConfig(cacheNames="roles" , cacheManager="cacheManager")
@Component("roleDao")
public class RoleDaoImpl implements RoleDao {
	
	private static final Logger logger = LoggerFactory.getLogger(RoleDaoImpl.class);
	
	@Autowired
	private RoleRepository repository;

	/**
	 * @return
	 */
	@Cacheable
	public List<Role> findAll() throws Exception {
		logger.debug("findAll() : Start."); 
        return repository.findAll();
    }
	
	/**
	 * @return
	 */
	@Cacheable
	public Role findOne(long id) throws Exception {
		return repository.findOne(id);
    }
	
	/**
	 * @param role
	 * @return
	 */
	@CacheEvict(key = "#id", allEntries=true)
	public Role create(Role role) throws Exception {
		clearCentralCacheId();
        return repository.save(role);
    }
	
	/**
	 * @param role
	 * @return
	 */
	@CachePut(key = "#id")
	@CacheEvict(key = "#id", allEntries=true)
	public void update(long id, Role role) throws Exception {
		clearCentralCacheId();
		repository.save(role);
    }
	
	/**
	 * @param role
	 * @return
	 */
	@CacheEvict(key = "#id", allEntries=true)
	public void delete(long id) throws Exception {
		clearCentralCacheId();
        repository.delete(id);
    }
	
	@Override
	public long count() throws Exception {
		return repository.count();
	}
	
	@CacheEvict(cacheManager="centralCacheManager", allEntries=true)
	public void clearCentralCacheId() throws Exception {
		//clear clearCentralCache
	}
	
	@Override
	@Cacheable(cacheManager="centralCacheManager")
	public String getCentralCacheId() throws Exception {
		return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
	}
}
