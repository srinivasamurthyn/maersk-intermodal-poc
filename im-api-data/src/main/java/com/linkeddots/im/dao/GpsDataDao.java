/*
\ * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao;

import java.util.List;

import com.linkeddots.im.data.GpsData;

/**
 * @author Linkeddots Development Team.
 *
 */
public interface GpsDataDao {
	
	public List<GpsData> findAll() throws Exception;
	
	public GpsData findOne(long id) throws Exception;
	
	public GpsData create(GpsData gpsdata) throws Exception;
	
	public void update(long id, GpsData gpsdata) throws Exception;

	public void delete(long id) throws Exception;
	 
	public long count() throws Exception;
 
	public String getCentralCacheId()throws Exception;


}
