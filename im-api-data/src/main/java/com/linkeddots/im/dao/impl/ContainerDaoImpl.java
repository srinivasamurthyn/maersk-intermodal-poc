/*
 * Copyright (c) 2016, Linkeddots Engineering Solutions Private Limited. All rights reserved.
 * Linkeddots Engineering Solutions Private Limited. PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.linkeddots.im.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.linkeddots.im.dao.ContainerDao;
import com.linkeddots.im.data.Container;
import com.linkeddots.im.repository.ContainerRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Linkeddots Development Team.
 *
 */

@CacheConfig(cacheNames="containers" , cacheManager="cacheManager")
@Component("ContainerDao")
@Slf4j
public class ContainerDaoImpl implements ContainerDao {
	
	
	@Autowired
	private ContainerRepository repository;

	@Cacheable
	public List<Container> findAll() throws Exception {
		log.debug("findAll() : Start."); 
        return repository.findAll();
    }
	
	@Cacheable
	public Container findOne(long id) throws Exception {
		return repository.findOne(id);
    }
	
	@CacheEvict(key = "#id", allEntries=true)
	public Container create(Container Container) throws Exception {
		clearCentralCacheId();
        return repository.save(Container);
    }
	
	@CachePut(key="#id")
	@CacheEvict(key="#id",allEntries=true)
	public void update(long id, Container Container) throws Exception {
		clearCentralCacheId();
		repository.save(Container);
    }
	
	@CacheEvict(key="#id",allEntries=true)
	public void delete(long id) throws Exception {
		clearCentralCacheId();
        repository.delete(id);
    }

	@Override
	public long count() throws Exception {
		return repository.count();
	}
	
	@CacheEvict(cacheManager="centralCacheManager", allEntries=true)
	public void clearCentralCacheId() throws Exception {
		//clear clearCentralCache
    }
	
	@Override
	@Cacheable(cacheManager="centralCacheManager")
	public String getCentralCacheId() throws Exception {
		return new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
    }
	
	
}
