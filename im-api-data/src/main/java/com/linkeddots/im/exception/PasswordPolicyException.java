package com.linkeddots.im.exception;

public class PasswordPolicyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PasswordPolicyException() {
		super();
	}

	public PasswordPolicyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PasswordPolicyException(String message, Throwable cause) {
		super(message, cause);
	}

	public PasswordPolicyException(String message) {
		super(message);
	}

	public PasswordPolicyException(Throwable cause) {
		super(cause);
	}
	
	

}
