package com.linkeddots.im.exception;

public class UnAuthRoleException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnAuthRoleException() {
		super();
	}

	public UnAuthRoleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public UnAuthRoleException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnAuthRoleException(String message) {
		super(message);
	}

	public UnAuthRoleException(Throwable cause) {
		super(cause);
	}
	
	

}
