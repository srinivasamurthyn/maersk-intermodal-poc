package com.linkeddots.im.exception;

public class ChangePasswordException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ChangePasswordException() {
		super();
	}

	public ChangePasswordException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ChangePasswordException(String message, Throwable cause) {
		super(message, cause);
	}

	public ChangePasswordException(String message) {
		super(message);
	}

	public ChangePasswordException(Throwable cause) {
		super(cause);
	}
	
	

}
