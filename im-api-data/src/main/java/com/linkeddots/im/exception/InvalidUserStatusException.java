package com.linkeddots.im.exception;

public class InvalidUserStatusException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUserStatusException() {
		super();
	}

	public InvalidUserStatusException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidUserStatusException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidUserStatusException(String message) {
		super(message);
	}

	public InvalidUserStatusException(Throwable cause) {
		super(cause);
	}
	
	

}
